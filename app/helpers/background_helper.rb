module BackgroundHelper

  def self.to_style(model)
    return nil unless model.background_attributes
    url = model.background.url
    attrs = model.background_attributes
    s = "background-image: url(#{ model.background.url }); "
    s.concat "background-repeat: no-repeat; background-size: cover; " if attrs["type"] == 'fill'
    s
  end

  def set_background_attributes(attrs)
    self.background_attributes = read_background_attributes(attrs)
  end

  private

  def check_background_attributes

  end

  # read background attributes from request params[:background_attributes]
  def read_background_attributes(attrs)
    attrs = JSON.parse(attrs)
    {
      "type" => attrs["type"]
    }
  end

end
