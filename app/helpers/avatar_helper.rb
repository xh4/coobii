# coding: utf-8
module AvatarHelper

  # do attributes checking
  def update_avatar_attributes(attrs)
    attrs = read_avatar_attributes(attrs)
    self.avatar_attributes = self.avatar_attributes.merge(attrs)
    self.avatar.reprocess_by_attributes
  end

  def set_avatar_attributes(attrs)
    attrs = read_avatar_attributes(attrs)
    self.avatar_attributes ||= {}
    self.avatar_attributes = self.avatar_attributes.merge(attrs)
  end

  private

  # read avatar attributes from request params[:avatar_attributes]
  def read_avatar_attributes(attrs)
    attrs = JSON.parse(attrs)
    {
      "scale" => attrs["scale"].to_f,
      "offset" => [attrs["offset_x"].to_f, attrs["offset_y"].to_f]
    }
  end

end
