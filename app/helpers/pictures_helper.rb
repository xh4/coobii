module PicturesHelper

  def self.gen_code(type)
    if type == :picture
      length = 7
    elsif type == :album
      length = 5
    end
    ([*('a'..'z'), *('A'..'Z'),*('0'..'9')]-%w(0 1 I O o)).sample(length).join
  end
end
