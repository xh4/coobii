module TimeHelper

  def self.time_ago(time)
    diff_seconds = Time.now - time
    case diff_seconds
    when 0 .. 59
      'now'
    when 60 .. (3600-1)
      "#{(diff_seconds/60).to_i}m"
    when 3600 .. (3600*24-1)
      "#{(diff_seconds/3600).to_i}h"
    when 3600*24 .. +1.0/0.0
      "#{(diff_seconds/3600/24).to_i}d"
    else
      nil
    end
  end

end
