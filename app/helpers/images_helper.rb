# -*- coding: utf-8 -*-
module ImagesHelper

  MSG_W = 456
  MSG_MAX_H = 220

  def self.msg_attr(image_attr)
    img_width = image_attr["width"] < MSG_W ? image_attr["width"] : MSG_W
    img_height = ( img_width.to_f / image_attr["width"] * image_attr["height"] ).to_i
    if img_height > MSG_MAX_H
      fram_height = MSG_MAX_H
      top = - ( img_height - fram_height ) / 2
    else
      fram_height = img_height
      top = 0
    end
    {
      :img_width => img_width,
      :img_height => img_height,
      :fram_height => fram_height,
      :top => top
    }
  end

  HERO_MSG_W = 199
  HERO_MSG_MAX_H = 98

  def self.hero_msg_attr(image_attr)
    img_width = image_attr["width"] < HERO_MSG_W ? image_attr["width"] : HERO_MSG_W
    img_height = ( img_width.to_f / image_attr["width"] * image_attr["height"] ).to_i
    if img_height > HERO_MSG_MAX_H
      fram_height = HERO_MSG_MAX_H
      top = - ( img_height - fram_height ) / 2
    else
      fram_height = img_height
      top = 0
    end
    {
      :img_width => img_width,
      :fram_height => fram_height,
      :top => top
    }
  end

  PIC_W = 540
  PIC_MAX_H = 520
  PIC_MIN_H = 200

  def self.msg_img(image)
    attr = self.msg_attr(image)
    content_tag(:div, content_tag(:img,
                                  :style => "top:#{attr[:top]}px; width:#{attr[:img_width]}px;",
                                  :'data-image-id' => image.id,
                                  :'data-image-width' => image.width,
                                  :'data-image-height' => image.height
                                 ),
                :class => 'msg_img',
                :style => "height: #{attr[:fram_height]}px;"
               )
  end

  def self.pic_attr(image)
    img_height = ( PIC_W.to_f / image.width * image.height ).to_i
    if img_height > PIC_MAX_H
      fram_height = PIC_MAX_H
    elsif img_height < PIC_MIN_H
      fram_height = PIC_MIN_H
    else
      fram_height = img_height
    end
    if fram_height > img_height
      top = ( fram_height - img_height ) / 2
    else
      top = - ( img_height - fram_height ) / 2
    end
    {
      :img_height => img_height,
      :fram_height => fram_height,
      :top => top
    }
  end

end
