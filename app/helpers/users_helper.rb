module UsersHelper

  def url
    "http://coobii.com/#{ self.username }"
  end

  def avatar_url()
    self.avatar.url
  end

  def bg_url
    self.background.url
  end

  def bg_style
    return nil unless self.background_attributes
    s = "background-image: url(#{ self.bg_url }); "
    s.concat "background-repeat: no-repeat; background-size: cover; " if self.background_attributes["type"] == 'fill'
    s
  end

end
