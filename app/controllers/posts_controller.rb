class PostsController < ApplicationController

  before_action :ensure_logged_in, only:[:create_or_get_draft,
    :new_image,
    :update]

  def list
    @topic = Topic.find_by_id(params[:topic_id])

    @posts = @topic.posts.where(draft: false)

    ids = @posts.inject([]) do |ids, post|
      ids << post.id
    end

    @contents = Content
      .joins("LEFT JOIN posts ON contents.detail_id = posts.id")
      .where(type: 'post')
      .where(detail_id: ids)
      .page(params[:page])
      .per(10)

    render 'contents/list'
  end

  def create_or_get_draft
    @topic = Topic.find_by_id(params[:topic_id])

    if @topic.state == 'close'
      return :nothing => true, :status => :forbidden
    end

    @post = @topic.posts.find_by(user_id: current_user.id,
                                 draft: true)
    if !@post
      @post = @topic.posts.create(user_id: current_user.id,
                                  draft: true,
				  primer: @topic.primer ? false : true)

      content_creator = ContentCreator.new @post
      @content = content_creator.create_content

    else
      @content = @post.content
    end

    render 'contents/get'
  end

  def update
    @post = Post.find_by_id(params[:id])

    @post.blocks = JSON.parse(params[:blocks])

    unless @post.valid?
      return render :json => @post.errors, :status => :bad_request
    end

    @post.save
    @content = @post.content

    render 'contents/get'
  end

  def publish
    @post = Post.find_by_id params[:id]
    @topic = @post.topic

    if @topic.state == 'close'
      return render :nothing => true, :status => :forbidden
    end

    @post.draft = false

    unless @post.valid?
      return render :json => @post.errors, :status => :bad_request
    end

    if @post.primer
      unless @topic.open
	return render :json => @topic.errors, :status => :bad_request
      end
    end

    @post.created_at = Time.now
    @post.save

    # user content
    user_content = UserContent.find_by_content_id(@post.content.id)
    user_content.created_at = Time.now
    user_content.save

    # timeline
    Resque.enqueue TimelineSender, @post.content.id, current_user.id

    # increase posts count
    @topic.increment(:posts_count).save

    # topic event
    if @post.primer
      @topic.topic_events.create(user_id: current_user.id,
                                 type: "open")
    end

    @content = @post.content
    @content.update(state: "normal")

    render 'contents/get'
  end

  def new_image
    @post = Post.find_by_id(params[:id])

    if @post.user_id != current_user.id
      return render :nothing => true, :status => :bad_request
    end

    @post_image = @post.post_images.create(image: params[:image],
                                           user_id: current_user.id)

    render 'get_image'
  end

  def list_images
    @post = Post.find_by_id(params[:id])

    @post_images = @post.post_images

    render 'list_images'
  end

end
