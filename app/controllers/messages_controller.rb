class MessagesController < ApplicationController

  before_action :ensure_logged_in

  def create

    @message = me.messages.new do |m|
      m.text = params[:text]
      m.image = params[:image]
    end

    unless @message.valid?
      return render :json => {:errors => @message.errors}, :status => :bad_request
    end

    @message.save

    content_creator = ContentCreator.new @message
    @content = content_creator.create_content

    render 'contents/get'

    Resque.enqueue TimelineSender, @content.id, current_user.id
  end

end
