class SuggestionsController < ApplicationController

  before_action :ensure_logged_in, only: [:list_users]

  def list_users
    @users = User
      .order("RANDOM()")
      .where.not(id: current_user.id)
      .limit(5)
    render 'users/list'
  end

  def list_messages
    @contents = Content.where(
                              :type => 'message'
                              )
      .order("RANDOM()")
      .limit(10)
    render 'contents/list'
  end

  def list_works
    @contents = Content.where(
                              :type => ['picture']
                              )
      .order("RANDOM()")
      .limit(10)
    render 'contents/list'
  end

  def get_radar
    @content = Content.where(
                             :type => 'picture',
                             :user_id => params[:user_id]
                             )
      .order('RANDOM()')
      .limit(1)
      .first
    render 'contents/get'
  end

end
