class ArticlesController < ApplicationController

  before_action :ensure_logged_in

  # text
  # list
  # image
  # quote

  def create_image

  end

  def create

    article = me.articles.new do |a|
      a.title = "草稿 #{ Time.now.to_date }"
      a.user_id = me.id
      a.blocks = []
    end
    article.save

    @post = me.posts.new do |p|
      p.type = 'article'
      p.detail_id = article.id
      p.ip = request.remote_ip
    end

    @post.save
    
    render 'posts/get'
 
  end

  def update
    params.require :post_id
    params.require(:blocks)
    blocks_param = []
    if params[:blocks].is_a? Hash
      params[:blocks].each_pair do |k, v|  # { 0 => { :block: }, 1 => { :block: } }
        if %w(text figure list quote).include?(v[:type])
          block = { :type => v[:type], :content => v[:content] }
          blocks_param.push block
        end
      end
    end

    @post = Post.find_by_id params[:post_id]

    if @post.nil?
      raise StandardError
    end

    if @post.user_id != me.id
      raise StandardError
    end

    article = @post.article

    article.blocks = []
    blocks_param.each do |b|
      sanitized = send "sanitize_#{b[:type]}_content", b[:content]
      if sanitized
        b[:content] = sanitized
        article.blocks.push b
      end
    end
    article.save

    render 'posts/get'

  end

  def update_cover
    params.require :offset

    if has_image_param? :cover
      image_file = Tempfile.new SecureRandom.uuid

      File.open(image_file.path, 'wb') do |f|

      end
    else

    end
  end

  private

  def sanitize_text_content(content)
    Sanitize.clean(content, :elements => %w(b i u a),
      :attributes => { 'a' => %w(href) },
      :protocols => { 'a' => { 'href' => %w(http https) } })
  end

  def sanitize_figure_content(content)
    image_id = content.to_i
    image = Image.find_by_id image_id
    if image.nil? then nil else image_id end
  end

  def sanitize_list_content(content)
    Sanitize.clean(content) # only text allowed
  end

  def sanitize_quote_content(content)
    Sanitize.clean(content)
  end

end
