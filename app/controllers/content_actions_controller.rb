class ContentActionsController < ApplicationController

  def do_action(action)
    get_content

    if ContentAction.where(type: action,
                           content_id: @content.id,
                           user_id: current_user.id).first.nil? &&
	@content.user_id != current_user.id

      ContentAction.create(type: action,
			   content_id: @content.id,
			   user_id: current_user.id)
      count = (action.to_s.pluralize + "_count").to_sym
      @content.increment(count).save
      @content.user.increment(count).save
    end
    render 'contents/get'
  end

  def undo_action(action)
    get_content

    record = ContentAction.find_by(type: action,
                                   content_id: @content.id,
                                   user_id: current_user.id)
    if record
      record.destroy
      count = (action.to_s.pluralize + "_count").to_sym
      @content.decrement(count).save
      @content.user.decrement(count).save
    end
    render 'contents/get'
  end

  def like
    self.do_action(:like)
  end

  def unlike
    self.undo_action(:like)
  end

  def mark
    self.do_action(:mark)
  end

  def unmark
    self.undo_action(:mark)
  end

  def share
    self.do_action(:share)
  end

  def unshare
    self.undo_action(:share)
  end

  private

  def get_content
    @content = Content.find_by_id(params[:content_id])
    unless @content
      return render :nothing => true, :status => :not_found
    end
  end

end
