class CategoriesController < ApplicationController

  before_action :ensure_logged_in, only: [:create, :delete]

  def get
    @category = Category.find_by_id params[:id]
    render 'get'
  end

  def list
    params.require(:user_id)
    @categories = User.find_by_id(params[:user_id]).categories.order(created_at: :asc).all
    render 'list'
  end

  def create
    params.require(:name)
    require_image_param :avatar

    @category = Category.new do |c|
      c.name = params[:name]
    end

    unless @category.valid?

    end

    key = ""
    path = '/tmp/uploads/backgrounds/' + key

    File.open path, 'wb' do |f|
      f.write params[:background].read
    end

    bucket = Storage::Bucket.new('backgrounds')

    bucket.files.create(
        :key => key,
        :body => File.open(path),
        :public => true
    )

    image = Image.new do |i|

    end
    image.save
    @category.image_id = image.id

    @category.user_id = me.id
    @category.save

    render 'get'
  end

  def delete
    # move all posts under this category to the default category
  end
end
