# coding: utf-8
class UsersController < ApplicationController

  include AvatarHelper

  before_action :ensure_logged_in, only:[:update_profile,
                                         :update_avatar,
                                         :update_bg,
                                         :reset_password]

  def get
    @user = User.find_by_id(params[:id])
    render 'get'
  end

  def update
    @user = current_user

    @user.update(display_name: params[:display_name],
                 headline: params[:headline],
                 bio: params[:bio])

    if params.has_key?(:avatar)
      @user.set_avatar_attributes params[:avatar_attributes]
      @user.avatar = params[:avatar]
    else
      @user.update_avatar_attributes params[:avatar_attributes]
    end

    @user.save

    if !@user.errors.blank?
      return render :json => @user.errors, :status => :bad_request
    end

    render 'get'

  rescue CarrierWave::ProcessingError
    return render :json => @user.errors, :status => :bad_request
  end

  def reset_password
    params.require :current_password
    params.require :new_password

    @user = current_user

    if @user.valid_password? params[:current_password]
      @user.password = params[:new_password]
      unless @user.valid?
        render :json => {
          :message => @user.errors[:password].first,
          :errors => @user.errors
        }, :status => :bad_request
      else
        @user.save
        sign_in @user, :bypass => true
        render 'get'
      end
    else
      render :nothing => true, :status => :unauthorized
    end
  end

  def update_bg
    @user = current_user

    if params.has_key?(:background)
      @user.background = params[:background]
    end
    @user.set_background_attributes params[:background_attributes]

    @user.save

    if !@user.errors.blank?
      return render :json => @user.errors, :status => :bad_request
    end

    render 'get'
  end


  def whoami
    if logged_in?
      @user = current_user
      render 'get'
    else
      render :text => 'guest'
    end
  end

  private

  def user_param
    params.require(:user).permit(:display_name, :headline, :bio, :avatar, :avatar_attributes)
  end

end
