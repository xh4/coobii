class NamespacesController < ApplicationController

  include ShowNamespace

  def show
    @namespace = Namespace.find_by_name_lower(params[:namespace].downcase)
    type = @namespace.type

    if type == 'user'

      show_user

    elsif type == 'page_alias:short'

      return redirect_to "/#{@namespace.name_lower}" if params[:namespace] != @namespace.name_lower
      show_page

    else ['page', 'page_alias:zh_cn'].include?(type)

      short_ns = Namespace.find_by(model_id: @namespace.model_id,
				   type: 'page_alias:short')
      return redirect_to "/#{short_ns.name_lower}" if short_ns

      page = Page.find_by_id(@namespace.model_id)

      if params[:namespace] != page.name
	return redirect_to "/#{page.name}"
      end

      show_page

    end

  end

end
