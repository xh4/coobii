class PagesController < ApplicationController

  def get
    @page = Page.find_by_id(params[:id])

    render 'get'
  end

  def change_bg
    @page = Page.find_by_id(params[:id])

    unless @page.creator_id == current_user.id
      return render :nothing => true, :status => :unauthorized
    end

    @page.set_background_attributes params[:background_attributes]
    @page.background = params[:background]

    @page.save

    if !@page.errors.blank?
      return render :json => @page.errors, :status => :bad_request
    end

    render 'get'
  end

end
