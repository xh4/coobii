require 'uri'

class LinksController < ApplicationController

  before_action :ensure_logged_in

  def create
    link = me.links.new do |l|
      l.title = params[:title]
      l.url = params[:url]
      l.caption = params[:caption]
    end
    unless link.valid?
      return render :json => link.errors, :status => :bad_request
    end

    if params[:image] && params[:image].is_a?(ActionDispatch::Http::UploadedFile)
      code = SecureRandom.uuid
      name = code + File.extname(params[:image].original_filename)
      path = Rails.root.join 'uploads', 'links', name

      # transfer
      File.open path, 'wb' do |f|
        f.write params[:images].read
      end

      Aliyun::OSS::OSSObject.store name, open(path), 'coobii-image'

      image_size = FastImage.size path

      image = me.images.new do |i|
        i.type = 'link'
        i.width = image_size[0]
        i.height = image_size[1]
        i.filesize = File.new(path).size
        i.filename = name
        i.format = params[:image].content_type
        i.user_id = me.id
      end
      image.save

      link.image_id = image.id
    end

    uri = URI.parse link.url
    link.domain = uri.host
    link.domain = link.domain.sub 'www.', ''

    link.save

    @post = me.posts.create(type: 'link', detail_id: link.id, ip: request.remote_ip)
    render 'posts/get'

  end
end
