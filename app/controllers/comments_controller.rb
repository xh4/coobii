class CommentsController < ApplicationController

  before_action :ensure_logged_in, only: [:create, :delete, :up, :down]

  # GET /contents/:id/comments
  def list
    content = Content.find_by_id params[:id]
    return render :nothing => true, :status => 404 if content.nil?
    @comments = content.comments
    render 'list'
  end

  # POST /contents/:id/comments
  def create
    @content = Content.find_by_id(params[:content_id])

    @comment = @content.comments.new do |c|
      c.text = params[:text]
      c.parent_id = params[:parent_id]
      c.user_id = current_user.id
      c.ip = request.remote_ip
      c.image = params[:image]
    end

    unless @comment.valid?
      return render :json => @comment.errors, :status => :bad_request
    end

    @comment.save

    render 'get'
  end

  # DELETE /comments/:id
  def delete
  end

  # POST /comments/:id/up
  def up
    @comment = Comment.find_by_id(params[:id])

    vote = @comment.comment_votes.find_by(user_id: current_user.id,
                                          comment_id: params[:id])
    if vote && vote.type == 'up'
      return render 'get'
    end

    if vote && vote.type == 'down'
        vote.destroy
    end

    CommentVote.create(user_id: current_user.id,
                       comment_id: params[:id],
                       type: "up")

    @comment.reload

    render 'get'
  end

  # POST /comments/:id/down
  def down
    @comment = Comment.find_by_id(params[:id])

    vote = @comment.comment_votes.find_by(user_id: current_user.id,
                                          comment_id: params[:id])
    if vote && vote.type == 'down'
      return render 'get'
    end

    if vote && vote.type == 'up'
        vote.destroy
    end

    CommentVote.create(user_id: current_user.id,
                       comment_id: params[:id],
                       type: "down")

    @comment.reload

    render 'get'
  end

  # PUT /comments
  def change_state

  end

end
