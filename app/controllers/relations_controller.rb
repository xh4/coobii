class RelationsController < ApplicationController

  before_action :ensure_logged_in

  def list_followers
    @users = current_user.followers
    render 'users/list'
  end

  def list_friends
    @users = current_user.friends
    render 'users/list'
  end

  def create
    params.require :user_id

    return render :nothing => true if self_action?

    user = User.find_by_id params[:user_id]
    return render :nothing => true, :status => :not_found if user.nil?

    unless current_user.following? user
      current_user.reverse_relations.create(user_id: params[:user_id])
      current_user.increment(:friends_count).save
      user.increment(:followers_count).save
    end

    @user = user
    render 'users/get'

    Resque.enqueue FollowAndPullContentsWorker, current_user.id, user.id
  end

  def delete
    params.require :user_id

    return render :nothing => true if self_action?

    user = User.find_by_id params[:user_id]
    return render :nothing => true, :status => :not_found if user.nil?

    if current_user.following? user
      current_user.reverse_relations.find_by(user_id: params[:user_id]).destroy
      current_user.decrement(:friends_count).save
      user.decrement(:followers_count).save
    end

    @user = user
    render 'users/get'

    Resque.enqueue UnfollowAndDeleteContentsWorker, current_user.id, user.id
  end

  private

  def self_action?
    current_user.id == params[:user_id]
  end

end
