require_dependency 'show_namespace'

class TopicsController < ApplicationController

  include ShowNamespace

  before_action :ensure_logged_in, only:[:create_or_get_pending,
    :update]

  def get
    @topic = Topic.find_by_id(params[:topic_id])

    if @topic.state == 'pending' && @topic.user_id != current_user.id
      return :nothing => true, :status => :not_found
    end

    render 'get'
  end

  def list
    @page = Page.find_by_id(params[:page_id])

    @topics = @page.topics
      .where.not(state: "pending")
      .order("created_at DESC")
      .page(params[:page])
      .per(10)

    render 'list'
  end

  def create_or_get_pending
    @page = Page.find_by_id(params[:page_id])

    @topic = @page.topics.find_by(user_id: current_user.id,
                                  state: "pending")
    if @topic.nil?
      @topic = @page.topics.create(user_id: current_user.id,
                                   state: "pending")
    end

    render 'get'
  end

  def update
    @topic = Topic.find_by_id(params[:topic_id])

    @topic.title = params[:title]

    unless @topic.valid?
      return render :json => @topic.errors, :status => :bad_request
    end

    @topic.save

    render 'get'
  end

  def close
    @topic = Topic.find_by_id(params[:topic_id])

    if @topic.user_id != current_user.id
      return render :nothing => true, :status => :unauthorized
    end

    if @topic.state == 'close' || @topic.state == 'pending'
      return render 'get'
    end

    if @topic.close
      @topic.topic_events.create(user_id: current_user.id,
                                 type: 'close')
    end

    render 'get'
  end

  def show
    @namespace = Namespace.find_by_name_lower(params[:namespace].downcase)

    @page = Page.find_by_id(@namespace.model_id)

    @topic = @page.topics.find_by_id(params[:topic_id])

    @posts = @topic.posts
      .where(draft: false)
      .order('created_at ASC')

    page_resources

    if user_signed_in?

      @user = current_user
      user_resources

      render 'show'
    else
      render 'show_guest'
    end
  end

  def increase_views_count
    @topic = Topic.find_by_id(params[:id])
    @topic.increment(:views_count)
    @topic.save
    render :nothing => true
  end

  def get_archives
    @page = Page.find_by_id(params[:page_id])

    @topics_by_month = @page.topics.find(:all).group_by { |t| t.created_at.strftime("%B") }

    render 'archive'
  end

end
