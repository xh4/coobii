class ContentsController < ApplicationController

  include ShowNamespace

  before_action :ensure_logged_in, only: [:delete, :list_marks, :list_likes, :list_shares, :list_timeline]

  def show
    unless %w(messages posts).include? params[:content_type]
      return render :nothing => true, :status => :not_found
    end

    @user = User.find_by_username_lower(params[:namespace].downcase)

    detail = @user.send(params[:content_type].downcase.pluralize.to_sym).find_by_id(params[:detail_id])

    return redirect_to '/' if detail.nil?

    @content = detail.content

    user_resources

    render 'show'
  end

  def get
    @content = Content.find_by_id(params[:content_id])
    render 'get'
  end

  def delete
    @content = Content.find_by_id(params[:content_id])

    if @content.user_id != current_user.id
      return render :nothing => true, :status => :unauthorized
    end

    if @content.type == 'post' && @content.detail.primer
      return render :nothing => true, :status => :forbidden
    end

    render 'get'
    @content.destroy
  end

  def list(join_contents, user = current_user)

    @contents = Content
      .joins("INNER JOIN #{join_contents} ON contents.id = #{join_contents}.content_id")
      .where('contents.type' => content_types,
             "#{join_contents}.user_id" => user.id,
             'contents.state' => 'normal')
      .order("#{join_contents}.created_at DESC")
      .page(params[:page])
      .padding(params[:padding])
      .per(10)

    render 'contents/list'
  end

  def list_user
    user = User.find_by_id(params[:user_id])
    self.list('user_contents', user)
  end

  def list_timeline
    self.list('timeline_contents')
  end

  def list_actions(action_type)
    @contents = Content
      .joins("INNER JOIN content_actions ON contents.id = content_actions.content_id")
      .where('contents.state' => 'normal',
             'content_actions.user_id' => current_user.id,
             'content_actions.type' => action_type)
      .order("content_actions.created_at DESC")
      .page(params[:page])
      .padding(params[:padding])
      .per(10)

    render 'contents/list'
  end

  def list_marks
    self.list_actions('mark')
  end

  def list_likes
    self.list_actions('like')
  end

  private

  def content_types
    all_types = %w(message post)
    type = params[:type]
    if type
      if type.is_a? String
        type = [type]
      end
      type = type.select { |t| all_types.include?(t) }
    else
      type = all_types
    end
    type
  end



end
