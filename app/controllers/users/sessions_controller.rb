class Users::SessionsController < ApplicationController

  def new
    render 'new', :layout => 'welcome'
  end

  def create

    @user = User.where(["lower(username) = :value OR lower(email) = :value", { :value => params[:username_or_email].downcase }]).first

    unless @user
      flash[:error] = "错误的用户名(邮件地址)或密码"
      flash[:user] = {
        username_or_email: params[:username_or_email]
      }
      return redirect_to "/login"
    end

    if login(@user.email, params[:password])
      redirect_to "/#{@user.username}"
    else
      flash[:error] = "错误的用户名(邮件地址)或密码"
      flash[:user] = {
        username_or_email: params[:username_or_email]
      }
      return redirect_to "/login"
    end
  end

  def destroy
    @user = current_user

    logout

    render 'users/get'
  end

  private

  def fail

  end

end
