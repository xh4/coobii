class Users::RegistrationsController < ApplicationController

  rescue_from ActionController::ParameterMissing, with: :missing_params

  def new
    render 'new', :layout => 'welcome'
  end

  def show
    flash[:user] = {
      display_name: params[:display_name],
      email: params[:email],
      password: params[:password]
    }
    redirect_to action: :new
  end

  def create

    @invite_code = InviteCode.find_by_code(params[:invite_code])

    if !@invite_code || @invite_code.used_by != nil
      return render :json => ["邀请码 不存在或已被使用"], :status => :bad_request
    end

    @user = User.new do |u|
      u.username = params[:username]
      u.display_name = params[:display_name]
      u.password = params[:password]
      u.email = params[:email]
    end

    @user.set_avatar_attributes params[:avatar_attributes]
    @user.avatar = params[:avatar]

    @user.save

    unless @user.errors.empty?
      return render :json => @user.errors.full_messages, :status => :bad_request
    end

    @invite_code.update(used_by: @user.id)

    auto_login @user

    render 'users/get'

  rescue CarrierWave::ProcessingError
    return render :json => @user.errors, :status => :bad_request
  end

  private

  def missing_params(e)
    render :text => "Required parameter missing: #{e.param}", :status => :bad_request
  end


end
