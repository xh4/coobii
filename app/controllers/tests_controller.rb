class TestsController < ApplicationController
  def show
    render "tests/#{params[:test]}", :layout => 'test'
  end
end
