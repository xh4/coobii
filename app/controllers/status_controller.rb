class StatusController < ApplicationController

  def counts
    render :json => {
      :users => User.count,
      :messages => Message.count,
      :works => Content.where(
                              :type => ['picture']
                              )
        .count
    }
  end

end
