class TimelinesController < ApplicationController

  before_action :ensure_logged_in


  def list(join_contents)

    @contents = Content
      .joins("LEFT OUTER JOIN timeline_contents ON contents.id = timeline_contents.content_id")
      .where('timeline_contents.user_id' => current_user.id)
      .where('contents.type' => type)
      .where('contents.state' => 'normal')
      .order('timeline_contents.created_at DESC')
      .page(params[:page])
      .padding(params[:padding])
      .per(10)

    render 'contents/list'
  end

  def list_user
    self.list('user_contents')
  end

  def list_timeline
    self.list('timeline_contents')
  end

  def list_marks
    self.list('marks')
  end

  def list_likes
    self.list('likes')
  end

  private

  def content_types
    all_types = %w(message post)
    type = params[:type]
    if type
      if type.is_a? String
        type = [type]
      end
      type = type.select { |t| all_types.include?(t) }
      type = all_types if type.blank?
    else
      type = all_types
    end
  end

end
