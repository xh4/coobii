class ApplicationController < ActionController::Base

  helper_method :user_signed_in?

  private

  def me
    current_user
  end

  def ensure_logged_in
    render :nothing => true, :status => :unauthorized unless user_signed_in?
  end

  def filter_browser
    if browser.mobile? || browser.tablet? || !browser.modern?
      use_lite_views
    end
  end

  def use_lite_views
    prepend_view_path Rails.root + 'app' + 'views_lite'
  end

  alias_method :user_signed_in?, :logged_in?

end
