class WelcomeController < ApplicationController

  before_action :filter_browser

  def index
    if user_signed_in?
      redirect_to "/#{current_user.username}"
    else
      @contents = Content.order("RANDOM()").limit(10)
      .where(type: 'message')
      @counts = {
        :users => User.count,
        :messages => Message.count,
        :works => Content.where(
                                :type => ['picture']
                                )
          .count
      }
      render 'index', layout: 'welcome'
    end
  end

  def list_creators
    @users = User.limit(13)

    render 'users/list'
  end

end
