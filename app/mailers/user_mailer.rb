class UserMailer < ActionMailer::Base
  default from: "Coobii <noreply@coobii.com>"

  def activate_mailer(user)
    @user = user
    attachments.inline['logo.png'] = File.read(Rails.root.join('app/assets/images/mailer_logo.png'))
    attachments.inline['top-rounded-bg.png'] = File.read(Rails.root.join('app/assets/images/top-rounded-bg.png'))
    attachments.inline['bottom-rounded-bg.png'] = File.read(Rails.root.join('app/assets/images/bottom-rounded-bg.png'))
    attachments.inline['btn-activate.png'] = File.read(Rails.root.join('app/assets/images/btn-activate.png'))

    mail to: @user.email,
         subject: "感谢注册 Coobii，请确认您的账号邮箱地址"
  end
end
