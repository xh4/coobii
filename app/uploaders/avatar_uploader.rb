require 'tempfile'

class AvatarUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :aliyun

  process :set_original_attributes

  process :check_avatar_attributes

  process :convert => "jpg"

  def store_dir
    Rails.env.production? ? nil : "avatars"
  end

  def extension_white_list
    %w(jpg jpeg png gif)
  end

  def initialize(*)
    super
    @aliyun_bucket = Rails.env.production? ? "coobii-avatars" : "coobii-test"
  end

  def filename
    "#{model.username}-o.jpg"
  end

  def set_original_attributes
    manipulate! do |img|
      model.avatar_attributes.merge!({
          "width" => img[:width],
          "height" => img[:height],
          "mime_type" => "image/jpeg",
          "bytes" => file.size
        })
      img = yield(img) if block_given?
      img
    end
  end

  def check_avatar_attributes
    size = [
      model.avatar_attributes["width"],
      model.avatar_attributes["height"]
    ]
    scale = model.avatar_attributes["scale"]
    offset = model.avatar_attributes["offset"]
    valid = true

    if scale <= 0 || scale > 120  # "nan".to_f == 0.0 == 0
      valid = false
    end
    width = ( size[0] * scale / 100 ).round
    height = ( size[1] * scale / 100 ).round
    if width < 200 || height < 200
      valid = false
    end
    if offset[0] < 0 || offset[1] < 0 || offset[0] + 200 > width || offset[1] + 200 > height
      valid = false
    end

    if !valid
      model.errors.add(:avatar_attributes, model.errors.generate_message(:avatar_attributes, :invalid))
      raise CarrierWave::ProcessingError.new
    end
  end

  def process_by_attributes
    manipulate! do |img|
      attrs = model.avatar_attributes
      scale = "#{attrs["scale"]}%"
      crop = "200x200+#{ attrs["offset"][0] }+#{ attrs["offset"][1] }"
      img.combine_options do |c|
        c.scale scale
        c.crop crop
      end
      img = yield(img) if block_given?
      img
    end
  end

  def reprocess_by_attributes
    self.cache_stored_file!
    self.retrieve_from_cache!(self.cache_name)
  end

  version :normal do
    def full_filename(for_file = model.avatar.file)
      "#{model.username}.jpg"
    end

    process :process_by_attributes
  end

end
