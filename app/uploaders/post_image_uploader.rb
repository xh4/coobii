class PostImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :aliyun

  process :set_image_attributes

  version :thumb do
    def full_filename(for_file = model.image.file)
      parent_name = super(for_file)
      ext         = File.extname(parent_name)
      base_name   = parent_name.gsub(/thumb_/, "").chomp(ext)
      [version_name, base_name].compact.join('_') + ".jpg"
    end
    process :resize_to_fill => [200,200]
    process :convert => "jpg"
    process :set_thumb_attributes
  end

  def filename
    if original_filename
      model.user.username +
        '-p-' +
        timestamp + '-' +
        token +
        File.extname(file.filename)
    end
  end

  def token
    model.image_attributes = model.image_attributes || {}
    model.image_attributes["token"] = model.image_attributes["token"] || SecureRandom.hex(3)
    model.image_attributes["token"]
  end

  def timestamp
    model.image_attributes = model.image_attributes || {}
    model.image_attributes["timestamp"] = model.image_attributes["timestamp"] || Time.new.utc.to_i.to_s
    model.image_attributes["timestamp"]
  end

  def store_dir
    "posts"
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def initialize(*)
    super
    @aliyun_bucket = Rails.env.production? ? "coobii-images" : "coobii-test"
  end

  def set_image_attributes
    manipulate! do |img|
      model.image_attributes = {
        "width" => img[:width],
        "height" => img[:height],
        "mime_type" => file.content_type,
        "bytes" => file.size
      }
      img = yield(img) if block_given?
      img
    end
  end

  def set_thumb_attributes
    manipulate! do |img|
      model.image_attributes["versions"] = {
        "thumb" => {
          "width" => img[:width],
          "height" => img[:height],
          "mime_type" => "image/jpeg",
          "bytes" => file.size
        }
      }
      img
    end
  end

end
