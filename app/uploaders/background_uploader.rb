class BackgroundUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :aliyun

  process :convert => "jpg"

  process :resize_to_fill => [960, 640], :if => :fill?

  def store_dir
    Rails.env.production? ? nil : "backgrounds"
  end

  def extension_white_list
    %w(jpg jpeg png gif)
  end

  def initialize(*)
    super
    @aliyun_bucket = Rails.env.production? ? "coobii-backgrounds" : "coobii-test"
  end

  def filename
    if original_filename
      if model.is_a?(User)
	"#{model.username}-bg#{ File.extname(original_filename) }"
      elsif model.is_a?(Page)
	"#{model.name}-bg#{ File.extname(original_filename) }"
      end
    end
  end

  protected

  def fill?(file)
    model.background_attributes && model.background_attributes["type"] == "fill"
  end

end
