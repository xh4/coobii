class MessageImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :aliyun

  process :set_image_attributes

  def filename
    if file
      model.user.username +
        '-msg-' +
        timestamp + '-' +
        token +
        File.extname(file.filename)
    end
  end

  def token
    model.image_attributes = model.image_attributes || {}
    model.image_attributes["token"] = model.image_attributes["token"] || SecureRandom.hex(3)
    model.image_attributes["token"]
  end

  def timestamp
    model.image_attributes = model.image_attributes || {}
    model.image_attributes["timestamp"] = model.image_attributes["timestamp"] || Time.new.utc.to_i.to_s
    model.image_attributes["timestamp"]
  end

  def store_dir
    "messages"
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def initialize(*)
    super
    @aliyun_bucket = Rails.env.production? ? "coobii-images" : "coobii-test"
  end

  def set_image_attributes
    if file
      img = MiniMagick::Image.open(file.file)
      model.image_attributes = model.image_attributes || {}
      model.image_attributes = {
        "width" => img[:width],
        "height" => img[:height],
        "mime_type" => file.content_type,
        "bytes" => file.size
      }
    end
  end

end
