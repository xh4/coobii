var Page = require('../app/views/pages/page.js');

var page = new Page;

var runTest = function() {
    page.on('slide-in-setup-complete', function() {
        $('#container').append( page.el );
        page.trigger('slide-in');
    });
    page.trigger('slide-in-setup');
};

$(function() {
    runTest();
});
