var _ = require('underscore'),
    Backbone = require('backbone');

var User = require('../models/user.js');

var UserStore = new (Backbone.Collection.extend({

    model: User

}));

module.exports = UserStore;
