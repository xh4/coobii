var _ = require('underscore'),
    Backbone = require('backbone');

var Comment = require('../models/comment.js');

var CommentStore = new (Backbone.Collection.extend({

    model: Comment

}));

module.exports = CommentStore;
