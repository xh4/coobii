var _ = require('underscore'),
    Backbone = require('backbone');

var Topic = require('../models/topic.js');

var TopicStore = new (Backbone.Collection.extend({

    model: Topic,

    initialize: function(options) {
        _.extend(this, options);
    },

    parse: function(res) {
        return res.topics;
    },

    loadTopics: function(options, fn) {
        var params = _.omit(options, 'url');
        $.get(options.url, params).done(function(res) {
            var topics = this.handleResponse(res.topics);
            fn(topics, res.pagination);
        }.bind(this));
    },

    handleResponse: function(topics) {
        var arr = [];
        _.each(topics, function(t) {
            var modelInCollection = this.findWhere({id: t.id});
            if (modelInCollection) {
                modelInCollection.set(t);
                modelInCollection.initialize();
                arr.push(modelInCollection);
            } else {
                var topic = new Topic(t);
                this.add(topic);
                arr.push(topic);
            }
        }.bind(this));
        return arr;
    }

}));

module.exports = TopicStore;
