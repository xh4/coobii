var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');

var Content = require('../models/content.js');

var Types = {
    message: require('../models/message.js'),
    post: require('../models/post.js')
};

var ContentStore = new (Backbone.Collection.extend({

    model: Content,

    url: '/api/contents',

    parse: function(res) {
        return res.contents;
    },

    loadContents: function(options, fn) {
        var collection = this;
        var params = _.omit(options, 'url');
        $.get(options.url, params).done(function(res) {
            var contents = collection.handleResponse(res);
            fn(contents, res.pagination);
        });
    },

    loadPosts: function(options, fn) {
        var topic_id = options.topic_id,
            url = '/api/posts?topic_id=' + topic_id;
        $.ajax({
            url: url,
            context: this
        }).done(function(res) {
            var contents = this.handleResponse(res);
            fn(contents, res.pagination);
        });
    },

    handleResponse: function(res) {
        var arr = [],
            collection = this;
        _.each(res.contents, function(c) {
            if (c &&
                c.type &&
                Types[c.type] !== undefined) {
                var modelInCollection = collection.findWhere({id: c.id});
                if (modelInCollection) {
                    modelInCollection.set(c);
                    modelInCollection.initialize();
                    arr.push(modelInCollection);
                } else {
                    var content = new Types[c.type](c);
                    collection.add(content);
                    arr.push(content);
                }
            }
        });
        return arr;
    }

}));

module.exports = ContentStore;
