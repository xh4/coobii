var _ = require('underscore'),
    Backbone = require('backbone');

var AppDispatcher = require('../dispatcher/app-dispatcher.js');

var BackgroundConstants = require('../constants/background-constants.js');

var BackgroundState = new (Backbone.Model.extend({



}));

AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.actionType) {
    case (BackgroundConstants.CHANGE_BACKGROUND):
        BackgroundState.set(action.attrs);
        break;
    default:
        return true;
    }

});

module.exports = BackgroundState;
