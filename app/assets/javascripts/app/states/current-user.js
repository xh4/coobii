var _ = require('underscore'),
    Backbone = require('backbone');

var AppDispatcher = require('../dispatcher/app-dispatcher.js');

var User = require('../models/user');

var CurrentUser = new (User.extend({

    construct: function() {
        var body = document.body;
        if (body.getAttribute('data-logged-in') !== undefined) {
            var id = body.getAttribute('data-current-user-id'),
                username = body.getAttribute('data-current-user-username'),
                display_name = body.getAttribute('data-current-user-display-name');
            this.set({
                id: parseInt(id),
                username: username,
                display_name: display_name
            });
        }
    },

    isSignedIn: function() {
        return !this.isGuest();
    },

    isLoggedIn: function() { return this.isSignedIn(); },

    getIdentity: function() {
        return document.body.getAttribute('data-identity');
    },

    isGuest: function() {
        return this.getIdentity() === 'guest';
    },

    isMaster: function() {
        return this.getIdentity() === 'master';
    }

}));

AppDispatcher.register(function(payload) {

    var action = payload.action;

    switch(action.actionType) {
    case ("toggle-place"):
        CurrentUser.set('place', action.place);
        break;
    default:
        return true;
    }

});

module.exports = CurrentUser;
