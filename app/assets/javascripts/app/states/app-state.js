var _ = require('underscore'),
    Backbone = require('backbone');

var AppDispatcher = require('../dispatcher/app-dispatcher.js');

var AppConstants = require('../constants/app-constants.js');

var AppState = new (Backbone.Model.extend({



}));

AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.actionType) {
    case (AppConstants.TOGGLE_PLACE):
        AppState.set('place', action.place);
        break;
    default:
        return true;
    }

});

module.exports = AppState;
