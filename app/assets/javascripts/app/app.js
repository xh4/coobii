var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

require('ahoy');

var Coobii = require('./coobii');

var Me = require('./me'),
    Router = require('./router'),
    Navbar = require('./views/navbar'),
    Showcase = require('./views/showcase'),
    Slider = require('./views/slider'),
    UserHero = require('./views/users/hero'),
    PageHero = require('./views/pages/hero'),
    Frame = require('./views/frame');

var AppState = require('./states/app-state'),
    AppActions = require('./actions/app-actions');

var CurrentUser = require('./states/current-user');

var Viewers = {
    "message": require('./views/viewers/viewer-message'),
    "post": require('./views/viewers/viewer-post')
};

var Application = function() {

    new Coobii;

    _.extend(this, Backbone.Events);

    // state: me
    Me.init();
    window.states["me"] = Me;

    CurrentUser.construct();
    if (!CurrentUser.isGuest())
        CurrentUser.fetch();

    var router = new Router();
    window["router"] = router;

    Backbone.history.start({
        pushState: true
    });

    // state: navbar
    var navbar = new Navbar();

    var frame = new Frame({
        el: $('#container > .fm')
    });

    this.switchToSlider = function(slideName, fn) {
        this.hero.hide(function() {
            this.slider.show(slideName, function() {
                if (fn) fn();
                AppActions.togglePlaceLock = false;
            });
        }.bind(this));
    };

    this.switchToHero = function(fn) {
        this.slider.hide(function() {
            this.hero.show(function() {
                if (fn) fn();
                AppActions.togglePlaceLock = false;
            });
        }.bind(this));
    };

    this.switchToSlide = function(slideName, options, fn) {
        var appendSide = options.appendSide;
        this.slider.switchToSlideByName(slideName, appendSide, function() {
            if (fn) fn();
            AppActions.togglePlaceLock = false;
        });
    };

    AppState.set('place', 'hero');

    this.listenTo(AppState, 'change:place', function(s, v, options) {
        if (options && options.silentToDispatcher) return;
        AppActions.togglePlaceLock = true;
        var prevPlace = AppState.previous('place'),
            newPlace = AppState.changedAttributes()['place'];
        if (prevPlace === 'hero' ) {
            // from hero to slider
            this.switchToSlider(newPlace);
        } else if (newPlace === 'hero') {
            // from slider to hero
            this.switchToHero();
        } else {
            // from slide to another slide
            this.switchToSlide(newPlace, options);
        }
        navbar.renderState();
    });

    var path = location.pathname, // /Koakuma/messages/14
        regex = /\/(\w+)\/(\w+)\/(\d+)/,
        nsType = $('body').attr('data-init-ns-type');

    this.initWithUser = function() {
        this.hero = new UserHero({
            el: $('.hero')
        });
        this.hero.hook();
        this.slider = new Slider({
            el: $('.slider')
        });
        this.slider.hook();
        var showcaseState = Coobii.createState('showcase');
    };

    this.initWithViewer = function() {
        var m = path.match(regex),
            ns = m[1],
            title = $('body').data('init-ns-title'),
            contentType = m[2].slice(0, -1),
            detailId = parseInt(m[3]),
            Viewer = Viewers[contentType];

        this.hero = new UserHero({
            el: $('.hero')
        });
        this.hero.hook();
        this.slider = new Slider({
            el: $('.slider')
        });
        this.slider.hook();

        var viewer = new Viewer({
            el: $('#showcase .viewer')
        });
        viewer.hook();

        var showcaseState = Coobii.createState('showcase', {
            'display': 'show',
            'savedNS': ns,
            'savedTitle': title
        });
        this.listenToOnce(showcaseState, 'change:display', function() {
            this.hero.show();
        });
        showcaseState.viewer = viewer;
    };

    this.initWithPage = function() {
        this.slider = new Slider({
            el: $('.slider')
        });
        this.slider.hook();
        if ($('body').attr('data-logged-in') !== undefined) {
            this.hero = new UserHero({
                el: $('.hero')
            });
            this.hero.hook();
        }
        AppState.set({
            'place': 'teatime'
        }, {
            'silentToDispatcher': true
        });
        var showcaseState = Coobii.createState('showcase');
    };

    if (nsType === 'page') this.initWithPage();
    else if (nsType === 'user') {
        if (regex.test(path)) this.initWithViewer();
        else this.initWithUser();
    }

    var showcase = new Showcase();
};

new Application();
