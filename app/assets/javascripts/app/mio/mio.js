var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var mioTemplate = require('../templates/mio/mio.hbs'),
    Coobii = require('../coobii.js'),
    MessageBox = require('./box-message.js'),
    PictureBox = require('./box-picture.js'),
    LinkBox = require('./box-link.js'),
    ArticleBox = require('./box-article.js'),
    QuoteBox = require('./box-quote.js'),
    PrivateMessageBox = require('./box-private-message.js');

var Boxes = {
    "message": MessageBox,
    "picture": PictureBox,
    "link": LinkBox,
    "article": ArticleBox,
    "quote": QuoteBox,
    "private-message": PrivateMessageBox
};

var Mio = Backbone.View.extend({

    className: 'mio',

    template: mioTemplate,

    render: function() {
        this.$el.html( this.template() );
        return this;
    },

    events: {
        "click .content-types li": "onClick"
    },

    onClick: function(e) {
        li = $(e.target).closest('li'),
        type = li.attr('data-content-type');
        this.loadBox(type);
    },

    loadBox: function(name) {
        var Box = Boxes[name];

        var box;
        if (name === 'message') {
            box = new Box({
                owner: 'mio',
                maxImageSize: 474
            });
        } else {
            box = new Box;
        }

        this.currentBox = box;

        box.render();

        var showcaseState = Coobii.getState('showcase');
        showcaseState.set('display', 'show', {
            'type': 'mio',
            'box': box
        });

        box.on('post-done', function() {
            this.closeBox();
        });
    }

});

module.exports = Mio;
