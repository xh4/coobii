var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var twitterText = require('twitter-text');

var MessageBoxText = Backbone.View.extend({

    className: 'text',

    inComposition: false,

    initialize: function() {
        this.$el.html("<div></div>");

        var text = this;

        setInterval(function() {
            if (text.$el.find('div').length === 0) {
                text.$el.html("<div></div>");
                var divNode = text.$el.find('div')[0];
                range = document.createRange();
                range.selectNodeContents(divNode);
                range.collapse(false);
                selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }, 50);

        this.el.addEventListener("paste", this.paste.bind(this));
    },

    attributes: {
        "contenteditable": "true",
        "spellcheck": "false"
    },

    getContent: function() {
        return  this.$el[0].textContent.replace(/\s*$/, '');
    },

    events: {
        "compositionstart": function(e) { this.inComposition = true; },
        "compositionend": function(e) { this.inComposition = false; }
    },

    getOffset: function() {
        var selection = window.getSelection(),
            focusNode = selection.focusNode,
            isSubNode = focusNode.parentNode.nodeName === 'A',
            offset = 0,
            currentNode = isSubNode ? focusNode.parentNode : focusNode;

        while(currentNode.previousSibling !== null) {
            currentNode = currentNode.previousSibling;
            offset += currentNode.textContent.length;
        } offset += selection.focusOffset;

        return offset;
    },

    resumeOffset: function(offset) {
        var selection = window.getSelection(),
            focusNode = selection.focusNode,
            lineNode = focusNode.nodeName === 'DIV' ?
                focusNode : focusNode.parentNode,
            charPassed = 0,
            childNodes = lineNode.childNodes;

        if (childNodes.length === 0) return;
        var targetNode = childNodes[0];
        for (var i=0; i<childNodes.length; i++) {
            var childNode = childNodes[i];
            if (charPassed + childNode.textContent.length >= offset) {
                targetNode = childNode;
                break;
            } else {
                charPassed += childNode.textContent.length;
            }
        }

        var focusOffset = offset - charPassed;

        var textNode = targetNode.nodeName === 'A' ?
                targetNode.childNodes[0] : targetNode,
            range = document.createRange();
        range.setStart(textNode, focusOffset);
        range.setEnd(textNode, focusOffset);
        range.collapse(false);
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    },

    autoLink: function() {
        var offset = this.getOffset(),
            coobiiBase = "/",
            options = {
                usernameUrlBase: coobiiBase
            },
            lineNodes = this.el.childNodes;
        for (var i=0; i<lineNodes.length; i++) {
            var lineNode = lineNodes[i],
                text = lineNode.textContent,
                escaped = twitterText.htmlEscape(text),
                linked = twitterText.autoLink(escaped, options)
                    .replace(/@<a/g, "<a href='#'>@</a><a");
            this.$(lineNode).html(linked);
        }
        this.resumeOffset(offset);
    },

    keyup: function() {
        if (!this.inComposition) {
            this.autoLink();
            this.updateCharNum();
        }
    },

    paste: function(e) {
        e.preventDefault();
        var text = e.clipboardData.getData("text/plain");
        document.execCommand('insertText', false, text);
    },

    updateCharNum: function() {
        this.trigger('update-char-num', this.getLength());
    },

    clear: function(focus) {
        this.$el.html("<div></div>");
        if (focus) {
            var divNode = this.$el.find('div')[0];
            var range = document.createRange();
            range.selectNodeContents(divNode);
            range.collapse(false);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }
        this.updateCharNum();
    },

    getText: function() {
        return this.el.textContent;
    },

    getLength: function() {
        var text = this.getText();
        return text.length;
    }


});

module.exports = MessageBoxText;
