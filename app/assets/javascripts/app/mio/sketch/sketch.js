var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var CodeMirror = require('../../vendor/codemirror/lib/codemirror.js');
require('../../vendor/codemirror/mode/markdown/markdown.js');
require('../../vendor/codemirror/mode/javascript/javascript.js');

var Sketch = Backbone.View.extend({

    className: "sketch-editor",

    render: function() {
        this.$el.html('<textarea class="sketch-document"></textarea>');
        return this;
    },

    loadEditor: function() {
        var minLines = 20;
        var startingValue = '';
        for (var i = 0; i < minLines; i++) {
            startingValue += '\n';
        }

        var editor = CodeMirror.fromTextArea(this.$('.sketch-document')[0], {
            value: startingValue,
            mode: "markdown",
            viewpostMargin: Infinity,
            lineNumbers: true,
            cursorBlinkRate: 530
        });

        editor.setValue(startingValue);

        editor.on("change", function(cm, changeObj) {
            console.log(changeObj);
        });

        editor.focus();
    }

});

module.exports = Sketch;
