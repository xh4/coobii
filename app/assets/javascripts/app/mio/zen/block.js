var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Block = Backbone.View.extend({

    render: function() {
        this.$el.attr('data-cid', this.cid);
        this.el.view = this;
        return this;
    }

});

module.exports = Block;
