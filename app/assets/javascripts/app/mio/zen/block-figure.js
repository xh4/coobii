var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Block = require('./block.js');

var figureBlockTemplate = require('../../templates/mio/zen/block-figure.hbs');

var FigureBlock = Block.extend({

    tagName: "figure",

    attributes: {
        "contenteditable": false
    },

    className: "figure-block zen-block",

    image: null,
    img: null,

    image_id: null,

    initialize: function(options) {
        _.extend(this, options);
    },

    render: function() {
        Block.prototype.render.call(this);

        this.$el.html( figureBlockTemplate() );
        this.$('.fig_image').prepend( this.img );

        var figure = this;
        this.uploader.fileupload('option', {
            url: "/api/posts/" + this.post.get('id') + "/images",
            done: function(e, data) {
                this.image_id = data.result.id;
            }.bind(this),
            send: function() {
                this.$('.image-upload-progress').show();
                this.$('.fig_f_focus-target').css('cursor', 'wait');
            }.bind(this),
            progress: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                this.$('.image-upload-progress .bar').css('width', progress + "%");
            }.bind(this),
            always: function() {
                this.$('.image-upload-progress').hide();
                this.$('.fig_f_focus-target').css('cursor', 'default');
            }.bind(this)
        });

        this.uploader.fileupload('send', { files: this.image });

        var targetNode = this.$('.fig_f_focus-target')[0],
            figure = this;
        var timer = setInterval(function() {
            if (document.activeElement === targetNode) {
                figure.focus();
            } else {
                figure.blur();
            }
        }, 50);

        return this;

    },

    events: {
        "click": function(e) {
            this.focus();
        },
        "keydown": function(e) {
            if (e.which === 8) {
                e.preventDefault();
                e.stopPropagation();
                this.el.previousSibling.view.focus();
                this.remove();
            }
        }
    },

    focused: false,

    focus: function() {
        if (this.focused) return false;
        this.focused = true;
        this.$el.addClass('active');
        var targetNode = this.$('.fig_f_focus-target')[0];
    },

    blur: function() {
        this.$el.removeClass('active');
        this.focused = false;
    },

    toJSON: function() {
        if (!this.image_id) return null;
        return {
            type: 'figure',
            id: this.image_id
        };
    }

});

module.exports = FigureBlock;
