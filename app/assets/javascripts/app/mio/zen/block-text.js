var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Block = require('./block.js');

var TextBlock = Block.extend({

    tagName: "p",

    className: "text-block zen-block",

    initialize: function() {

    },

    render: function() {
        Block.prototype.render.call(this);

        return this;
    },

    focus: function() {
        var range = document.createRange();
        range.selectNodeContents(this.el);
        range.collapse(false);
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    },

    toJSON: function() {
        var content = this.el.textContent;
        if (/^\s*$/.test(content)) return null;
        return {
            type: 'text',
            content: content
        };
    }

});

module.exports = TextBlock;
