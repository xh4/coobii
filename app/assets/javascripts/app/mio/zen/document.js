var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Tooltip = require('./tooltip.js'),
    TextBlock = require('./block-text.js'),
    FigureBlock = require('./block-figure.js');

require('jquery.fileupload');
var loadImage = require('../../vendor/load-image.js');

var ZenDocument = Backbone.View.extend({

    className: "zen-blocks zen-document",

    attributes: {
        "contenteditable": true
    },

    blocks: [],

    margin: 20,

    initialize: function(options) {
        this.topic = options.topic;
        this.post = options.post;

        this.tooltip = new Tooltip({
            document: this
        });
    },

    render: function() {
        var block = this.renderBlock("text");
        this.$el.append( block.render().el );

        // first text block
        setInterval($.proxy(function() {
            var node = block.el;
            if (!node.parentNode) {
                this.$el.prepend(node);
                var range = document.createRange();
                range.selectNodeContents(node);
                range.collapse(false);
                var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }, this), 50);

        // middle lines
        setInterval($.proxy(function() {
            var middlelines = [],
                sum = 0,
                margin = this.margin;
            _.each(this.el.childNodes, function(node, index, nodes) {
                var height = node.scrollHeight;
                if (index === 0) {
                    var line = [margin / 2, node, 'top'];
                    middlelines.push(line);
                    sum = sum + margin;
                }
                line = [sum + height + margin / 2, node, 'bottom'];
                middlelines.push(line);
                sum += height;
                sum += margin;
            });
            this.middlelines = middlelines;
        }, this), 100);

        // uploader
        var document = this;
        this.uploader = this.$el.fileupload({
            dropZone: this.$el,
            paramName: 'image',
            add: function(e, data) {
                var file = data.files[0];
                if (['image/jpeg',
                     'image/png',
                     'image/gif'].indexOf(file.type) === -1) {
                }  else {
                    loadImage(file, function(img) {
                        document.insertBlock("figure", document.targetLine[1], {
                            image: file,
                            img: img,
                            uploader: document.uploader
                        });
                    }, {
                        crop: false,
                        canvas: false
                    });
                }
            }
        });

        this.listenTo(this.tooltip, 'insert-block', function(type, options) {
            this.insertBlock(type, this.targetLine[1], options);
        });

        setInterval(function() {
            document.fixMissing();
            document.hookMissing();
        }, 50);

        return this;
    },

    events: {
        "click": "onClick",
        "dragover": "dragover",
        "dragleave": "dragleave",
        "drop": "drop",
        "keydown": "keydown",
        "mousemove": "mousemove"
    },

    renderBlock: function(type, options) {
        if (type === 'figure') {
            _.extend(options, {
                uploader: this.uploader,
                post: this.post
            });
        }

        var block;
        switch(type) {
        case "text":
            block = new TextBlock(options);
            break;
        case "figure":
            block = new FigureBlock(options);
            break;
        }
        this.blocks.push(block);
        return block;
    },

    insertBlock: function(type, afterNode, options) {
        var block = this.renderBlock(type, options),
            blockNode = block.render().el;

        var insertTextNoderAfter = $.proxy(function(node) {
            var textBlock = this.renderBlock('text');
            node.parentNode.insertBefore(textBlock.el, node.nextSibling);
        }, this);

        if (afterNode.nodeName !== 'P') {
            insertTextNoderAfter(afterNode);
            afterNode = afterNode.nextSibling;
        }
        afterNode.parentNode.insertBefore(blockNode, afterNode.nextSibling);
        if (!blockNode.nextSibling || blockNode.nextSibling.nodeName !== 'P') {
            insertTextNoderAfter(blockNode);
        }
    },

    minDiff: null,
    targetLine: null,
    mousemove: function(e) {
        var y;
        if (e.type === 'mousemove') {
            y = e.pageY - this.$el.offset().top;
        } else if (e.type === 'dragover') {
            y = e.originalEvent.pageY - this.$el.offset().top;
        }
        if (y < 0) y = 0;

        _.each(this.middlelines, $.proxy(function(line, index, lines) {
            var diff = Math.abs(line[0] - y);
            if (this.minDiff === null) {
                this.minDiff = diff;
            }
            if (diff <= this.minDiff) {
                this.minDiff = diff;
                this.targetLine = line;
            }
        }, this));

        if (this.targetLine) {
            var node = this.targetLine[1];
            if (this.targetLine[2] === 'top') {
                this.tooltip.$el.css('top', 0);
            } else {
                this.tooltip.$el.css('top', node.offsetTop + node.scrollHeight + 2);
            }
        }

        this.minDiff = null;
    },

    dragover: function(e) {
        this.tooltip.$('.zen-tooltip_ctrl').hide();
        this.tooltip.$('.zen-dropline').show();
        this.mousemove(e);
    },

    dragleave: function(e) {
        this.tooltip.$('.zen-tooltip_ctrl').show();
        this.tooltip.$('.zen-dropline').hide();
    },

    drop: function(e) {
        e.preventDefault();
        this.tooltip.$('.zen-tooltip_ctrl').show();
        this.tooltip.$('.zen-dropline').hide();
    },

    onClick: function(e) {

    },

    keydown: function(e) {
        if (e.which === 8) {
            this.onBackSpace(e);
        } else if (e.which === 13) {
            this.onEnter(e);
        }
    },

    onBackSpace: function(e) {
        var selection = window.getSelection(),
            anchorNode = selection.anchorNode,
            focusNode = selection.anchorNode;
        if (focusNode.nodeName === 'P') {
            var p = focusNode,
                prevNode = p.previousSibling;
            if (!prevNode) {
                e.preventDefault();
            } else if (prevNode.nodeName === 'FIGURE') {
                e.preventDefault();
                $(prevNode).find('.fig_f_focus-target').focus();
            }
        }
    },

    onEnter: function(e) {

    },

    fixMissing: function() {
        var document = this;
        _.each(this.el.childNodes, function(node, index, nodes) {
            if (['FIGURE', 'BLOCKQUOTE', 'UL'].indexOf(node.nodeName) !== -1) {
                if (!node.nextSibling || node.nextSibling.nodeName !== 'P') {
                    var block = new TextBlock({
                        document: document
                    });
                    node.parentNode.insertBefore(block.el, node.nextSibling);
                }

            };
        });
    },

    hookMissing: function() {
        var document = this;
        _.each(this.el.childNodes, function(node, index, nodes) {
            if (['P', 'FIGURE'].indexOf(node.nodeName) !== -1) {
                if (!node.view) {
                    var blockType = node.className.match(/(\w+)-block/)[1],
                        block = document.renderBlock(blockType, {
                            el: node
                        });
                    block.render();
                }
            };
        });
    },

    toJSON: function() {
        var arr = [];
        _.each(this.el.childNodes, function(blockNode) {
            var view = blockNode.view,
                json = view.toJSON();
            if (json) arr.push(json);
        });
        return arr;
    }

});

module.exports = ZenDocument;
