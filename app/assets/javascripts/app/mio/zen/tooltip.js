var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var tooltipTemplate = require('../../templates/mio/zen/tooltip.hbs');

require('jquery.fileupload');
var loadImage = require('../../vendor/load-image.js');

var Tooltip = Backbone.View.extend({

    className: "zen-tooltip",

    render: function() {
        this.$el.html( tooltipTemplate() );

        var tooltip = this;
        this.$('input').fileupload({
            dropZone: this.$('input'),
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            add: function(e, data) {
                var file = data.files[0];
                loadImage(file, function(img) {
                    tooltip.trigger('insert-block', 'figure', {
                        image: file,
                        img: img
                    });
                }, {
                    crop: false,
                    canvas: false
                });
            }
        });

        return this;
    }

});

module.exports = Tooltip;
