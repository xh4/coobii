var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Document = require('./document.js');

var Zen = Backbone.View.extend({

    className: "zen-editor",

    initialize: function(options) {
        this.document = new Document(options);
    },

    render: function() {
        this.$el.html( this.document.render().el );
        this.document.$el.before( this.document.tooltip.render().el );
        return this;
    },

    toJSON: function() {
        return this.document.toJSON();
    }
});

module.exports = Zen;
