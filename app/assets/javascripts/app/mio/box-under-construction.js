var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Box = require('./box.js');

var template = require('../templates/mio/box-under-construction.hbs');

var BoxUC = Box.extend({

    className: 'mio-box-under-construction mio-box',

    render: function() {
        this.$el.html( template() );
    },

    events: {
        "click .act-close-box": "closeBox"
    }

});

module.exports = BoxUC;
