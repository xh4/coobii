var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Box = require('./box.js'),
    Zen = require('./zen/zen.js'),
    // Sketch = require('./sketch/sketch.js'),
    Alert = require('../views/alert.js'),
    topicBoxTemplate = require('../templates/mio/box-topic.hbs');

var TopicStore = require('../stores/topic-store'),
    ContentStore = require('../stores/content-store');

var TopicBox = Box.extend({

    className: "topic-box mio-box-topic mio-box",

    initialize: function(options) {
        this.topic = options.topic;
        this.post = options.post;
    },

    render: function() {
        Box.prototype.render.call(this);

        this.$('.mio-box_cnt').html( topicBoxTemplate() );

        this.zen = new Zen({
            topic: this.topic,
            post: this.post
        });
        this.$('.topic-box-editor').html( this.zen.render().el );

        // var sketch = new Sketch;
        // this.$('.topic-box-editor').html( sketch.render().el );
        // setTimeout(sketch.loadEditor);

        return this;
    },

    events: {
        "click .act-create-content": "createContent",
        "click .act-close-box": "closeBox"
    },

    getTitle: function() {
        return this.$('.topic-box-header input').val();
    },

    createContent: function() {
        var blocks = this.zen.toJSON();

        this.showSpinner();
        this.showOverlay();

        $.ajax({
            type: "PUT",
            url: "/api/topics/" + this.topic.id,
            data: {
                title: this.getTitle()
            },
            context: this
        }).done(function(t) {
            this.topic.set(t);
        }).then(function() {
            return $.ajax({
                type: "PUT",
                url: "/api/posts/" + this.post.get('id'),
                data: {
                    blocks: JSON.stringify(blocks)
                },
                context: this
            });
        }).done(function(p) {
            this.post.set(p);
        }).then(function() {
            return $.ajax({
                type: "post",
                url: "/api/posts/" + this.post.get('id') + "/publish",
                context: this
            });
        }).done(function(p) {

            this.post.set(p);
            this.topic.set(p.topic);

            ContentStore.add(this.post)
                .trigger('new-composition', this.post)
                .trigger('new-composition:post', this.post);
            TopicStore.add(this.topic)
                .trigger('new-composition', this.topic);

            this.trigger('post-done');

        }).fail(function() {
            alert('error');
        }).always(function() {
            this.hideSpinner();
            this.hideOverlay();
        });

    }

});

module.exports = TopicBox;
