var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Showcase = require('../views/showcase.js'),
    Coobii = require('../coobii.js');

require('jquery.transit');

var Box = Backbone.View.extend({

    className: 'mio-box',

    render: function() {
        this.$el.html("<div class='mio-box_cnt'></div> \
                      <div class='mio-box_overlay'></div>");
        return this;
    },

    closeBox: function() {
        var box = this;

        var showcaseState = Coobii.getState('showcase');
        showcaseState.set('display', 'hide');

        this.$el.transition({
            'opacity': 0,
            'scale': 0.7
        }, function() {
            box.remove();
        });
    },

    show: function(fn) {
        this.$el.transition({
            'opacity': 1,
            'scale': 1
        }, function() {
            if (fn) fn();
        });
    },

    showOverlay: function() {
        this.$('.mio-box_overlay').show();
    },

    hideOverlay: function() {
        this.$('.mio-box_overlay').hide();
    },

    showSpinner: function() {
        this.$('.spinner').show();
    },

    hideSpinner: function() {
        this.$('.spinner').hide();
    }

});

module.exports = Box;
