var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Box = require('./box.js'),
    Header = require('./header.js'),
    Text = require('./box-message-text.js'),
    Message = require('../models/message.js'),
    Alert = require('../views/alert.js'),
    messageBoxTemplate = require('../templates/mio/box-message.hbs');

require('jquery.fileupload');
var loadImage = require('../vendor/load-image.js');

var ContentStore = require('../stores/content-store'),
    CurrentUser = require('../states/current-user');

var MessageBox = Box.extend({

    className: 'message-box mio-box-message mio-box',

    initialize: function(options) {
        this.owner = options.owner;
        this.maxImageSize = options.maxImageSize;
    },

    template: messageBoxTemplate,

    image: null,

    render: function() {
        Box.prototype.render.call(this);
        this.$('.mio-box_cnt').html( this.template() );

        if (this.owner === 'mio') {
            this.header = new Header;
            this.$('.mio-box_cnt').prepend(this.header.render().el);
        }

        this.renderText();
        this.loadUploader();
    },

    renderText: function() {
        this.text = new Text;
        this.$('.content').prepend(this.text.el);
        this.listenTo(this.text, 'update-char-num', this.renderCharNum);
    },

    loadUploader: function() {
        var box = this;
        this.$('.image-selector').fileupload({
            dropzone: this.$('.image-selector'),
            paramName: 'image',
            url: '/api/messages',
            add: function(e, data) {
                var file = data.files[0];
                if (["image/jpeg",
                     "image/png",
                     "image/gif"].indexOf(file.type)=== -1) {

                } else {
                    box.image = file;
                    loadImage(file, function(img) {
                        box.$('.extra').css('display', 'block');
                        box.$('.extra .image').css('display', 'inline-block')
                            .html(img).append("<a class=\"dismiss\"><i></i></a>");
                        box.$('.add-image').addClass('has-image');

                        setTimeout(function() {
                            box.focusText();
                        }, 500);
                    }, {
                        maxWidth: box.maxImageSize,
                        crop: false,
                        canvas: false
                    });
                }
            }
        });
    },

    events: {
        "focus .text": "focus",
        "blur .text": "blur",
        "click .new": "onClickNew",
        "click .act-create-content": "createContent",
        "click .act-close-box": "closeBox",
        "click .add-image": "addImage",
        "click .extra .image .dismiss": "removeImage",
        "mouseover .menu": "disableBlur",
        "mouseleave .menu": "enableBlur"
    },

    renderCharNum: function(charnum) {
        var rest = 140 - charnum;

        this.$(".length-limit").html(rest);

        if (rest < 0 || rest === 140) {
            this.$(".act-create-content")
                .removeClass("btn-blue")
                .addClass("btn-white");
        } else {
            this.$(".act-create-content")
                .removeClass("btn-white")
                .addClass("btn-blue");
        }

        if (rest > 20) {
            this.$(".length-limit").css("color", "#999");
        } else if (rest <= 20 && rest > 10) {
            this.$(".length-limit").css("color", "#5c0002");
        } else {
            this.$(".length-limit").css("color", "#d40d12");
        }

    },

    onClickNew: function(e) {
        this.$el.addClass('expand');
        this.text.$el.focus();
    },

    focus: function() {
        this.$el.addClass('active');
    },

    blur: function() {
        this.$el.removeClass('active');
        if (!this.noBlur && !this.image && this.text.getLength() === 0) {
            this.collapse();
        }
        if (document.activeElement === this.text.el) {
            this.text.el.blur();
        }
    },

    collapse: function() {
        this.$el.removeClass('expand');
    },

    clear: function() {
        this.removeImage();
        this.text.clear(false);
        this.$el.removeClass('active');
        this.collapse();
        this.text.el.blur();
    },

    enableBlur: function() {
        this.noBlur = false;
    },

    disableBlur: function() {
        this.noBlur = true;
    },

    focusText: function() {
        var node = this.text.$('div').last()[0],
            selection = window.getSelection(),
            range = document.createRange();
        range.selectNode(node);
        range.collapse(false);
        selection.removeAllRanges();
        selection.addRange(range);
    },

    removeImage: function() {
        this.$('.extra .image').css('display', 'none').html("");
        this.$('.add-image').removeClass('has-image');
        if (this.$('.extra .location').css('display') === 'none') {
            this.$('.extra').css('display', 'none');
        }
        this.image = null;
        this.focusText();
    },

    createContent: function() {
        var box = this,
            text = this.text.getText(),
            length = this.text.getLength();

        if (/^\s+$/.test(text)) {
            return;
        }

        this.showOverlay();
        this.showSpinner();

        this.trigger('post-start');

        var done = function(res) {
            this.trigger('post-done');
            var message = new Message(res);
            message.user = CurrentUser;
            ContentStore.add(message)
                .trigger('new-composition', message)
                .trigger('new-composition:message', message);
        };

        var fail = function(err, statusCode, statusText) {
            this.trigger('post-fail', statusText);
        };

        var always = function() {
            this.hideOverlay();
            this.hideSpinner();
            this.trigger('post-always');
        };

        if (this.image) {
            this.$('.image-selector').fileupload('option', {
                formData: {
                    text: text
                },
                done: function(e, data) {
                    done.call(box, data.result);
                },
                fail: function(e, data) {
                    fail.call(box,
                              data.jqXHR.responseJSON,
                              data.jqXHR.status,
                              data.errorThrown);
                },
                always: function() {
                    always.call(box);
                }
            });
            this.$('.image-selector').fileupload('send', {files: this.image});
        } else {
            $.ajax({
                url: '/api/messages',
                type: 'POST',
                dataType: 'json',
                data: {
                    text: text
                }
            }).done(function(res) {
                done.call(box, res);
            }).fail(function(jqXHR, textStatus, errThrown) {
                fail.call(box,
                          jqXHR.responseJSON,
                          jqXHR.status,
                          errThrown);
            }).always(function() {
                always.call(box);
            });
        }
    }

});

module.exports = MessageBox;
