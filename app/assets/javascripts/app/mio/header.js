var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Me = require('../me.js'),
    UserAnchor = require('../views/users/anchor.js');


var MioHeader = Backbone.View.extend({

    className: 'mio-header',

    render: function() {
        var userAnchor = new UserAnchor({
            model: Me
        });
        this.$el.html( userAnchor.render().el );
        return this;
    }

});

module.exports = MioHeader;
