var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Coobii = require('../../coobii.js');

var Viewer = Backbone.View.extend({

    className: 'content-viewer',

    getShowcaseState: function() {
        return Coobii.getState('showcase');
    }

});

module.exports = Viewer;
