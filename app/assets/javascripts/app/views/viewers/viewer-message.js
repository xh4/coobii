var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Viewer = require('./viewer.js'),
    Message = require('../../models/message.js'),
    MessageView = require('../timelines/message.js'),
    CommentArea = require('../comments/comment-area.js'),
    messageViewerTemplate = require('../../templates/viewers/viewer-message.hbs');

require('jquery.transit');

var MessageViewer = Viewer.extend({

    className: 'message-viewer content-viewer viewer',

    model: Message,

    initialize: function(options) {
        _.extend(this, options);
    },

    render: function() {
        this.$el.html( messageViewerTemplate() );

        this.contentView = new MessageView({
            tagName: 'div',
            model: this.model,
            viewPoint: 'showcase'
        });
        this.contentView.$el.attr('data-view-point', 'showcase');

        this.$('.viewer-cnt').css({
            'opacity': 0,
            'scale': 0.7
        }).append( this.contentView.render().el );

        this.commentArea = new CommentArea({
            model: this.model
        });
        this.$('.viewer-cnt').append( this.commentArea.render().el );
        this.contentView.$el.css({
            'border-radius': '5px 5px 0 0'
        });

        this.listenTo(this.contentView, 'click-comment', function() {
            this.commentArea.focus();
        });

        return this;
    },

    events: {
        "click .act-close": "close",
        "click .act-back": "back"
    },

    close: function(e) {
        var state = this.getShowcaseState();
        state.set('display', 'hide');
        this.hideContent();
    },

    showContent: function() {
        var viewer = this;

        this.$('.viewer-cnt').transition({
            'opacity': 1,
            'scale': 1
        }, function() {
            viewer.onDisplay();
        });
    },

    hideContent: function() {
        this.$('.viewer-cnt').transition({
            'opacity': 0,
            'scale': 0.7
        });
    },

    onDisplay: function() {
        this.commentArea.loadComments(function() {
            if (this.$('.comment').length !== 0) {
                this.$('.add-comment').css('border-radius', 0);
            }
        }.bind(this));
        if (this.focusComment) {
            this.commentArea.focus();
        }
        this.listenToOnce(this.model, 'destroy', this.onceDestroy);
    },

    hook: function() {},

    back: function() {
        this.close();
    },

    onceDestroy: function() {
        this.close();
    }

});

module.exports = MessageViewer;
