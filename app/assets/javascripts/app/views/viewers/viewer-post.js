var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Viewer = require('./viewer.js'),
    TopicView = require('../contents/topic.js'),
    PostView = require('../contents/post.js'),
    Topic = require('../../models/topic.js'),
    CommentArea = require('../comments/comment-area.js');

var postViewerTemplate = require('../../templates/viewers/viewer-post.hbs');

require('jquery.transit');

var PostViewer = Viewer.extend({

    className: 'post-viewer content-viewer viewer',

    initialize: function(options) {
        _.extend(this, options);
    },

    render: function() {
        this.$el.html( postViewerTemplate() );

        return this;
    },

    showContent: function() {
        var topic = new Topic(this.model.get('topic')),
            topicView = new TopicView({
                model: topic,
                viewPoint: "showcase",
                initPost: this.model
            });
        topicView.render();

        var wrapper = this.$('.topic-w');

        wrapper.html( topicView.el )
            .css({
                'margin-top': '200px',
                'opacity': 0
            })
            .transition({
                'margin-top': '100px',
                'opacity': 1
            });

        setInterval(this.adjastContent.bind(this), 100);

        topicView.$('.act-close').on('click', this.close.bind(this));
    },

    hideContent: function() {
        this.$('.topic-w').transition({
            'margin-top': '200px',
            'opacity': 0
        });
        if (!this.boxMode) {
            this.$('.topic-header').transition({
                'top': '240px'
            });
        }
    },

    boxMode: true,

    adjastContent: function() {
        var frameHeight = $('#container').height(),
            wrapper = this.$('.topic-w'),
            marginTop = 100,
            headerHeight = 40,
            contentHeight = this.$('.topic-w').height();
        if (contentHeight <= frameHeight - 40) {
            if (this.boxMode) return;
            this.boxMode = true;

            wrapper.transition({
                'margin-top': marginTop+"px"
            }, function() {
                this.$('.topic-header').css({
                    'position': 'relative',
                    'top': 0,
                    'z-index': 'initial'
                });
                this.$('.topic').css('padding-top', 0);
            }.bind(this));

            this.$('.topic-header').transition({
                'top': marginTop+headerHeight+"px"
            });

        } else {
            if (!this.boxMode) return;
            this.boxMode = false;

            wrapper.transition({
                'margin-top': 0
            }, function() {
                this.$('.topic-header').css({
                    'position': 'fixed',
                    'top': '40px',
                    'z-index': '100'
                });
                this.$('.topic').css('padding-top', headerHeight+"px");
            }.bind(this));

            this.$el.css('padding-bottom', 0);
        }
    },

    events: {
        "click .act-view-user": "back"
    },

    close: function(e) {
        var state = this.getShowcaseState();
        state.set('display', 'hide');
        this.hideContent();
    },

    back: function(e) {
        this.close();
    },

    hook: function() {

    }
});

module.exports = PostViewer;
