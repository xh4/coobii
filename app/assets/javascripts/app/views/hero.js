var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

require('jquery.transit');

var Hero = Backbone.View.extend({

    className: 'hero',

    getTransEndAttrs: function() {
        return 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
    },

    acceptTransEvent: false,

    hide: function(fn) {
        this.acceptTransEvent = true;
        this.$el.addClass('hide').one(this.getTransEndAttrs(), function(e) {
            if (e.target !== this.el || !this.acceptTransEvent) return;
            this.acceptTransEvent = false;
            if (fn) fn();
        }.bind(this));
    },

    show: function(fn) {
        this.acceptTransEvent = true;
        this.$el.removeClass('hide').one(this.getTransEndAttrs(), function(e) {
            if (e.target !== this.el || !this.acceptTransEvent) return;
            this.acceptTransEvent = false;
            if (fn) fn();
        }.bind(this));
    }
});

module.exports = Hero;
