var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var imagePickerTemplate = require('../templates/image-picker.hbs');

require('jquery.transit');
require('jquery.fileupload');
require('jquery.ui.slider');

var loadImage = require('../vendor/load-image.js');

var ImagePicker = Backbone.View.extend({

    className: 'img-picker',

    diameter: 200,

    template: null,

    imageFile: null,
    img: null,
    imgOrigWidth: 0,
    imgOrigHeight: 0,
    minPercentage: 0,
    maxPercentage: 0,
    percentage: 0,
    top: 0,
    left: 0,

    imageFileChanged: false,
    imageDisplayChanged: false,

    initialize: function(options) {
        this.initAvatar = options.initAvatar;
    },

    render: function() {

        this.$el.html( imagePickerTemplate() );

        this.$('input').fileupload({
            dropzone: this.$el,
            add: function(e, data) {
                var file = data.files[0];
                if (["image/jpeg",
                     "image/png",
                     "image/gif"].indexOf(file.type) === -1) {

                } else {
                    this.imageFile = file;
                    loadImage(file, function(img) {
                        this.imageFileChanged = true;
                        this.loadImage(img);
                    }.bind(this), {
                        crop: false,
                        canvas: false
                    });
                }
            }.bind(this)
        });

        if (this.initAvatar) {
            this.$el.addClass('loading-image');
            var img = new Image;
            img.src = this.initAvatar.original_url;

            var timer = setTimeout(function() {
                img = null;
                this.$el.removeClass('loading-image');
            }.bind(this), 30000);

            img.onload = function() {
                clearTimeout(timer);
                this.$el.removeClass('loading-image');
                this.loadImage(img, this.initAvatar);
            }.bind(this);
        }

        return this;
    },

    loadImage: function(img, display) {
        this.$('.img-picker_image').html(img);
        this.img = img;
        this.imgOrigWidth = img.width;
        this.imgOrigHeight = img.height;

        if (img.height < img.width) {
            this.minPercentage = this.diameter / img.height;
        } else {
            this.minPercentage = this.diameter / img.width;
        }

        if (this.minPercentage > 1.2) {
            this.maxPercentage = this.minPercentage * 1.2;
        } else {
            this.maxPercentage = 1.2;
        }

        this.$('.img-picker_slider').slider({
            change: function(e, ui) {
                this.onSlide(e, ui);
                this.moveCorrect();
                this.imageDisplayChanged = true;
            }.bind(this),

            slide: this.onSlide.bind(this)
        });

        this.$('.img-picker_slider').slider('option', 'value', 0);

        if (!display) {
            this.imageDisplayChanged = true;
            this.centerImage();
        } else {
            this.left = -display.offset[0] + 10;
            this.top = -display.offset[1] + 43;
            this.percentage = display.scale / 100;

            this.changeTrackUI(display.scale);
            this.changeHandlerUI(display.scale);

            this.img.width = Math.round( this.imgOrigWidth * this.percentage );
            this.img.height = Math.round( this.imgOrigHeight * this.percentage );
            $(this.img).css({
                left: this.left,
                top: this.top
            });
        }
    },

    changeTrackUI: function(value) {
        var width = value + "%";
        this.$('.track-before').css('width', width);
    },

    changeHandlerUI: function(value) {
        var left = value + "%";
        this.$('.ui-slider-handle').css('left', left);
    },

    onSlide: function(e, ui) {
        var value = ui.value;
        this.changeTrackUI(value);
        var d = value / 100 * (this.maxPercentage - this.minPercentage);
        this.percentage = this.minPercentage + d;

        var newWidth = Math.round(this.imgOrigWidth * this.percentage),
            newHeight = Math.round(this.imgOrigHeight * this.percentage),
            centerPoint = this.getCenterPoint(),
            dx = newWidth - this.img.width,
            dy = newHeight - this.img.height,
            dx = dx * centerPoint[0] / this.img.width,
            dy = dy * centerPoint[1] / this.img.height;
        this.left = this.left - dx;
        this.top = this.top - dy;
        $(this.img).css({
            left: this.left,
            top: this.top
        });

        this.img.width = newWidth;
        this.img.height = newHeight;
    },

    centerImage: function() {
        var boxWidth = 220,
            boxHeight = 285,
            width = this.img.width,
            height = this.img.height;
        this.top = Math.round((boxHeight - height) / 2);
        this.left = Math.round((boxWidth - width) / 2);
        $(this.img).css({
            left: this.left,
            top: this.top
        });
    },

    getCenterPoint: function() {
        return [ 10 - this.left + this.diameter / 2,
                 43 - this.top + this.diameter / 2 ];
    },

    moveCorrect: function() {
        var imgWidth = this.img.width,
            imgHeight = this.img.height,
            offsetLeft = this.left,
            offsetRight = offsetLeft + imgWidth,
            offsetTop = this.top,
            offsetBottom = offsetTop + imgHeight;
        if (offsetTop > 43) {
            this.top = 43;
        }
        if (offsetBottom < this.diameter + 43) {
            this.top = this.diameter + 43 - imgHeight;
        }
        if (offsetLeft > 10) {
            this.left = 10;
        }
        if (offsetRight < this.diameter + 10) {
            this.left = this.diameter + 10 - imgWidth;
        }
        this.$('.img-picker_image img').transition({
            top: this.top,
            left: this.left
        });
    },

    events: {
        "mousedown .img-picker_front_middle": function(e) {
            if (!this.img) return;
            this.enableDrag = true;
            this.pageX = e.pageX;
            this.pageY = e.pageY;
        },
        "mouseup .img-picker_front_middle": function(e) {
            if (!this.img) return;
            this.enableDrag = false;
            var dx = e.pageX - this.pageX,
                dy = e.pageY - this.pageY;
            this.top = this.top + dy;
            this.left = this.left + dx;
            this.moveCorrect();
            this.imageDisplayChanged = true;
        },
        "mousemove .img-picker_front_middle": function(e) {
            if (!this.enableDrag || !this.img) return;
            var dx = e.pageX - this.pageX,
                dy = e.pageY - this.pageY,
                top = this.top + dy,
                left = this.left + dx;
            $(this.img).css({
                top: top,
                left: left
            });
        },
        "mouseleave .img-picker_front_middle": function(e) {
            if (!this.enableDrag || !this.img) return;
            this.enableDrag = false;
            this.moveCorrect();
        },
        "click .btn-act-cancel": function(e) {
            this.trigger('cancel');
        },
        "click .btn-act-ok": function(e) {
            var imagePicker = this;
            this.trigger('cancel');
            if (this.imageFileChanged) {
                var reader = new FileReader;
                reader.onload = function(e) {
                    this.imgSrc = e.target.result;
                    this.trigger('change-image', this.imgSrc);

                    this.trigger('change-display', this.getAttributes());
                }.bind(this);
                reader.readAsDataURL(this.imageFile);
            } else {
                if (this.imageDisplayChanged) {
                    this.trigger('change-display', this.getAttributes());
                }
            }
        }
    },

    getAttributes: function() {
        var scale = this.percentage * 100,
            offsetX = Math.abs(10 - this.left),
            offsetY = Math.abs(43 - this.top);

        return  {
            scale: scale,
            offset: [offsetX, offsetY],
            width: this.imgOrigWidth,
            height: this.imgOrigHeight
        };
    },

    request: function(options) {
        var imagePicker = this,
            attrs = this.getAttributes();

        var avatarForm = {
            "avatar_attributes": JSON.stringify({
                scale: attrs["scale"],
                offset_x: attrs["offset"][0],
                offset_y: attrs["offset"][1]
            })
        };

        var formData = _.extend(avatarForm, options.formData);

        if (this.imageFileChanged) {
            this.$("input").fileupload('option', {
                url: options.url,
                paramName: "avatar",
                type: options.type,
                formData: formData,
                done: function(e, data) {
                    this.imageFileChanged = false;
                    this.imageDisplayChanged = false;
                    if (options.done) options.done(data.result);
                },
                fail: function(e, data) {
                    if (options.fail) options.fail(data.jqXHR.responseJSON);
                },
                always: function(e, data) {
                    if (options.always) options.always();
                }
            });
            this.$("input").fileupload('send', {
                files: this.imageFile
            });
        } else {
            $.ajax({
                url: options.url,
                type: options.type,
                data: formData
            }).done(function(res) {
                if (options.done) options.done(res);
            }).fail(function(jqXHR) {
                if (options.fail) options.fail(jqXHR.responseJSON);
            }).always(function() {
                if (options.always) options.always();
            });
        }
    }
});

module.exports = ImagePicker;
