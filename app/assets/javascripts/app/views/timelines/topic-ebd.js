var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var template = require('../../templates/timelines/topic-ebd.hbs');

var TopicEbdView = Backbone.View.extend({

    render: function() {
        this.$el.html( template( this.model.toJSON() ) );
        return this;
    }

});

module.exports = TopicEbdView;
