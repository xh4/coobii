var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var ContentView = require('./content.js'),
    Post = require('../../models/post.js'),
    Topic = require('../../models/topic.js'),
    TopicEbdView = require('./topic-ebd'),
    User = require('../../models/user.js'),
    ContentFooter = require('./content-footer.js'),
    UserAnchor = require('../users/anchor.js'),
    Me = require('../../me.js'),
    Coobii = require('../../coobii.js');

var postTemplate = require('../../templates/timelines/post.hbs');

var PostView = ContentView.extend({

    tagName: 'li',

    className: 'post post-tl content content-tl',

    attributes: {
        'data-content-type': 'post'
    },

    model: Post,

    initialize: function() {

    },

    render: function() {
        this.$el.html( postTemplate() );

        var topic = new Topic( this.model.get('topic') );
        this.topicView = new TopicEbdView({
            model: topic
        });
        this.$el.prepend( this.topicView.render().el );

        this.contentFooter = new ContentFooter({
            model: this.model
        });
        this.$el.append( this.contentFooter.render().el );

        this.renderBlocks();

        return this;
    },

    renderBlocks: function() {
        var blocks = this.model.get('blocks');
        _.each(blocks, function(block) {
            var type = block.type,
                caped = type.charAt(0).toUpperCase() + type.slice(1),
                el = $('<div class="post-block"></div>');
            el.addClass('post-block-' + type);
            el = this['render' + caped + "Block"](el, block);
            this.$('.post-tl-body').append(el);
        }.bind(this));
    },

    renderTextBlock: function(el, block) {
        el.html(block.content);
        return el;
    },

    renderFigureBlock: function(el, block) {
        var img = new Image;
        img.src = block.url;
        el.html(img);
        return el;
    },

    events: {
        "click .topic-ebd-pg-name": "onClickPageName",
        "click .topic-ebd-title": "onClickTopicTitle",
        "click .topic-ebd-header-toggler": "onClickHeaderToggler"
    },

    onClickPageName: function(e) {

    },

    onClickTopicTitle: function(e) {
        e.preventDefault();
        ContentView.prototype.viewContent.call(this);
    },

    onClickHeaderToggler: function(e) {
        alert('toggler header');
    },

    hook: function() {
        var attr = {
            id: this.$el.data('id'),
            cid: this.$el.data('content-id'),
            type: 'post'
        };

        this.model = new Post(attr);

        this.footer = new ContentFooter({
            el: this.$('.content-footer')
        });
        this.footer.hook();
    }


});

module.exports = PostView;
