var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Coobii = require('../../coobii.js');

var contentsTemplate = require('../../templates/timelines/contents.hbs');

var Content = {
    "message": require('../../models/message.js'),
    "post": require('../../models/post.js')
};

var ContentView = {
    "message": require('./message.js'),
    "post": require('./post.js')
};

var ContentStore = require('../../stores/content-store');

var ContentsView = Backbone.View.extend({

    className: 'contents',

    initialize: function(options) {

        /**
         *
         * type: user|timeline|shares|marks
         * user
         * limit
         * autoLoad
         *
         * construct:
         * - url
         *
         */

        _.extend(this, options);

        if (this.type === 'user') {
            this.url = '/api/contents';
        } else if (this.type === 'timeline') {
            this.url = '/api/timeline';
        } else if (this.type === 'likes') {
            this.url = '/api/likes';
        } else if (this.type === 'marks') {
            this.url = '/api/marks';
        }

        this.stream = [];

        this.listenTo(ContentStore,
                      'new-composition',
                      this.addComposition);
        this.listenTo(ContentStore, 'remove', this.onRemove);
    },

    page: 1,

    limit: 10,

    viewMode: 'guest',

    url: "",

    stream: [],

    zeroStateText: "暂无任何内容",

    render: function() {
        this.$el.html( contentsTemplate() );
        return this;
    },

    events: {
        "click .load-more-btn": "loadMore"
    },

    addComposition: function(content) {
        var type = content.get("type");
        var contentView = new ContentView[type]({
            model: content,
            viewPoint: 'timeline'
        });
        contentView.render();

        if (this.stream.length === 0) {
            this.clearBlankState();
        }

        this.stream.push(content.id);

        contentView.$el.css('display', 'none').appendTo('body');
        var height = contentView.$el.height();
        contentView.$el.detach()
            .css({
                'display': 'block',
                'height': 0,
                'opacity': 0
            });
        this.$('ol').prepend(contentView.el);
        contentView.$el.animate({
            'height': height
        }, 400, function() {
            $(this).css('height', 'auto').animate({ 'opacity': 1 }, 1000);
        });

    },

    onRemove: function(content) {
        this.stream = _.without(this.stream, content.get('id'));
        if (this.stream.length === 0) {
            this.renderBlankState();
        }
    },

    clearBlankState: function() {
        this.$('.contents-blank-state').remove();
    },

    renderBlankState: function() {
        $('<p class="contents-blank-state">').html( this.zeroStateText ).appendTo( this.$('.indication') );
        this.$('.indication').show();
    },

    renderAndLoad: function(fn) {
        this.render();
        this.loadPage(fn);
    },

    loadPage: function(fn) {
        var options = {
            user_id: this.user.id,
            url: this.url,
            limit: this.limit,
            page: this.page,
            type: this.contentType
        };
        ContentStore.loadContents(options, function(contents, pagination) {

            _.each(contents, function(c) {

                var type = c.get('type');
                var contentView = new ContentView[type]({
                    model: c,
                    viewPoint: 'timeline'
                });
                this.stream.push(c.get('id'));
                this.$('ol').append( contentView.render().el );
            }.bind(this));

            this.$('.contents-pagination').html(pagination);

            if (this.$('.contents-pagination .next').length === 0) {
                this.$('.load-more-btn').remove();
            }

            if (contents.length === 0) {
                this.renderBlankState();
            }

            if (fn) fn();
        }.bind(this));
    },

    hook: function() {
        var contentsView = this;
        this.$('.content').each(function(idx, li) {
            var type = $(li).data('content-type');
            var contentView = new ContentView[type]({
                el: li,
                viewPoint: 'timeline'
            });
            contentView.hook();
            ContentStore.add(contentView.model);
            contentsView.stream.push(contentView.model.get('id'));
        });
    },

    loadMore: function() {
        this.$('.load-more-btn .spinner').css('display', 'inline-block');
        this.page++;
        this.loadPage(function() {
            this.$('.load-more-btn .spinner').hide();
        }.bind(this));
    }

});

module.exports = ContentsView;
