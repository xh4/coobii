var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var ContentView = require('./content'),
    User = require('../../models/user'),
    Message = require('../../models/message'),
    Menu = require('../contents/content-menu'),
    UserAnchor = require('../users/anchor'),
    Me = require('../../me'),
    Coobii = require('../../coobii');

var MessageView = ContentView.extend({

    tagName: 'li',

    className: 'message content',

    attributes: {
        'data-content-type': 'message'
    },

    model: Message,

    template: require('../../templates/timelines/message.hbs'),

    imageAttr: null,

    expanded: false,

    initialize: function(options) {
        this.viewPoint = options.viewPoint;
    },

    render: function() {

        this.$el.html( this.template(this.model.toJSON() ) );

        this.$('.msg_tx').html(this.model.get('cooked'));

        // classname
        if (this.viewPoint === 'timeline') this.$el.addClass('message-tl content-tl');

        // user
        if (this.model.get('user').id === Me.get('id')) {
            this.user = Me;
        } else {
            this.user = new User(this.model.get('user'));
        }

        // user anchor
        var userAnchor = new UserAnchor({
            model: this.user
        });
        this.$('.msg_h').prepend(userAnchor.render().el);

        // menu
        this.menu = new Menu({
            model: this.model
        });
        this.$('.msg_menu-right').html(this.menu.render().el);
        this.listenMenuEvents();

        // connect
        if (this.model.get('shares_count') === 0 &&
            this.model.get('likes_count') === 0 &&
            this.model.get('marks_count') === 0) {
            this.$('.msg_con').css('display', 'none');
        } else {
            this.$('.msg_con_count').each(function(index, el) {
                if ($(el).find('strong').html() === '0') {
                    $(el).remove();
                }
            });
        }

        // image
        var image = this.model.get('image');
        if (image) {
            $('<img>')
                .attr('src', image.url)
                .appendTo(this.$('.msg_img'));

            if (this.$el.data('view-point') !== 'showcase') {
                this.calcImageAttr();
                this.$('.msg_img').css({
                    'height': this.imageAttr.frameHeight,
                    'width': this.imageAttr.imgWidth
                });
                this.$('.msg_img img').css({
                    'top': this.imageAttr.top,
                    'width': this.imageAttr.imgWidth
                });
            }
        }

        this.listenToOnce(this.model, 'destroy', this.onceDestroyDone);

        return this;
    },

    hook: function() {
        this.user = this.hookUser();

        var userAnchor = new UserAnchor({
            el: this.$('.msg_h > .user-a')
        });
        userAnchor.hook();

        var attr = {
            id: this.$el.data('id'),
            cid: this.$el.data('content-id'),
            type: 'message'
        };

        var img = this.$('.msg_img img'),
            imgExists = img.length !== 0;
        if (imgExists) {
            attr.image = {
                id: img.data('image-id'),
                width: img.data('image-width'),
                height: img.data('image-height')
            };
        }

        this.model = new Message(attr);
        this.model.user = this.user;

        this.hookMenu(); // after model

        if (imgExists) {
            this.calcImageAttr();
        }

        this.listenToOnce(this.model, 'destroy', this.onceDestroyDone);
    },

    events: {
        "click": function() {
            if (this.$el.data('view-point') === 'showcase') return;
            this.toggleSize();
        },
        "click .msg_tx a": function(e) {
            e.stopPropagation();
        },
        "click .act-view-content": "viewContent"
    },

    toggleSize: function() {
        if (this.expanded) {
            this.collapse();
        } else {
            this.expand();
        }
    },

    expand: function() {
        var message = this,
            height = this.$('.msg_expand-cnt').height();

        // fix the fucking chrome
        this.$('.msg_info_report').css('float', 'right');

        this.$('.msg_expand-cnt').css({
            'position': 'relative',
            'display': 'block',
            'height': 0
        }).animate({
            height: height
        }, 200, function() {
            message.$('.msg_expand-cnt').css('height', 'auto');
            if (message.model.get('image')) {
                message.expandImage();
            };
        });

        this.$('.act-expand').css('display', 'none');
        this.$('.act-collapse').css('display', 'inline');

        this.$el.addClass('expanded');
        this.$el.next().addClass('expand-before');
        this.$el.prev().addClass('expand-after');

        this.expanded = true;
    },

    collapse: function() {
        var message = this;
        this.$('.msg_expand-cnt').animate({
            'height': 0
        }, 200, function() {
            message.$('.msg_expand-cnt').css({
                'height': 'auto',
                'display': 'none'
            });
            message.$('.msg_info_report').css('float', 'none');
            if (message.model.get('image')) {
                message.collapseImage();
            };
        });

        this.$('.act-collapse').css('display', 'none');
        this.$('.act-expand').css('display', 'inline');

        this.$el.removeClass('expanded');
        this.$el.next().removeClass('expand-before');
        this.$el.prev().removeClass('expand-after');

        this.expanded = false;
    },

    expandImage: function() {
        var height = this.imageAttr.imgHeight;
        this.$('.msg_img').animate({
            'height': height
        }, 200);
        this.$('.msg_img img').animate({
            'top': 0
        }, 200);
    },

    collapseImage: function() {
        var height = this.imageAttr.frameHeight,
            top = this.imageAttr.top;
        this.$('.msg_img').animate({
            'height': height
        }, 200);
        this.$('.msg_img img').animate({
            'top': top
        }, 200);
    },

    calcImageAttr: function() {
        var frameWidth = 456,
            maxHeight = 220,
            image = this.model.get('image');

        var imgWidth;
        if (image.width > frameWidth) {
            imgWidth = frameWidth;
        } else {
            imgWidth = image.width;
        }
        var imgHeight = imgWidth / image.width * image.height;

        var frameHeight,
            top;
        if (imgHeight > maxHeight) {
            frameHeight = maxHeight;
            top = -(imgHeight - frameHeight) / 2;
        } else {
            frameHeight = image.height;
            top = 0;
        }
        this.imageAttr = {
            'frameHeight': frameHeight,
            'top': top,
            'imgWidth': imgWidth,
            'imgHeight': imgHeight,
            'diff': imgHeight - frameHeight
        };
        return this.imageAttr;
    },

    onAddLike: function() {
    },

    onDeleteLike: function() {
    },

    viewContent: function(e) {
        this.stopEvent(e);

        ContentView.prototype.viewContent.call(this);
    },

    onClickCommentButton: function(e) {
        if (this.viewPoint === 'timeline') {
            this.stopEvent(e);
            ContentView.prototype.viewContent.call(this, {
                focusComment: true
            });
        } else if (this.viewPoint === 'showcase') {
            this.trigger('click-comment');
        }

    },

    onceDestroyDone: function() {
        if (this.viewPoint === 'timeline') {
            ContentView.prototype.onceDestroyDone.call(this);
            this.$el.next().removeClass('expand-before');
            this.$el.prev().removeClass('expand-after');
        } else if (this.viewPoint === 'showcase') {
            // event handled by viewer
        }
    }

});

module.exports = MessageView;
