var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Menu = require('../contents/content-menu.js'),
    UserAnchor = require('../users/anchor.js');

var contentFooterTemplate = require('../../templates/timelines/content-footer.hbs');

var ContentFooter = Backbone.View.extend({

    className: 'content-footer',

    render: function() {
        this.$el.html( contentFooterTemplate( this.model.toJSON() ) );

        var user = this.model.user,
            userAnchor = new UserAnchor({
                model: user
            });
        this.$('.content-footer_info').prepend(userAnchor.render().el);

        return this;
    },

    hook: function() {
        var userAnchor = new UserAnchor({
            el: this.$('.user-a')
        });
        userAnchor.hook();
    },

    stopEvent: function(e) {
        e.preventDefault();
        e.stopPropagation();
    }

});

module.exports = ContentFooter;
