var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var User = require('../../models/user.js'),
    Menu = require('../contents/content-menu.js'),
    Me = require('../../me.js'),
    Coobii = require('../../coobii.js');

var Content = Backbone.View.extend({

    tagName: 'article',

    className: 'content',

    initialize: function(options) {

    },

    stopEvent: function(e) {
        e.preventDefault();
        e.stopPropagation();
    },

    hookUser: function() {
        var id = this.$el.data('user-id');
        if (!Me.isGuest() && Me.get('id') === id) {
            this.user = Me;
        } else {
            this.user = new User({ id: id });
        }
        return this.user;
    },

    hookMenu: function() {
        this.menu = new Menu({
            el: this.$('.content-menu'),
            model: this.model
        });
        this.menu.hook();
        this.listenMenuEvents();
    },

    listenMenuEvents: function() {
        this.listenTo(this.menu, 'delete-content', this.onDeleteContent);
        this.listenTo(this.menu, 'like-content', this.onAddLike);
        this.listenTo(this.menu, 'unlike-content', this.onDeleteLike);
        this.listenTo(this.menu, 'click-comment', this.onClickCommentButton);
    },

    loaded: false,

    viewContentWithLoaded: function(options) {
        var showcaseState = Coobii.getState('showcase');
        showcaseState.set('display', 'show', {
            type: 'content',
            contentType: this.model.get('type'),
            content: this.model,
            options: options
        });
    },

    viewContent: function(options) {
        var contentView = this;
        if (this.loaded) {
            this.viewContentWithLoaded(options);
        } else {
            this.model.fetch().then(function() {
                this.model.initialize();
                this.loaded = true;
                this.viewContentWithLoaded(options);
            }.bind(this));
        }
    },

    onDeleteContent: function(e) {
        this.model.destroy();
    },

    onceDestroyDone: function() {
        this.$el.transition({
            'opacity': 0
        }, function() {
            this.remove();
        });
    },

    onClickCommentButton: function(e) {
        this.trigger('click-comment', e);
    }

});

module.exports = Content;
