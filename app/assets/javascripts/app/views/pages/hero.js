var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Hero = require('../hero.js');

var PageHero = Hero.extend({

    className: 'page-hero hero'

});

module.exports = PageHero;
