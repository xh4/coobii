var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var TopicBox = require('../../mio/box-topic.js'),
    Topic = require('../../models/topic.js'),
    Post = require('../../models/post.js'),
    Page = require('../../models/page.js'),
    Coobii = require('../../coobii.js');

var pageHeaderTemplate = require('../../templates/pages/page-header.hbs');

var CurrentUser = require('../../states/current-user');

var PageHeader = Backbone.View.extend({

    className: 'page-header',

    initialize: function(options) {
        this.pageState = options.state;
    },

    render: function() {
        this.$el.html( pageHeaderTemplate() );

        return this;
    },

    listenEvents: function() {
        this.listenTo(this.pageState, "change:section", this.onChangeSection);
    },

    events: {
        "click .page-nav-item": "onClickNavItem",
        "click .act-new-topic": "openTopicBox"
    },

    onChangeSection: function(pageState, sectionName) {
        this.$('.page-nav-item').removeClass('active');
        this.$('.page-nav-item[data-section=' + sectionName + ']')
            .addClass('active');
    },

    onClickNavItem: function(e) {
        var li = $(e.target).closest('li'),
            sectionName = li.data('section');
        this.pageState.set('section', sectionName);
    },

    openTopicBoxLocked: false,
    openTopicBox: function(e) {
        if (CurrentUser.isGuest()) {
            return alert("登录后才能创建话题");
        }

        if (this.openTopicBoxLocked) return;
        this.openTopicBoxLocked = true;

        this.$('.act-new-topic .spinner').show();

        $.ajax({
            type: 'POST',
            url: '/api/topics?page_id=' + this.model.id,
            context: this
        }).then(function(t) {
            return $.ajax({
                type: 'POST',
                url: '/api/posts?topic_id=' + t.id,
                context: this
            });
        }, function(jqXHR) {
            return jqXHR;
        }).always(function() {
            this.$('.act-new-topic .spinner').hide();
            this.openTopicBoxLocked = false;
        }).done(function(p) {
            var post = new Post(p),
                topic = new Topic(p.topic),
                box = new TopicBox({
                    post: post,
                    topic: topic
                });
            box.render();
            this.showTopicBox(box);
        }).fail(function() {
            alert('error');
        });
    },

    showTopicBox: function(box) {
        var showcaseState = Coobii.getState('showcase');
        showcaseState.set('display', 'show', {
            'type': 'mio',
            'box': box
        });
        box.on('post-done', function() {
            this.closeBox();
        });
    },

    stopListeningEvents: function() {
        this.stopListening(this.pageState);
    }

});

module.exports = PageHeader;
