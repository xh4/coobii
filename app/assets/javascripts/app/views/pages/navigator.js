var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Coobii = require('../../coobii.js'),
    pageNavigatorTemplate = require('../../templates/pages/navigator.hbs');

var AppState = require('../../states/app-state.js');

var PageNavigator = Backbone.View.extend({

    id: "page-navigator",

    render: function() {
        this.$el.html( pageNavigatorTemplate() );

        this.listenTo(AppState, 'change:place', function(m, v) {
            if (v === 'teatime') this.$('.pn-meter').addClass('active');
            else this.$('.pn-meter').removeClass('active');
        });

        return this;
    },

    events: {
        "click .pn-meter": "onClickMeter"
    },

    dropdownOpened: false,

    toggleDropdown: function(e) {
        if (this.dropdownOpened) {
            this.closeDropdown();
            this.dropdownOpened = false;
        } else {
            this.openDropdown();
            this.dropdownOpened = true;
        }
    },

    openDropdown: function() {
        this.$('.pn-meter').addClass('active');
        this.$('.pn-dropdown').show();
    },

    closeDropdown: function() {
        this.$('.pn-meter').removeClass('active');
        this.$('.pn-dropdown').hide();
    },

    onClickMeter: function(e) {
        var meter = $(e.target).closest('.pn-meter'),
            actived = meter.hasClass('active'),
            place = actived? 'hero' : 'teatime';

        AppState.set('place', place);
    }

});

module.exports = PageNavigator;
