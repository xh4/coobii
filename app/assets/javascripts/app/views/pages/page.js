var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Slide = require("../slide.js"),
    PageHeader = require('./page-header.js');

var Page = require('../../models/page.js'),
    TopicView = require('../contents/topic.js'),
    Me = require('../../me.js'),
    Coobii = require('../../coobii.js');

var pageTemplate = require('../../templates/pages/page.hbs');

var PageState = Backbone.Model.extend();

var BackgroundActions = require('../../actions/background-actions');

var CurrentUser = require('../../states/current-user');

var Sections = {
    "main": require('./sections/main.js'),
    "archive": require('./sections/archive.js')
};

var PageSlide = Slide.extend({

    className: 'page page-slide slide hide',

    name: "",

    bodyMinHeight: 500,

    initialize: function() {
        this.router = window["router"];

        this.name = this.model.get('name_lower');

        // sections
        this.sections = {};
        this.currentSection = null;

        this.state = new PageState();
        this.model.state = this.state;

        this.header = new PageHeader({
            state: this.model.state,
            model: this.model
        });

        this.listenSliderEvents();
    },

    render: function() {
        this.$el.html( pageTemplate() );

        this.$(".page-banner").after( this.header.render().el );
        this.header.listenEvents();

        this.listenTo(this.model.state, "change:section", this.changeSection);
        this.listenTo(this.model.state, "change:view", this.changeView);

        return this;
    },

    changeSection: function(state, sectionName) {
        var section = this.sections[sectionName];
        if (section === undefined) {
            section = new Sections[sectionName]({
                pageView: this,
                page: this.model
            });
            section.render();
            this.sections[sectionName] = section;
        }
        if (this.currentSection) this.currentSection.$el.detach();
        this.currentSection = section;
        this.$('.page-body').html( section.el );
    },

    changeView: function(state, view, options) {
        if (view === 'topic') {
            this.viewTopic(options);
        } else {
            this.viewBack();
        }
    },

    viewTopic: function(options) {
        var topic = options.model;
        var topicView = new TopicView({
            model: topic
        });

        this.savedTopic = topic;

        // router
        var uri = this.model.getDisplayNS() + "/topics/" +
                topic.get('id');
        this.router.navigate(uri);
        this.listenToOnce(this.router, 'route:viewNamespace', this.onRoutingBack);
        this.listenToOnce(this.router, 'route:viewTopic', this.onRoutingResume);

        // title
        document.title = topic.get('title');

        // save scroll-top and height
        this.savedScrollTop = this.el.scrollTop;
        this.savedHeight = this.$('.page-body').outerHeight();

        // render
        this.header.$el.append( topicView.header.render().el );
        this.$('.page-body').css({
            'min-height': this.savedHeight
        });
        this.$('.page-section').hide();
        this.$('.page-body').append( topicView.render().el );
        this.$('.page-body').css({
            'min-height': this.bodyMinHeight
        });

        // fix offset top
        var bannerHeight = this.getBannerHeight();
        if (this.el.scrollTop > bannerHeight) {
            this.el.scrollTop = bannerHeight;
        }

        this.listenTo(topicView, 'back', function() {
            this.model.state.set('view', null);
        });

        topic.increaseViewsCount();
    },

    onRoutingBack: function() {
        this.model.state.set('view', null);
    },

    onRoutingResume: function() {
        this.model.state.set('view', 'topic', {
            model: this.savedTopic
        });
    },

    viewBack: function() {
        var currentScrollTop = this.el.scrollTop;

        this.$('.page-body .topic').remove();
        this.$('.page-header .topic-header').remove();
        this.$('.page-body').css({
            'min-height': this.savedHeight
        });
        this.$('.page-section').show();

        this.el.scrollTop = this.savedScrollTop;

        this.changeTitle();
        this.router.navigate('/'+this.model.getDisplayNS(), {replace: true});
    },

    changeBackground: function() {
        this.model.fetch().then(function(p) {
            BackgroundActions.changeBackground(p.background);
        });
    },

    restoreBackground: function() {
        CurrentUser.fetch().then(function() {
            BackgroundActions.changeBackground(CurrentUser.get('background'));
        });
    },

    changeNamespace: function() {
        var ns = this.model.getDisplayNS();
        this.router.navigate('/' + ns, { replace: true });
    },

    restoreNamespace: function() {
        var ns = $('body').data('current-user-username');
        this.router.navigate('/' + ns, { replace: true });
    },

    changeTitle: function() {
        document.title = this.model.get('display_name');
    },

    restoreTitle: function() {
        var me = Coobii.getState('me'),
            title = me.getTitle();
        document.title = title;
    },

    stopListeningEvents: function() {
        this.stopListening(this.model.state);
    },

    loaded: false,

    beforeSlideIn: function() {
        this.render();
        this.resetHeaderPosition();
        this.$el.removeClass('hide');
        this.trigger('ready');
    },

    beforeSlideUp: function() {
        if (!this.loaded) this.render();
        this.resetHeaderPosition();
        this.trigger('ready');
    },

    beforeSlideOut: function() {
        this.detach();
    },

    beforeSlideDown: function() {
        this.detach();
    },

    afterSlideIn: function() {
        this.resume();
        this.load();
    },

    afterSlideUp: function() {
        this.resume();
        if (!this.loaded) this.load();
    },

    afterSlideOut: function() {
        this.recover();
        this.kill();
    },

    afterSlideDown: function() {
        this.recover();
    },

    load: function() {
        this.loaded = true;
        this.model.state.set('section', 'main');
    },

    detach: function() {
        this.$el.off('scroll');
        if (this.headerFixed) {
            this.pullHeader();
            var bannerHeight = this.getBannerHeight(),
                scrollTop = this.el.scrollTop;
            var top = scrollTop - bannerHeight;
            this.$('.page-body').css('margin-top', 0);
            this.header.$el.css({
                'top': top
            });
        }
        this.headerFixed = false;
    },

    resume: function() {
        this.bindScrollEvents();
        this.changeBackground();
        this.changeNamespace();
        this.changeTitle();
    },

    recover: function() {
        this.$el.addClass('hide');
        this.el.scrollTop = 0;
        this.setTop();
        this.$('.page-body').css('margin-top', 0);
        this.restoreBackground();
        this.restoreNamespace();
        this.restoreTitle();
    },

    kill: function() {
        this.loaded = false;
        this.header.$el.detach();
        this.$el.detach();
        this.stopListeningEvents();
        this.header.stopListeningEvents();
        this.model.state.clear({ silent: true });
        this.sections = {};
        this.currentSection = null;
    },

    pushHeader: function() {
        $('.slide-h-w').show();
        this.header.$el.appendTo('.slide-h-w');
    },

    pullHeader: function() {
        this.header.$el.detach();
        $('.slide-h-w').hide();
        this.$('.page-banner').after(this.header.el);
    },

    resetHeaderPosition: function() {
        this.header.$el.css({
            'top': 0
        });
    },

    headerFixed: false,

    bindScrollEvents: function() {
        this.$el.on('scroll', this.onScroll.bind(this));
    },

    onScroll: function(e) {
        var offsetTop = this.$('.page-card').offset().top - 40,
            bannerHeight = this.$('.page-banner').outerHeight();
        if (offsetTop + bannerHeight <= 0) {
            if (this.headerFixed) return;
            this.headerFixed = true;
            this.pushHeader();
            this.$('.page-body').css('margin-top', '40px');
        } else {
            if (!this.headerFixed) return;
            this.headerFixed = false;
            this.pullHeader();
            this.$('.page-body').css('margin-top', 0);
        }
    },

    getBannerHeight: function() {
        return this.$('.page-banner').outerHeight();
    },
    getBannerAndHeaderHeight: function() {
        return this.getBannerHeight() + this.header.$el.outerHeight();
    },

    hook: function() {
        this.header = new PageHeader({
            el: this.$('.page-header'),
            state: this.model.state,
            model: this.model
        });
        this.header.listenEvents();
        this.bindScrollEvents();
        this.listenTo(this.model.state, "change:section", this.changeSection);
        this.listenTo(this.model.state, "change:view", this.changeView);

        if (this.$('.page-body .topic').length !== 0) {
            this.hookTopic();
        } else {
            this.hookMainSection();
        }
    },

    hookTopic: function() {
        var topicView = new TopicView({
            el: this.$('.page-body .topic')
        });
        topicView.hook({
            headerEl: this.$('.page-header .topic-header')
        });
        this.listenTo(topicView, 'back', function() {
            this.model.state.set({
                view: null,
                section: "main"
            });
        });
    },

    hookMainSection: function() {
        var mainSection = new Sections["main"]({
            el: this.$('.page-section-main'),
            page: this.model,
            pageView: this
        });
        this.sections["main"] = mainSection;
        this.currentSection = mainSection;
        mainSection.hook();
    }

});

module.exports = PageSlide;
