var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Topic = require('../../../models/topic.js'),
    TopicListItem = require('./topic-list-item.js'),
    PageModule = require('./page-module.js'),
    Coobii = require('../../../coobii.js');

var topicListTemplate = require('../../../templates/pages/modules/topic-list.hbs');

var TopicStore = require('../../../stores/topic-store');

var TopicList = PageModule.extend({

    className: "topic-list page-module",

    p: 1,

    initialize: function(options) {
        this.page = options.page;
        this.pageView = options.pageView;

        this.collection = this.page.topics;
    },

    render: function() {
        this.$el.html( topicListTemplate() );

        this.renderPage();

        this.listenComposition();

        return this;
    },

    renderPage: function() {
        var options = {
            url: '/api/topics?page_id=' + this.page.id,
            page: this.p
        };
        TopicStore.loadTopics(options, function(topics, pagination) {

            this.$('>ul').empty();

            // fix scroll top
            var bannerHeight = this.pageView.getBannerHeight();
            if (this.pageView.el.scrollTop > bannerHeight) {
                this.pageView.el.scrollTop = bannerHeight;
            }

            _.each(topics, function(topic) {
                var topicItem = new TopicListItem({
                    model: topic,
                    page: this.page
                });
                this.$('>ul').append( topicItem.render().el );
            }.bind(this));

            this.$('.topic-list-pagination').html(pagination);

        }.bind(this));
    },

    events: {
        "click .pagination .first": "onClickFirst",
        "click .pagination .prev": "onClickPrev",
        "click .pagination .next": "onClickNext",
        "click .pagination .last": "onClickLast",
        "click .pagination .p": "onClickP"
    },

    onClickFirst: function() {
        this.p = 1;
        this.renderPage();
    },
    onClickPrev: function() {
        this.p--;
        this.renderPage();
    },
    onClickNext: function() {
        this.p++;
        this.renderPage();
    },
    onClickLast: function() {
        var s = this.$('.pagination .p').last().html();
        this.p = parseInt(s);
        this.renderPage();
    },
    onClickP: function(e) {
        var p = $(e.target).closest('.p');
        if (p.hasClass('.current')) return;
        var s = p.html();
        this.p = parseInt(s);
        this.renderPage();
    },

    listenComposition: function() {
        this.listenTo(TopicStore, 'new-composition', function(topic) {
            var topicItem = new TopicListItem({
                model: topic,
                page: this.page
            });
            if (this.$('>ul >li').length === 0) {
                this.clearBlankState();
            }
            this.$('>ul').prepend( topicItem.render().el );
        });
    },

    hook: function() {
        this.$('.topic-list-item').each(function(idx, el) {
            var topicItem = new TopicListItem({
                el: el,
                page: this.page
            });
            topicItem.hook();
            TopicStore.add(topicItem.model);
        }.bind(this));
    }

});

module.exports = TopicList;
