var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageModule = require('./page-module.js');

var template = require('../../../templates/pages/modules/page-messages.hbs');

var PageModuleMessages = PageModule.extend({

    className: "page-module-messages page-module",

    initialize: function(options) {

    },

    render: function() {
        PageModule.prototype.render.call(this);

        this.$('.page-module-title').html("留言");
        $('<span>').html("+添加")
            .addClass('page-module-menu')
            .appendTo(this.$('.page-module-header'));

        return this;
    },

    hook: function() {

    }

});

module.exports = PageModuleMessages;
