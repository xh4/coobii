var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var template = require('../../../templates/pages/modules/page-module.hbs');

var PageModule = Backbone.View.extend({

    className: "page-module",

    render: function() {
        this.$el.html( template() );

        return this;
    },

    renderBlankState: function() {
        this.$('.page-module-body').prepend('<div class="page-module-blank-state"></div>');
    },

    clearBlankState: function() {
        this.$('.page-module-blank-state').remove();
    }

});

module.exports = PageModule;
