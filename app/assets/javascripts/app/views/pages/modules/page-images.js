var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageModule = require('./page-module.js');

var template = require('../../../templates/pages/modules/page-images.hbs');

var PageModuleImages = PageModule.extend({

    className: "page-module-images page-module",

    initialize: function(options) {

    },

    render: function() {
        PageModule.prototype.render.call(this);

        this.$('.page-module-title').html("图像");
        $('<span>').html("+上传")
            .addClass('page-module-menu')
            .appendTo(this.$('.page-module-header'));

        return this;
    },

    hook: function() {

    }

});

module.exports = PageModuleImages;
