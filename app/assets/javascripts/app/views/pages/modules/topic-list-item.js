var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Topic = require('../../../models/topic.js'),
    UserAnchor = require('../../users/anchor.js');

var TopicStore = require('../../../stores/topic-store');

var itemTemplate = require('../../../templates/pages/modules/topic-list-item.hbs');

var TopicView = Backbone.View.extend({

    tagName: "li",

    className: "topic-list-item",

    initialize: function(options) {
        this.page = options.page;
    },

    render: function() {
        this.$el.html( itemTemplate( this.model.toJSON() ) );

        if (this.model.get('views_count') === 0) {
            this.$('.topic-views-count').remove();
        }

        // user anchor
        var userAnchor = new UserAnchor({
            model: this.model.user
        });
        this.$('.topic-list-item-info').prepend( userAnchor.render().el );

        // thumbnails
        var thumbnails = this.model.get('thumbnails'),
            item = this;
        if (thumbnails) {
            _.each(thumbnails, function(t, idx) {
                if (idx > 3) return;
                var li = $('<li class="topic-list-item-thumb">'),
                    img = new Image,
                    url = t.versions.thumb.url;
                img.src = url;
                li.css('background-image', 'url(' + url + ')').html(img);
                item.$('.topic-list-item-thumbs').append( li );
            });
        }

        return this;
    },

    events: {
        "click .topic-list-item-title": "viewTopic"
    },

    viewTopic: function(e) {
        e.preventDefault();

        this.page.state.set('view', 'topic', {
            model: this.model
        });
    },

    hook: function() {
        this.model = new Topic({
            id: this.$el.data('topic-id'),
            title: this.$('.topic-list-item-title a').html(),
            page: this.page.toJSON()
        });
        var userAnchor = new UserAnchor({
            el: this.$('.topic-list-item-info .user-a')
        });
        userAnchor.hook();
        this.model.set('user', userAnchor.model.toJSON());
    }

});

module.exports = TopicView;
