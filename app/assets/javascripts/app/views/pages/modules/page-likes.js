var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageModule = require('./page-module.js');

var template = require('../../../templates/pages/modules/page-likes.hbs');

var PageModuleLikes = PageModule.extend({

    className: "page-module-likes"

});

module.exports = PageModuleLikes;
