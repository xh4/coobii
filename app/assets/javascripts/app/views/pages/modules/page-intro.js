var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageModule = require('./page-module.js');

var template = require('../../../templates/pages/modules/page-intro.hbs');

var PageModuleIntro = PageModule.extend({

    className: "page-module-intro page-module",

    render: function() {
        PageModule.prototype.render.call(this);

        this.$('.page-module-title').html("简介");

        return this;
    }

});

module.exports = PageModuleIntro;
