var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageSection = require('./section.js');

var archiveSectionTemplate = require('../../../templates/pages/sections/archive.hbs');

var ArchiveSection = PageSection.extend({

    className: "page-section-archive page-section",

    initialize: function() {

    },

    render: function() {
        this.$el.html( archiveSectionTemplate() );

        return this;
    }

});

module.exports = ArchiveSection;
