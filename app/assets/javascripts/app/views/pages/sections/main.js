var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageSection = require('./section.js'),
    TopicList = require('../modules/topic-list.js'),
    LikesModule = require('../modules/page-likes.js'),
    MessagesModule = require('../modules/page-messages.js'),
    ImagesModule = require('../modules/page-images.js'),
    IntroModule = require('../modules/page-intro.js');

var template = require('../../../templates/pages/sections/main.hbs');

var MainSection = PageSection.extend({

    className: "page-section-main page-section",

    initialize: function(options) {
        this.page = options.page;
        this.pageView = options.pageView;
    },

    render: function() {
        this.$el.html( template() );

        var topicList = new TopicList({
            page: this.page,
            pageView: this.pageView
        });
        this.$('.page-section-main-left').html( topicList.render().el );
        topicList.renderPage();

        this.renderSidebar();

        return this;
    },

    renderSidebar: function() {
        var messagesModule = new MessagesModule({

        });
        this.$('.page-section-main-right')
            .append( messagesModule.render().el );
    },

    load: function() {

    },

    hook: function() {
        this.topicList = new TopicList({
            el: this.$('.topic-list'),
            page: this.page,
            pageView: this.pageView
        });
        this.topicList.hook();
    }

});

module.exports = MainSection;
