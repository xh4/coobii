var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageSection = Backbone.View.extend({

    className: "page-section"

});

module.exports = PageSection;
