var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

require('jquery.transit');

var BackgroundState = require('../states/background-state');

var Backgrond = Backbone.View.extend({

    el: '.fm_bg',

    initialize: function() {
        this.listenTo(BackgroundState, 'change', this.onChangeState);
    },

    busy: false,

    delayed: null,

    onChangeState: function(state, value, options) {
        var attrs = state.toJSON();
        if (this.busy) {
            this.delayed = attrs;
        } else {
            this.changeBackground(attrs);
        }
    },

    changeBackground: function(attrs) {
        this.busy = true;

        var url = attrs.url,
            type = attrs.type;

        var img = new Image;
        img.src = url;

        img.onload = function() {

            var fm = document.getElementById('user-fm'),
                bg = fm.getElementsByClassName('fm_bg')[0],
                new_bg = document.createElement('div');

            new_bg.className = 'fm_bg-new';
            bg.parentNode.appendChild(new_bg);
            new_bg.style.backgroundImage = "url(" + url + ")";

            if (type === "fill") {
                new_bg.style.backgroundSize = 'cover';
                new_bg.style.backgroundRepeat = 'no-repeat';
            }

            $(new_bg).transition({
                opacity: 1
            }, 2000, function() {
                bg.parentNode.removeChild(bg);
                new_bg.className = 'fm_bg';

                if (this.delayed) {
                    this.changeBackground(this.delayed);
                    this.delayed = null;
                } else {
                    this.busy = false;
                }
            }.bind(this));

        }.bind(this);
    }

});


module.exports = Backgrond;
