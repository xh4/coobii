var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Kinetic = require('kinetic');

var User = require('../../models/user.js');

var Hexagon = Backbone.View.extend({

    model: User,

    size: 120,

    avatarSize: 200,

    tagName: "a",

    attributes: {
        "target": "_blank"
    },

    className: "hex",

    render: function() {

        this.$el.attr('href', "/" + ( this.model.get('username') ));

        var stage = new Kinetic.Stage({
            container: this.el,
            width: this.size,
            height: this.size
        });

        var layer = new Kinetic.Layer;

        var hexagon = new Kinetic.RegularPolygon({
            x: this.size / 2,
            y: this.size / 2,
            sides: 6,
            radius: this.size / 2,
            strokeWidth: 3,
            strokeRed: 255,
            strokeGreen: 255,
            strokeBlue: 255,
            strokeAlpha: .4,

            fillPatternRepeat: 'no-repeat',
            fillPatternOffset: { x: 100,
                                 y: 100 },
            fillPatternScale: { x: this.size / this.avatarSize,
                                y: this.size / this.avatarSize }
        });

        layer.add(hexagon);

        stage.add(layer);

        var avatar = new Image;

        avatar.src = this.model.getAvatarUrl(null);

        avatar.onload = $.proxy(function() {
            hexagon.setFillPatternImage(avatar);
            stage.draw();
            this.trigger('ready');
        }, this);

        var tween = new Kinetic.Tween({
            node: hexagon,
            duration: 0.14,
            strokeAlpha: 1,
            easing: Kinetic.Easings.Linear
        });


        this.$el.on('mouseover', function() {
            tween.play();
        });

        this.$el.on('mouseleave', function() {
            tween.reverse();
        });

    }

});

module.exports = Hexagon;
