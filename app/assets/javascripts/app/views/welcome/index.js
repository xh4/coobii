var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Hexagon = require('./hexagon.js'),
    User = require('../../models/user.js');

var Index = Backbone.View.extend({

    el: ".welcome-fm",

    initialize: function() {

        if (localStorage.user_login) {
            this.$('.username-field input').val(localStorage.user_login);
        }

        $('#creator-list').html("<div class='line-top'></div> \
                                <div class='line-middle'> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                </div> \
                                <div class='line-bottom'> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                <div class='ph'></div> \
                                </div>");

        $.get('/api/welcome/creators', function(users) {
            _.each(users, function(u) {
                var user = new User(u),
                    hex = new Hexagon({
                        model: user
                    });
                hex.on('ready', function() {
                    var top_num = $('#creator-list .line-top .hex').length,
                        middle_num = $('#creator-list .line-middle .hex').length,
                        bottom_num = $('#creator-list .line-bottom .hex').length;
                    if (top_num < 4) {
                        $('#creator-list .line-top').append(hex.el);
                    } else if (middle_num === 0) {
                        $('#creator-list .line-middle .ph').eq(2).html(hex.el);
                    } else if (middle_num === 1) {
                        $('#creator-list .line-middle .ph').eq(1).html(hex.el);
                    } else if (middle_num === 2) {
                        $('#creator-list .line-middle .ph').eq(3).html(hex.el);
                    } else if (middle_num === 3) {
                        $('#creator-list .line-middle .ph').eq(0).html(hex.el);
                    } else if (middle_num === 4) {
                        $('#creator-list .line-middle .ph').eq(4).html(hex.el);
                    } else if (bottom_num === 0) {
                        $('#creator-list .line-bottom .ph').eq(1).html(hex.el);
                    } else if (bottom_num === 1) {
                        $('#creator-list .line-bottom .ph').eq(2).html(hex.el);
                    } else if (bottom_num === 2) {
                        $('#creator-list .line-bottom .ph').eq(0).html(hex.el);
                    } else {
                        $('#creator-list .line-bottom .ph').eq(3).html(hex.el);
                    }
                });

                hex.render();
            });
        });

        // bg

        $('.fm_bg').append("<div class='fm_bg-top'></div>");

        var url = '/assets/bg-top.jpg';

        var bg = new Image;

        bg.src = url;

        bg.onload = function() {
            $('.fm_bg-top').css({
                'background-image': "url(" + url + ")",
                'background-repeat': 'repeat-x',
                'height': bg.height,
                'opacity': 0
            }).animate({
                'opacity': 1
            }, 500);
        };

    }

});

module.exports = Index;
