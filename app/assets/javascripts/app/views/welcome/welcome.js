var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Index = require('./index'),
    Login = require('./login'),
    Register = require('./register');

var pathname = location.pathname;

if (/register/.test(pathname)) {
    new Register();
} else if (/login/.test(pathname)) {
    new Login();
} else if (pathname === '/'){
    new Index();
}
