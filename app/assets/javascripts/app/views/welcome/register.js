var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var AvatarField = require('../fields/field-avatar.js');

var Register = Backbone.View.extend({

    el: 'body',

    initialize: function() {

        this.$('.register-form').show();

        this.$('.avatar-field').remove();

        this.avatarField = new AvatarField({

            name: "头像"

        });

        this.$('form h1').after( this.avatarField.render().el );

        this.$('input[type=submit]').click(function(e) {
            e.preventDefault();

            this.showOverlay();

            this.avatarField.imagePicker.request({
                url: "/registrations",
                type: "POST",
                formData: this.getFormData(),
                done: function(user) {
                    window.location = "/"+user.username;
                }.bind(this),
                fail: function(err) {
                    window.scrollTo(0, 0);
                    this.renderErrors(err);
                }.bind(this),
                always: function() {
                    this.hideOverlay();
                }.bind(this)
            });
        }.bind(this));
    },

    getFormData: function() {
        return {
            display_name: this.$('#display_name').val(),
            email: this.$('#email').val(),
            password: this.$('#password').val(),
            username: this.$('#username').val(),
            invite_code: this.$('#invite_code').val()
        };
    },

    renderErrors: function(err) {
        this.$('.flash.error').remove();
        _.each(err, function(e) {
            var el = $('<p class="flash error"></p>').html(e);
            this.$('h1').after(el);
        }.bind(this));
    },

    showOverlay: function() {
        this.$('.welcome-overlay').show();
    },

    hideOverlay: function() {
        this.$('.welcome-overlay').hide();
    }

});

module.exports = Register;
