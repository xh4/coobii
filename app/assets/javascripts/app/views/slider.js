var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Space = require("./users/space.js"),
    PageSlide = require("./pages/page.js"),
    Explorer = require('./users/explorer.js'),
    Settings = require('./users/settings.js'),
    Page = require('../models/page.js'),
    Coobii = require('../coobii.js'),
    Me = require('../me.js');

var AppActions = require('../actions/app-actions.js');

var Slider = Backbone.View.extend({

    className: 'slider',

    currentSlide: null,

    hook: function() {
        this.slides = [];

        var s = this.$('> .slide'),
            type = s.attr('data-slide');
        if (type === 'space') {

        }
        this.space = new Space({
            el: this.$('.space')
        });
        this.slides.push(this.space);

        this.currentSlide = this.space;

        if (!Me.isGuest()) {
            this.explorer = new Explorer;
            this.settings = new Settings;
            this.slides.push(this.explorer);
            this.slides.push(this.settings);
        }

        var teatime = new Page({
            id: 1,
            name: "teatime",
            name_lower: "teatime",
            display_name: "放课后的茶话会",
            color: "orange",
            aliases: [{
                type: 'short',
                name: 'tt',
                name_lower: 'tt'
            }, {
                type: 'zh_cn',
                name: '放课后的茶话会',
                name_lower: '放课后的茶话会'
            }]
        });

        if ($('body').attr('data-init-ns-type') === 'page') {
            var page = new PageSlide({
                model: teatime,
                el: $('.slider > .page')
            });
            this.currentSlide = page;
            page.hook();
            this.showCarouselNav();
        } else {
            var page = new PageSlide({
                model: teatime
            });
        }

        // fix slide-h-w in firefox
        if ($('html').hasClass('no-cssscrollbar')) {
            var scrollBarWidth = this.getScrollBarWidth();
            $('.slide-h-w').css({
                'right': scrollBarWidth+'px',
                'min-width': 1024-scrollBarWidth+'px'
            });
        }

        this.slides.push(page);
    },

    getSlideByName: function(name) {
        if (name === 'space') {
            return this.space;
        } else if (name === 'explorer') {
            return this.explorer;
        } else if (name === 'settings') {
            return this.settings;
        } else {
            return _.last(this.slides);
        }
        return null;
    },

    show: function(slideName, fn) {
        var slider = this;
        this.$el.addClass('show');

        var currentSlide = this.currentSlide,
            targetSlide = this.getSlideByName(slideName);

        if (currentSlide.name !== targetSlide.name) currentSlide.kill();

        this.listenToOnce(targetSlide, 'ready', function() {
            this.$el.append(targetSlide.el);
            this.slideUp(targetSlide, function() {
                targetSlide.trigger('after-slide-up');
                slider.currentSlide = targetSlide;
                slider.showCarouselNav();
                if (fn) fn();
            });
        });
        targetSlide.trigger('before-slide-up');

    },

    hide: function(fn) {
        var slider = this,
            currentSlide = this.currentSlide;
        this.hideCarouselNav();
        currentSlide.trigger('before-slide-down');
        currentSlide.slideDown(function() {
            currentSlide.trigger('after-slide-down');
            slider.$el.addClass('hide');
            if (fn) fn();
        });
    },

    slideUp: function(targetSlide, fn) {
        var slider = this;
        targetSlide.hideBottom();
        setTimeout(function() {
            targetSlide.slideUp(function() {
                if (fn) fn();
            });
        }, 0);
    },

    switchToSlideByName: function(slideName, appendSide, fn) {
        var slider = this,
            targetSlide = this.getSlideByName(slideName);

        this.currentSlide.trigger('before-slide-out');

        this.listenToOnce(targetSlide, 'ready', function() {
            var frameWidth = this.$el.parent().width(),
                side = appendSide || this.getAppendSide(targetSlide);

            var appendLeft;
            if (side === 'right') {
                appendLeft = frameWidth;
            } else {
                appendLeft = -frameWidth;
            }

            this.$el.css('overflow', 'visible');
            targetSlide.$el.css({
                position: 'absolute',
                left: appendLeft
            }).appendTo(this.$el);

            this.slideHorizontal(-appendLeft, function() {
                this.$el.css({
                    'transition': "transform 0ms",
                    '-webkit-transition': "-webkit-transform 0ms",
                    'transform': "translate3d(0px, 0px, 0px)",
                    '-webkit-transform': "translate3d(0px, 0px, 0px)"
                });
                targetSlide.$el.css({
                    position: 'relative',
                    'left': 0
                });
                this.$el.css('overflow', 'hidden');
                this.currentSlide.trigger('after-slide-out');

                targetSlide.trigger('after-slide-in');
                this.currentSlide = targetSlide;

                if (fn) fn();
            });
        });

        targetSlide.trigger('before-slide-in');
    },

    getTransEndAttr: function() {
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'transition': 'transitionend'
        };
        return transEndEventNames[Modernizr.prefixed('transition')];
    },

    slideHorizontal: function(left, fn) {
        var left = left + "px",
            slider = this,
            transformAttr = "translate3d(" + left + ", 0px, 0px)",
            transEndEvent = this.getTransEndAttr();
        this.$el.bind(transEndEvent, function(e) {
            if (e.target != slider.el) return;
            slider.$el.unbind(transEndEvent);
            if (fn) fn.call(slider);
        }).css({
            '-webkit-transition': "-webkit-transform 350ms cubic-bezier(0.645, 0.045, 0.355, 1)",
            'transition': "transform 350ms cubic-bezier(0.645, 0.045, 0.355, 1)",
            '-webkit-transform': transformAttr,
            'transform': transformAttr
        });
    },

    getAppendSide: function(slide) {
        if (_.indexOf(this.slides, slide) <
            _.indexOf(this.slides, this.currentSlide)) {
            return 'left';
        }
        return 'right';
    },

    showCarouselNav: function() {
        $('.carousel-nav').fadeIn();
        $('.carousel-nav').on('click', this.onClickCarouselNav.bind(this));
    },

    hideCarouselNav: function() {
        $('.carousel-nav').fadeOut();
        $('.carousel-nav').unbind('click');
    },

    onClickCarouselNav: function(e) {
        var nav = $(e.target),
            direction = nav.attr('class').match(/carousel-(\w{4})/)[1],
            slides = this.slides,
            length = slides.length,
            currentIndex = _.indexOf(slides, this.currentSlide),
            appendSide = null,
            targetSlide;
        if (direction === 'prev') {
            if (currentIndex === 0) {
                targetSlide = _.last(slides);
                appendSide = 'left';
            } else {
                targetSlide = slides[currentIndex - 1];
            }
        } else if (direction === 'next') {
            if (currentIndex === length - 1) {
                targetSlide = slides[0];
                appendSide = 'right';
            } else {
                targetSlide = slides[currentIndex + 1];
            }
        }
        AppActions.togglePlace(targetSlide.name, {
            appendSide: appendSide
        });

    },

    getScrollBarWidth: function() {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);
    }

});

module.exports = Slider;
