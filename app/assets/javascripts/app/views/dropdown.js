var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Dropdown = Backbone.View.extend({

    className: 'dropdown',

    initialize: function(options) {
        this.toggler = options.toggler;
        this.toggler.after(this.el);

        var dropdown = this;
        this.toggler.on('click', function(e) {
            dropdown.stopEvent(e);

            dropdown.toggle();
        }).css({
            'user-select': 'none',
            '-moz-user-select': 'none',
            '-webkit-user-select': 'none',
            '-ms-user-select': 'none'
        });
    },

    visible: false,

    caretHeight: 13,

    render: function() {
        this.$el.html('<div class="dropdown_inner"></div>');
        this.setPos();
        return this;
    },

    setPos: function() {
        var width = this.$el.width(),
            togglerWidth = this.toggler.outerWidth(),
            right = (togglerWidth - width) / 2;

        this.$el.css('right', right);
    },

    hide: function() {
        if (this.beforeHide) this.beforeHide();
        this.$el.hide();
        this.visible = false;
        if (this.afterHide) this.afterHide();
    },

    show: function() {
        if (this.beforeShow) this.beforeShow();
        this.$el.show();
        this.visible = true;
        if (this.afterShow) this.afterShow();
    },

    // 设置posSetted属性，解决toggler和drpodown未插入视图时导致无法定位的问题
    posSetted: false,
    toggle: function() {
        if (!this.posSetted) {
            this.setPos();
            this.posSetted = true;
        }

        if (this.visible) {
            this.hide();
        } else {
            this.show();
        }
    },

    stopEvent: function(e) {
        e.preventDefault();
        e.stopPropagation();
    }

});

module.exports = Dropdown;
