var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Dropdown = require('../dropdown.js'),
    deleteDropdownTemplate = require('../../templates/contents/dropdown-delete.hbs');

var DeleteDropdown = Dropdown.extend({

    initialize: function(options) {
        Dropdown.prototype.initialize.call(this, options);
    },

    className: 'delete-dropdown dropdown',

    render: function() {
        Dropdown.prototype.render.call(this);
        this.$('.dropdown_inner').html( deleteDropdownTemplate() );
    },

    beforeShow: function() {
        this.toggler.closest('.msg_menu-right').addClass('active');
    },

    afterShow: function() {
        this.toggler.closest('.msg_menu-right').removeClass('active');
    },

    events: {
        "click .act-cancel": function(e) {
            this.stopEvent(e);
            this.toggle();
        },
        "click .act-confirm": function(e) {
            this.stopEvent(e);
            this.toggle();
            this.trigger('delete-content');
        }
    }

});

module.exports = DeleteDropdown;
