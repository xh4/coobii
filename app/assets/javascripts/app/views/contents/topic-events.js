var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var User = require('../../models/user.js'),
    UserAnchor = require('../users/anchor.js');

var eventTemplate = require('../../templates/contents/topic-event.hbs');

var TopicEvents = Backbone.View.extend({

    tagName: 'ul',

    className: 'topic-events',

    render: function() {

        this.$el.empty();

        var events = this.model.get('events');

        _.each(events, function(e) {

            var user = new User(e.user),
                userAnchor = new UserAnchor({
                    model: user
                });

            var el = $('<div class="topic-event"></div>')
                    .html(eventTemplate());

            $(el).find('.topic-event-type').addClass(e.type);

            var tx;
            if (e.type === 'open') {
                tx = "开启话题";
            } else if (e.type === 'close') {
                tx = "关闭话题";
            }
            $(el).find('.topic-event-content')
                .append(userAnchor.render().el)
                .append(tx);

            this.$el.append(el);

        }.bind(this));

        this.listenEvents();

        return this;
    },

    listenEvents: function() {
        this.listenTo(this.model, 'change:events', this.render);
    }

});

module.exports = TopicEvents;
