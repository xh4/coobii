var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var ContentView = require('./content-view'),
    ContentMenu = require('./content-menu'),
    UserAnchor = require('../users/anchor'),
    CommentArea = require('../comments/comment-area');

var postTemplate = require('../../templates/contents/post.hbs');

var PostView = ContentView.extend({

    className: "post",

    imageMaxWidth: 440,

    initialize: function() {
    },

    render: function() {
        this.$el.html( postTemplate( this.model.toJSON() ) );

        var userAnchor = new UserAnchor({
            model: this.model.user
        });
        this.$('.post-header').prepend( userAnchor.render().el );

        if (this.model.get('floor') === 2)
            this.$el.addClass('after-bar');

        this.renderBlocks();

        this.commentArea = new CommentArea({
            model: this.model
        });
        this.$('.post-body').after( this.commentArea.render().el );
        this.commentArea.$('.comments').after( this.commentArea.$('.add-comment') );
        this.commentArea.loadComments(function() {
            if (this.commentArea.$('.comment').length != 0) {
                this.commentArea.$el.show();
            }
        }.bind(this));

        var primer = this.model.get('primer');

        if (primer) this.$el.addClass('primer');

        // menu
        if (!primer) {
            this.menu = new ContentMenu({
                model: this.model
            });
            this.$('.post-footer').prepend( this.menu.render().el );
            this.listenTo(this.menu, 'delete-content', function() {
                this.model.destroy();
            });
        }

        // events
        this.listenToOnce(this.model, 'destroy', this.remove);

        return this;
    },

    renderBlocks: function() {
        var blocks = this.model.get('blocks'),
            postView = this;
        _.each(blocks, function(block) {
            var type = block.type,
                caped = type.charAt(0).toUpperCase() + type.slice(1),
                el = $('<div class="post-block"></div>');
            el.addClass('post-block-' + type);
            el = postView['render' + caped + "Block"](el, block);
            postView.$('.post-blocks').append(el);
        });
    },

    renderTextBlock: function(el, block) {
        el.html(block.content);
        return el;
    },

    renderFigureBlock: function(el, block) {
        var url = block.url,
            width = block.width,
            height = block.height;
        var w = width < this.imageMaxWidth ? width : this.imageMaxWidth,
            h = w / width * height;
        var img = $('<img />');
        img.attr({
            'src': url,
            'width': w,
            'height': h
        });
        el.html(img);
        return el;
    },

    events: {
        "click .act-reply": "onClickReply"
    },

    onClickReply: function(e) {
        this.commentArea.$el.show();
        this.commentArea.focus();
    },

    hook: function() {
        this.hookMenu();
        this.listenToOnce(this.model, 'destroy', this.remove);
    },

    hookMenu: function() {
        this.menu = new ContentMenu({
            el: this.$('.content-menu'),
            model: this.model
        });
        this.menu.hook();
        this.listenTo(this.menu, 'delete-content', function() {
            this.model.destroy();
        });
    }

});

module.exports = PostView;
