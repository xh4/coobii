var $ = require('jquery'),
Backbone = require('backbone');
Backbone.$ = $;

var PostBox = require('../../mio/box-post.js'),
    Topic = require('../../models/topic.js'),
    Post = require('../../models/post.js'),
    Coobii = require('../../coobii.js');

var templatePG = require('../../templates/contents/topic-header.hbs'),
    templateSA = require('../../templates/contents/topic-header-sa.hbs');

var TopicHeader = Backbone.View.extend({

    className: "topic-header",

    model: Topic,

    initialize: function(options) {
        this.viewPoint = options.viewPoint;
    },

    render: function() {
        var template;

        if (this.viewPoint === 'showcase') template = templateSA;
        else if (this.viewPoint === 'page') template = templatePG;

        this.$el.html( template( this.model.toJSON() ) );

        var pageUri = '/'+this.model.get('page').display_namespace;
        this.$('.topic-header-page-btn').attr('href', pageUri);
        this.$('.topic-title').attr('href', pageUri+'/topics/'+this.model.id);

        return this;
    },

    events: {
        "click .act-back": "onClickBack",
        "click .act-new-post": "openPostBox"
    },

    onClickBack: function(e) {
        e.preventDefault();
        this.trigger('back');
    },

    openTopicBoxLocked: false,
    openPostBox: function(e) {
        if (this.openPostBoxLocked) return;
        this.openPostBoxLocked = true;

        this.$('.act-new-post .spinner').show();

        $.ajax({
            type: 'POST',
            url: '/api/posts?topic_id=' + this.model.id,
            context: this
        }).always(function() {
            this.$('.act-new-post .spinner').hide();
            this.openPostBoxLocked = false;
        }).done(function(p) {
            var post = new Post(p),
                box = new PostBox({
                    post: post,
                    topic: this.model
                });
            box.render();
            this.showPostBox(box);
        }).fail(function() {
            alert('error');
        });
    },

    showPostBox: function(box) {
        var showcaseState = Coobii.getState('showcase');
        showcaseState.set('display', 'show', {
            'type': 'mio',
            'box': box
        });
        box.on('post-done', function() {
            this.closeBox();
        });
    }

});

module.exports = TopicHeader;
