var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var TopicBar = Backbone.View.extend({

    tagName: 'ul',

    className: 'topic-bar'

});

module.exports = TopicBar;
