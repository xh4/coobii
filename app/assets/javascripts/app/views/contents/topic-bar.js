var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Coobii = require('../../coobii.js');

var template = require('../../templates/contents/topic-bar.hbs');

var CurrentUser = require('../../states/current-user');

var TopicBar = Backbone.View.extend({

    tagName: 'ul',

    className: 'topic-bar',

    render: function() {

        this.$el.html( template( this.model.toJSON() ) );

        var topicState = this.model.get('state'),
            liked = this.model.get('primer').liked,
            isMaster = !CurrentUser.isGuest() &&
                CurrentUser.id === this.model.get('user').id;

        if (topicState === 'close')
            this.$('.buttons').append('<button class="btn btn-red">话题已关闭</button>');
        if (isMaster && topicState === 'open')
            this.$('.buttons').append('<button class="btn btn-red act-close-topic">关闭话题</button>');

        var p = this.model.get('primer');
        if (!isMaster) {
            if (p.liked)
                this.$('.buttons').append('<button class="btn btn-green">已赞</button>');
            else
                this.$('.buttons').append('<button class="btn btn-white act-like-topic">赞此话题</button>');
        }

        return this;
    },

    listenEvents: function() {
        this.listenTo(this.model, 'change', this.render);
    },

    events: {
        "click .act-close-topic": "onClickCloseTopic",
        "click .act-like-topic": "onClickLikeTopic"
    },

    onClickCloseTopic: function(e) {
        if (!confirm("确定关闭吗？关闭后不能重新开启")) {
            return;
        }
        this.$('.act-close-topic')
            .removeClass('act-close-topic')
            .html("话题已关闭");

        $.ajax({
            url: '/api/topics/' + this.model.id + '/close',
            type: 'POST',
            context: this
        }).done(function(res) {
            var t = res;
            this.model.set(t);
        }).fail(function() {
            this.$('.btn-red').addClass('act-close-topic')
                .html("关闭话题");
        });
    },

    onClickLikeTopic: function(e) {
        if (CurrentUser.isGuest()) {
            return alert("登录后才能点赞");
        }
        $(e.target).removeClass('btn-white act-like-topic')
            .addClass('btn-green')
            .html("已赞");
        var countString = this.$('.likes-count span').html(),
            count = parseInt(countString);
        count++;
        this.$('.likes-count span').html(count);

        $.ajax({
            url: '/api/contents/' + this.model.get('primer').cid + '/likes',
            type: 'POST',
            context: this
        }).fail(function() {
            count--;
            this.$('.likes-count span').html(count);
            $(e.target).removeClass('btn-green')
                .addClass('btn-white btn-like-topic')
                .html("赞此话题");
        });
    }

});

module.exports = TopicBar;
