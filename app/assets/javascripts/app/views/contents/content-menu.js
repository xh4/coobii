var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var UserAnchor = require('../users/anchor.js'),
    DeleteDropdown = require('./dropdown-delete.js'),
    contentMenuTemplate = require('../../templates/contents/content-menu.hbs');

var CurrentUser = require('../../states/current-user');

var ContentMenu = Backbone.View.extend({

    className: 'content-menu',

    render: function() {
        this.$el.html( contentMenuTemplate( this.model.toJSON() ) );

        if (this.isAuthor()) {
            this.$el.attr('data-is-author', true);
        } else {
            this.$el.attr('data-is-author', false);
        }

        var actions = ['like', 'share', 'mark'];
        _.each(actions, function(action) {
            if (this.model.get(action + 's_count') !==0) {
                this.$('.act-' + action + ' .content-menu-btn_count')
                    .addClass('not-zero');
            }
        }.bind(this));

        this.renderDropdowns();

        return this;
    },

    isAuthor: function() {
        return !CurrentUser.isGuest() &&
            CurrentUser.get('id') === this.model.get('user').id;
    },

    events: {
        "click .act-like": "postAction",
        "click .act-mark": "postAction",
        "click .act-share": "postAction",
        "click .act-comment": "onClickCommentButton"
    },

    postAction: function(e) {
        this.stopEvent(e);

        if (CurrentUser.isGuest()) {
            return alert('登录后才能操作');
        }

        var btn = $(e.target).closest('li'),
            action = btn.attr('class').match(/act-(\w+)/)[1],
            afterActionString = action === 'mark' ? 'marked' : action + 'd',
            url = "/api/contents/" + this.model.id + '/' + action + "s";

        var currentValue = this.model.get(afterActionString);
        var method = currentValue? "DELETE": "POST";
        this.model.set(afterActionString, !currentValue);

        $.ajax({
            url: url,
            type: method,
            timeout: 5000,
            data: {
                content_id: this.model.id
            }
        }).done(function(res) {
            var eventName;
            if (method === 'POST') {
                eventName = action + '-content';
            } else if (method === 'DELETE') {
                eventName = 'un' + action + '-content';
            }
            this.trigger(eventName, this.model);
            CurrentUser.trigger(eventName, this.model);
            this.model.set(res);
            this.render();
        }.bind(this)).fail(function() {

        }.bind(this));
    },

    hook: function() {
        var attrs = {};
        _.each(['shared', 'liked', 'marked'], function(s) {
            attrs[s] = this.$(".content-menu-btn").hasClass(s)? true: false;
        }.bind(this));
        this.model.set(attrs);

        this.renderDropdowns();
    },

    renderDropdowns: function() {
        this.deleteDropdown = new DeleteDropdown({
            toggler: this.$('.act-delete .act-dropdown-toggler')
        });
        this.deleteDropdown.render();
        this.listenTo(this.deleteDropdown, 'delete-content', function() {
            this.trigger('delete-content');
        });

    },

    onClickCommentButton: function(e) {
        this.trigger('click-comment', e);
    },

    stopEvent: function(e) {
        e.preventDefault();
        e.stopPropagation();
    }

});

module.exports = ContentMenu;
