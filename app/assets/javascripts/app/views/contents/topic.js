var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var TopicHeader = require('./topic-header'),
    TopicBar = require('./topic-bar'),
    TopicEvents = require('./topic-events'),
    Topic = require('../../models/topic'),
    Post = require('../../models/post'),
    PostView = require('./post'),
    Coobii = require('../../coobii');

var topicTemplate = require('../../templates/contents/topic.hbs');

var ContentStore = require('../../stores/content-store');

var TopicView = Backbone.View.extend({

    className: "topic",

    postsCount: 0,

    initialize: function(options) {

        _.extend(this, options);

        this.viewPoint = this.viewPoint || "page";

        this.header = new TopicHeader({
            model: this.model,
            viewPoint: this.viewPoint
        });

    },

    render: function() {
        this.$el.html( topicTemplate( this.model.toJSON() ) );

        this.header.render();
        this.listenTo(this.header, 'back', this.back);

        if (this.viewPoint === "showcase") {
            this.$el.prepend( this.header.el );
            var postView = new PostView({
                className: 'post post-sa',
                model: this.initPost
            });
            this.$('.posts').append( postView.render().el );
        } else if (this.viewPoint === "page") {
            this.loadPosts();
        }

        return this;
    },

    back: function() {
        this.header.remove();
        this.remove();
        this.trigger('back');
    },

    loadPosts: function() {
        ContentStore.loadPosts({
            topic_id: this.model.id
        }, function(posts, pagination) {
            _.each(posts, function(post, index) {
                post.set('floor', index + 1);
                var postView = new PostView({
                    className: 'post post-sa',
                    model: post
                });
                this.postsCount++;
                this.$('.posts').append( postView.render().el );
                if (post.get('primer')) {
                    this.model.set(post.get('topic'));
                    this.renderEvents();
                    this.renderBar();
                }
            }.bind(this));
            this.listenComposition();
        }.bind(this));
    },

    renderBar: function() {
        this.bar = new TopicBar({
            model: this.model
        });
        this.$('.posts').append( this.bar.render().el );
        this.bar.listenEvents();
    },

    renderEvents: function() {
        this.eventsView = new TopicEvents({
            model: this.model
        });
        this.$('.posts').append( this.eventsView.render().el );
    },

    listenComposition: function() {
        this.listenTo(ContentStore, 'new-composition:post', function(post) {
            this.postsCount++;
            post.set('floor', this.postsCount);
            var postView = new PostView({
                className: 'post post-sa',
                model: post
            });
            this.$('.posts').append( postView.render().el );
        });
    },

    hook: function(options) {
        this.model = new Topic({
            id: this.$el.data('id'),
            title: $(options.headerEl).find('.topic-title').html(),
            primer: {
                id: this.$('.primer').data('id'),
                cid: this.$('.primer').data('content-id')
            }
        });
        this.header = new TopicHeader({
            el: options.headerEl,
            model: this.model
        });
        this.listenTo(this.header, 'back', this.back);
        this.hookBar();
        this.hookEvents();
        this.hookPosts();
        this.listenComposition();
    },

    hookPosts: function() {
        this.$('.post').each(function(idx, el) {
            var id = $(el).data('id'),
                cid = $(el).data('content-id');
            var post = new Post({
                id: id,
                cid: cid
            });
            var postView = new PostView({
                el: el,
                model: post
            });
            postView.hook();
        });
    },

    hookBar: function() {
        this.topicBar = new TopicBar({
            el: this.$('.topic-bar'),
            model: this.model
        });
        this.topicBar.listenEvents();
    },

    hookEvents: function() {
        this.topicEvents = new TopicEvents({
            el: this.$('.topic-events'),
            model: this.model
        });
        this.topicEvents.listenEvents();
    }

});

module.exports = TopicView;
