var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var ContentView = Backbone.View.extend({

    className: "content"

});

module.exports = ContentView;
