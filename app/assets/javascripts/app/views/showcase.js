var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

require('jquery.transit');
require('ahoy');

var Coobii = require('../coobii.js'),
    Me = require('../me.js');

var Viewers = {
    "message": require('./viewers/viewer-message.js'),
    "post": require('./viewers/viewer-post.js')
};

var Showcase = Backbone.View.extend({

    el: '#showcase',

    initialize: function(options) {
        this.state = Coobii.getState('showcase');

        this.listenTo(this.state, 'change:display', this.onChangeState);
    },

    onChangeState: function(state, display, options) {
        if (display === 'show') {
            if (options.type === 'content') {
                this.viewContent(options);
            } else if (options.type === 'mio') {
                this.viewMio(options);
            }
        } else if (display === 'hide') {
            this.exit();
        }
    },

    html: function(dom) {
        $('#showcase .fm_m').html(dom);
    },

    show: function() {
        $('#showcase').show();
    },

    hide: function() {
        $('#showcase').hide();
    },

    fadeIn: function(fn) {
        $('#showcase').transition({
            'background-color': 'rgba(0, 0, 0, .5)'
        }, function() {
            if (fn) fn();
        });
    },

    fadeOut: function(fn) {
        $('#showcase').transition({
            'background-color': 'rgba(0, 0, 0, 0)'
        }, function() {
            if (fn) fn();
        });
    },

    clear: function(fn) {
        $('#showcase .fm_m').html("");
    },

    viewContent: function(options) {
        var showcase = this,
            contentType = options.contentType,
            model = options.content,
            viewerOptions = options.options,
            Viewer = Viewers[contentType];

        viewerOptions = _.extend({}, viewerOptions, {
            model: model
        });
        var viewer = new Viewer(viewerOptions);
        this.state.viewer = viewer;

        showcase.show();
        showcase.html(viewer.render().el);
        showcase.fadeIn();
        viewer.showContent();

        ahoy.track('View Content', {
            type: contentType,
            id: model.id
        });
    },

    exit: function() {
        var savedNS = this.state.get('savedNS'),
            savedTitle = this.state.get('savedTitle');
        if (savedNS) {
            window["router"].navigate('/' + savedNS, {replace: true});
        }
        if (savedTitle) {
            document.title = savedTitle;
        }
        this.fadeOut(function() {
            this.state.clear();
            this.hide();
            this.clear();
        }.bind(this));
    },

    viewMio: function(options) {
        var box = options.box;
        this.show();
        this.html(box.el);
        this.fadeIn();
        box.show();
    }

});

module.exports = Showcase;
