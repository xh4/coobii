var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var RadioField = require('./fields/field-radio.js'),
    Me = require('../me.js');

require('jquery.fileupload');
var loadImage = require('../vendor/load-image.js');
require('jquery.transit');

var backgroundBoxTemplate = require('../templates/background-box.hbs'),
    uploadTemplate = require('../templates/background-box-upload.hbs'),
    patternTemplate = require('../templates/background-box-pattern.hbs');

var CurrentUser = require('../states/current-user');

var BackgroundActions = require('../actions/background-actions');

var BackgroundBox = Backbone.View.extend({

    className: 'bg-box',

    render: function() {
        this.$el.html( backgroundBoxTemplate() );
        this.$('.switcher li[data-page-name=upload]').addClass('active');
        this.renderPage('upload');
        return this;
    },

    events: {
        "click .btn-close": "close",
        "click .switcher li": "changePage"
    },

    backgroundFile: null,

    styleChanged: false,

    renderUpload: function() {
        this.$('.bg-box_cnt').html( uploadTemplate() );

        this.displayField = new RadioField({
            name: "显示方式",
            options: [
                {
                    text: "全屏"
                },
                {
                    text: "纹理（平铺）"
                }
            ]
        });
        this.$('.display-field').html( this.displayField.render().el );
        var box = this;
        this.$('.dropzone input').fileupload({
            dropZone: this.$('.dropzone'),
            paramName: 'background',
            type: "PUT",
            url: "/api/users/background",
            add: function(e, data) {
                var file = data.files[0];
                this.backgroundFile = file;
                if (["image/jpeg",
                     "image/png"].indexOf(file.type) === -1) {
                } else {
                    this.image = file;
                    loadImage(file, function(img) {
                        this.loadImage(img);
                    }.bind(this), {
                        crop: false,
                        canvas: false
                    });
                }
            }.bind(this),
            send: function(e, data) {
                this.$('.dropzone').addClass('uploading');
                this.$('.dropzone_overlay').show();
                this.showOverlay();
            }.bind(this),
            progress: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                this.$('.image-upload-progress .bar').css('width', progress + "%");
            }.bind(this),
            done: function(e, data) {
                var u = data.result;
                CurrentUser.set('background', u.background);
                CurrentUser.updateBackgroundTimestamp();
                BackgroundActions.changeBackground({
                    url: CurrentUser.getBackgroundUrl(),
                    type: u.background.type
                });
                this.onceDone();
            }.bind(this),
            fail: function(e, data) {
                alert('更换背景失败');
            }.bind(this),
            always: function() {
                this.hideOverlay();
            }.bind(this)
        });
        return this;
    },

    loadImage: function(img) {
        this.$el.addClass('has-image');
        this.$(".dropzone").css('height', 'auto');
        this.$(".dropzone .help").remove();
        this.$(".dropzone .image").html(img);
        if (img.width <= 500 || img.height <= 500) {
            this.$('.radio-field input')[1].checked = true;
        } else {
            this.$('.radio-field input')[0].checked = true;
        }
        this.$('.dropzone input').fileupload('option', {
            formData: {
                background_attributes: JSON.stringify(this.getAttributes())
            }
        });
        this.$('.dropzone input').fileupload('send', {files: [this.backgroundFile]});
    },

    getAttributes: function() {
        var type = this.$('.radio-field input')[0].checked ? "fill": "tile";
        return {
            type: type
        };
    },

    onceDone: function() {
        this.$('.radio-field input').change(function(e) {
            this.updateAttributes();
        }.bind(this));
    },

    updateAttributes: function() {
        this.showOverlay();
        $.ajax({
            type: "PUT",
            url: '/api/users/background',
            data: {
                background_attributes: JSON.stringify(this.getAttributes())
            }
        }).done(function(res) {
            var u = res;
            CurrentUser.set('background', u.background);
            CurrentUser.updateBackgroundTimestamp();
            BackgroundActions.changeBackground({
                url: CurrentUser.getBackgroundUrl(),
                type: u.background.type
            });
        }.bind(this)).fail(function() {
            alert('更换背景失败');
        }.bind(this)).always(function() {
            this.hideOverlay();
        }.bind(this));
    },

    renderPattern: function() {
        this.$('.bg-box_cnt').html( patternTemplate() );
        return this;
    },

    close: function(e) {
        this.trigger('close');
    },

    renderPage: function(name) {
        if (name === 'upload') {
            this.renderUpload();
        } else if (name === 'pattern') {
            this.renderPattern();
        }
    },

    changePage: function(e) {
        var pageName = $(e.target).data('page-name');
        $('.switcher li').removeClass('active');
        $(e.target).addClass('active');
        this.renderPage(pageName);
    },

    showOverlay: function() {
        this.$('.bg-box_overlay').show();
    },

    hideOverlay: function() {
        this.$('.bg-box_overlay').hide();
        this.$('.dropzone_overlay').hide();
    }

});

module.exports = BackgroundBox;
