var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Slide = Backbone.View.extend({

    className: 'slide',

    width: 900,

    getTransEndAttrs: function() {
        return 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
    },

    acceptTransEvent: false,

    slideUp: function(fn) {
        this.acceptTransEvent = true;
        this.$el.removeClass('hide').one(this.getTransEndAttrs(), function(e) {
            if (e.target !== this.el || !this.acceptTransEvent) return;
            this.acceptTransEvent = false;
            fn();
        }.bind(this));
    },

    slideDown: function(fn) {
        this.acceptTransEvent = true;
        this.$el.addClass('hide').one(this.getTransEndAttrs(), function(e) {
            if (e.target !== this.el || !this.acceptTransEvent) return;
            this.acceptTransEvent = false;
            fn();
        }.bind(this));
    },

    hideBottom: function() {

    },

    setTop: function() {

    },

    pushHeader: function() {
        this.$el.addClass('h-in-fm');
        $('.slide-h-w').show();
        this.header.$el
            .css({
                'position': 'relative',
                'top': '0'
            })
            .appendTo('.slide-h-w');
    },

    pullHeader: function() {
        this.$el.removeClass('h-in-fm');
        this.header.$el.prependTo(this.$('.slide-card'));
        $('.slide-h-w').hide();
    },

    listenSliderEvents: function() {
        this.on('before-slide-up', this.beforeSlideUp);
        this.on('before-slide-down', this.beforeSlideDown);
        this.on('before-slide-in', this.beforeSlideIn);
        this.on('before-slide-out', this.beforeSlideOut);
        this.on('after-slide-up', this.afterSlideUp);
        this.on('after-slide-down', this.afterSlideDown);
        this.on('after-slide-in', this.afterSlideIn);
        this.on('after-slide-out', this.afterSlideOut);
    }

});

module.exports = Slide;
