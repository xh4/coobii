var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var fieldTemplate = require('../../templates/fields/field.hbs');

var Field = Backbone.View.extend({

    className: 'field',

    initialize: function(options) {
        this.name = options.name;
    },

    render: function() {
        this.$el.html( fieldTemplate );
        this.$('.field_label_tx').html( this.name );
        return this;
    }

});

module.exports = Field;
