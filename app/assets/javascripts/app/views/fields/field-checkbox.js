var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Field = require('./field.js');

var CheckboxField = Field.extend({

    initialize: function(options) {
        Field.prototype.initialize.call(this, options);
        this.defaultValue = options.default;
    },

    render: function() {
        Field.prototype.render.call(this);

        var ctrl = $('<span class="field_ctrl">')
                .appendTo(this.$('.field_ctrls')),
            input = $('<input type="checkbox">')
                .appendTo(ctrl);

        if (this.defaultValue === true) {
            input[0].checked = true;
        }

        return this;
    },

    events: {
        "click .field_label": function(e) {
            var input = this.$('input')[0];
            input.checked = !input.checked;
        }
    },

    getValue: function() {
        return this.$('input')[0].checked;
    }

});

module.exports = CheckboxField;
