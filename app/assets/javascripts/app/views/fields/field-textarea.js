var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Field = require('./field.js');

var TextareaField = Field.extend({

    className: 'textarea-field field',

    initialize: function(options) {
        Field.prototype.initialize.call(this, options);
    },

    render: function() {
        var field = this;

        Field.prototype.render.call(this);

        var textarea = $('<textarea></textarea>');
        this.$('.field_ctrls').html(textarea);

        return this;
    },

    getValue: function() {
        var value = this.$('textarea').val();
        return value;
    }

});

module.exports = TextareaField;
