var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Field = require('./field.js');

var RadioField = Field.extend({

    className: 'radio-field field',

    initialize: function(options) {
        Field.prototype.initialize.call(this, options);
        this.options = options.options;
    },

    render: function() {
        Field.prototype.render.call(this);

        _.each(this.options, $.proxy(function(option) {
            var span = $('<span class="field_ctrl"></span>'),
                input = $('<input>').attr({
                    type: 'radio',
                    name: this.name
                });
            span.append(input)
                .append(option.text)
                .appendTo(this.$('.field_ctrls'));
            if (option.default) {
                input[0].checked = true;
            }
        }, this));

        return this;
    },

    events: {
    },

    getValue: function() {
        var text = null;
        this.$('field_ctrl').each(function(index, el) {
            if ($(el).children('input')[0].checked) {
                text = el.textContent.trim();
            }
        });
        return text;
    }

});

module.exports = RadioField;
