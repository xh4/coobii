var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Field = require('./field.js');

var TextField = Field.extend({

    className: 'text-field field',

    initialize: function(options) {
        Field.prototype.initialize.call(this, options);
        this.type = options.type ? options.type : 'text';
        this.placeholder = options.placeholder ?
            options.placeholder :
            '';
        this.trim = options.trim;
    },

    cache: null,

    render: function() {
        var field = this;

        Field.prototype.render.call(this);

        var input = $('<input />').attr({
            type: this.type,
            placeholder: this.placeholder
        });
        this.$('.field_ctrls').html(input);

        setInterval(function() {
            var data = field.$('input').val();
            if (field.cache !== null && data !== field.cache) {
                field.trigger('change', data, field.cache);
            }
            field.cache = data;
        }, 50);

        return this;
    },

    getValue: function() {
        var value = this.$('input').val();
        if (this.trim) {
            value = value.trim();
        }
        return value;
    }

});

module.exports = TextField;
