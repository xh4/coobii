var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Field = require('./field.js'),
    ImagePicker = require('../image-picker.js'),
    avatarFieldTemplate = require('../../templates/fields/field-avatar.hbs');

var AvatarField = Field.extend({

    className: 'avatar-field field',

    diameter: 160,

    imagePickerDiameter: 200,

    changed: false,

    initialize: function(options) {
        Field.prototype.initialize.call(this, options);

        this.initAvatar = options.avatar;
    },

    render: function() {

        Field.prototype.render.call(this);

        this.$('.field_ctrls').html( avatarFieldTemplate() );

        if (this.initAvatar) {
            var src = this.initAvatar.original_url,
                img = $('<img>').attr('src', src)
                    .appendTo(this.$('.field_ctrls .image'));
            this.onChangeDisplay(this.initAvatar);
        } else {
            $('<img>').attr('src', "/assets/avatar_guest_400.png")
                .css({
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    left: 0,
                    top: 0
                })
                .appendTo(this.$('.field_ctrls .image'));
        }

        this.renderImagePicker();

        return this;
    },

    renderImagePicker: function() {
        this.imagePicker = new ImagePicker({
            initAvatar: this.initAvatar
        });
        this.$el.append( this.imagePicker.render().el );

        this.listenTo(this.imagePicker, 'cancel', this.toggleImagePicker);

        this.listenTo(this.imagePicker, 'change-image', this.onChangeImage);
        this.listenTo(this.imagePicker, 'change-display', this.onChangeDisplay);
    },

    events: {
        "click .avatar": "toggleImagePicker"
    },

    onChangeImage: function(imgSrc) {
        this.changed = true;
        var img = new Image;
        img.src = imgSrc;
        this.$('.avatar .image').html(img);
    },

    onChangeDisplay: function(attrs) {
        this.changed = true;
        var img = this.$('.avatar .image img')[0];
        var s = this.diameter / this.imagePickerDiameter,
            scale = s * attrs.scale / 100;
        img.width = attrs.width * scale;
        img.height = attrs.height * scale;
        var left = - s * attrs.offset[0],
            top = - s * attrs.offset[1];
        $(img).css({
            'position': 'absolute',
            'left': left,
            'top': top
        });
    },

    imagePickerOpened: false,

    toggleImagePicker: function(e) {
        if (this.imagePickerOpened) {
            this.imagePicker.$el.hide();
            this.imagePickerOpened = false;
        } else {
            this.imagePicker.$el.show();
            this.imagePickerOpened = true;
        }
    }

});

module.exports = AvatarField;
