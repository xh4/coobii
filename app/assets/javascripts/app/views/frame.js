var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Background = require('./background.js');

var Frame = Backbone.View.extend({

    className: 'fm',

    initialize: function(options) {
        this.background = new Background();
    },

    hook: function() {

    }
});

module.exports = Frame;
