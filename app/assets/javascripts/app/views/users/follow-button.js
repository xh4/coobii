var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var User = require('../../models/user.js'),
    Me = require('../../me.js');

var FollowBtn = Backbone.View.extend({

    model: User,

    tagName: 'button',

    className: 'follow-btn btn-white btn',

    ifFriend: false,

    render: function() {
        this.isFriend = this.model.get('is_friend');
        if (this.isFriend) {
            this.$el.attr('data-following', true);
            this.$el.html("<i class=\"icon-ok\"></i>正在关注");
        } else {
            this.$el.attr('data-following', false);
            this.$el.html("<i class=\"icon-plus\"></i>关注");
        }
        return this;
    },

    events: {
        "click": "click",
        "mouseover": "mouseover",
        "mouseleave": "mouseleave"
    },

    click: function(e) {
        var module = this,
            type = this.isFriend ? 'DELETE' : 'POST';

        $.ajax({
            url: '/api/friends',
            type: type,
            data: {
                user_id: this.model.get('id')
            }
        }).done(function(u) {
            module.model.set('is_friend', u.is_friend);
            module.render();
            if (type === 'DELETE') {
                Me.trigger('change-relation', 'delete', module.model);
                Me.trigger('delete-relation', module.model);
            } else if (type === 'POST') {
                Me.trigger('change-relation', 'add', module.model);
                Me.trigger('add-relation', module.model);
            }

        });
    },

    mouseover: function(e) {
        if (this.isFriend) {
            return this.$el.html("<i class=\"icon-minus\"></i>取消关注");
        }
    },

    mouseleave: function(e) {
        return this.render();
    }

});

module.exports = FollowBtn;
