var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SettingsBox = require('./settings-box.js'),
    Me = require('../../me.js'),
    TextField = require('../fields/field-text.js'),
    accountBoxTemplate = require('../../templates/users/settings-box-account.hbs');

var AccountBox = SettingsBox.extend({

    className: 'setting-box-account setting-box',

    render: function() {

        this.$el.html( accountBoxTemplate() );

        this.oldPassField = new TextField({
            name: '原密码',
            type: 'password'
        });
        this.$('.setting-box_cnt').append(this.oldPassField.render().el);

        this.newPassField = new TextField({
            name: '新密码',
            type: 'password'
        });
        this.$('.setting-box_cnt').append(this.newPassField.render().el);

        this.repeatPassField = new TextField({
            name: '重复输入',
            type: 'password'
        });
        this.$('.setting-box_cnt').append(this.repeatPassField.render().el);

        return this;

    },

    events: {
        "click .act-save": "save"
    },

    save: function(e) {
        var oldPass = this.oldPassField.getValue(),
            newPass = this.newPassField.getValue(),
            repeatPass = this.repeatPassField.getValue(),
            box = this;

        if (newPass !== repeatPass) {
            return alert('两次输入的新密码不匹配');
        }

        this.showSpinner();
        this.showOverlay();

        var btn = $(e.target);
        btn.addClass('disabled');

        $.ajax({
            url: '/api/users/password',
            type: 'PUT',
            data: {
                current_password: oldPass,
                new_password: newPass
            }
        }).done(function(user) {
            alert('密码修改成功');
        }).fail(function(user) {
            alert('密码修改失败');
        }).always(function() {
            btn.removeClass('disabled');
            box.hideSpinner();
            box.hideOverlay();
        });

    }

});

module.exports = AccountBox;
