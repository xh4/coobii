var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SlideContent = require('./content.js'),
    ContentsView = require('../timelines/contents.js'),
    Coobii = require('../../coobii.js');

var CurrentUser = require('../../states/current-user');

var SpaceContent = SlideContent.extend({

    className: 'space-content user-slide-content',

    initialize: function(options) {
        this.user = options.user;
        this.state = Coobii.createState('space-content', {
            name: 'index'
        });
        this.listenTo(this.state, 'change', this.onChangeState);
    },

    hookIndex: function(options) {
        this.contentsView = new ContentsView({
            el: this.$('.contents'),
            type: 'user',
            user: this.user
        });
        this.contentsView.hook();
    },

    onChangeState: function(state, value, options) {
        var name = state.get('name'),
            method = 'view' +
                name.charAt(0).toUpperCase() +
                name.slice(1);
        this[method](state, value, options);
    },

    viewIndex: function(state) {
        var contents = [{
            name: 'ContentsView',
            options: {
                type: 'user',
                user: this.user,
                limit: 10,
                contentType: ["message", "post"]
            }
        }];
        if ( CurrentUser.isLoggedIn() && CurrentUser.id === this.user.id )
            contents.unshift({
                name: 'Mio'
            });

        this.viewContents(contents);
    },

    viewTypified: function(s, v, options) {
        this.viewContents([{
            name: 'ContentsView',
            options: {
                type: 'user',
                limit: 10,
                user: this.user,
                contentType: s.get('contentType')
            }
        }]);
    }


});

module.exports = SpaceContent;
