var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var contentBoxTemplate = require('../../templates/users/content-box.hbs');

var ContentBox = Backbone.View.extend({

    className: 'content-box',

    initialize: function(options) {
        this.title = options.title;
    },

    render: function() {
        var attrs = {
            title: this.title
        };
        this.$el.html( contentBoxTemplate( attrs ) );

        console.log(this.collection);

        return this;
    }

});

module.exports = ContentBox;
