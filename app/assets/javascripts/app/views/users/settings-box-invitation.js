var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SettingsBox = require('./settings-box.js'),
    Me = require('../../me.js'),
    invitationBoxTemplate = require('../../templates/users/settings-box-invitation.hbs');

var InvitationBox = SettingsBox.extend({

    className: 'setting-box-invitation setting-box',

    render: function() {

        this.$el.html( invitationBoxTemplate() );



        return this;

    }

});

module.exports = InvitationBox;
