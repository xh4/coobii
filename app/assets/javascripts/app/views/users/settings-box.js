var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SettingsBox = Backbone.View.extend({

    showSpinner: function() {
        this.$('.setting-box_f .spinner').show();
    },

    hideSpinner: function() {
        this.$('.setting-box_f .spinner').hide();
    },

    showOverlay: function() {
        this.$('> .overlay').show();
    },

    hideOverlay: function() {
        this.$('> .overlay').hide();
    }

});

module.exports = SettingsBox;
