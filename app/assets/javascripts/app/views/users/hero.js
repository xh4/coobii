var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var BackgroundBox = require('../background-box.js'),
    Hero = require('../hero.js');

var AppState = require('../../states/app-state');

var UserAnchor = require('./anchor');

var UserHero = Hero.extend({

    initialize: function(options) {
        this.backgroundBox = new BackgroundBox;
    },

    events: {
        "click .sidebar .tabs li": "toggleSidebarContent",
        "click .act-show-slider": "showSlider",
        "click .act-change-bg": "toggleBackgroundBox"
    },

    hook: function() {
        var scrollbarWidth = this.getScrollBarWidth();
        this.$('.sidebar .main').css({
            'margin-right': -scrollbarWidth + "px",
            'overflow-y': 'scroll'
        });

        this.$('.user-a').each(function(idx, el) {
            var userAnchor = new UserAnchor({
                el: el
            });
            userAnchor.hook();
        });
    },

    toggleSidebarContent: function(e) {
        e.preventDefault();

        var tab = $(e.target).closest('li'),
            hero = this;
        if (tab.hasClass('active')) return;

        this.$('.sidebar .tabs li').removeClass('active');
        tab.addClass('active');

        var contentName = tab.attr('class').match(/tab-(\w+)/)[1];
        this.$('.sidebar .main .cnt:visible').fadeOut(function() {
            hero.$(".sidebar .main .cnt-" + contentName).fadeIn();
        });
    },

    showSlider: function(e) {
        AppState.set('place', 'space');
    },

    getScrollBarWidth: function() {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);
    },

    backgroundBoxOpened: false,
    toggleBackgroundBox: function(e) {
        if (this.backgroundBoxOpened) {
            this.closeBackgroundBox();
            this.backgroundBoxOpened = false;
        } else {
            this.openBackgroundBox();
            this.backgroundBoxOpened = true;
        }
    },

    openBackgroundBox: function() {
        $('.hero_cnt').html( this.backgroundBox.render().el );
        this.listenToOnce(this.backgroundBox, 'close', this.toggleBackgroundBox);
    },

    closeBackgroundBox: function() {
        this.backgroundBox.$el.detach();
        $('.hero_cnt').empty();
    }

});

module.exports = UserHero;
