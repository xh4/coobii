var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Me = require('../../me.js'),
    Sidebar = require('./sidebar.js'),
    Settings = require('./sidebar-modules/settings.js');

var SettingsSidebar = Sidebar.extend({

    className: 'settings-sidebar sidebar',

    initialize: function(options) {

        this.renderPlaceholder();

    },

    renderPlaceholder: function() {
        this.$el.html("<div class='ph-settings'></div> \
                      <div class='sidebar-overlay'></div> \
                      ");
    },

    load: function() {
        var settings = this.loadModule(Settings,
                                      null,
                                      this.$('.ph-settings'));
    }


});

module.exports = SettingsSidebar;
