var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Content = require('./content.js'),
    Coobii = require('../../coobii.js'),
    Me = require('../../me.js');

var Boxes = {
    "profile": require('./settings-box-profile.js'),
    "account": require('./settings-box-account.js'),
    "invitation": require('./settings-box-invitation.js')
};

var SettingsContent = Content.extend({

    className: 'settings-content user-slide-content',

    initialize: function() {
        this.state = Coobii.createState('settings-content');
        this.listenTo(this.state, 'change:name', this.onChangeState);
    },

    onChangeState: function(state, value, options) {
        var name = value;
        this.viewBox(name);
    },

    viewBox: function(name) {
        var Box = Boxes[name],
            box = new Box;
        this.$el.html( box.render().el );
    }

});

module.exports = SettingsContent;
