var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var AppState = require('../../states/app-state.js');

var SlideHeader = require("./slide-header.js");

var SpaceHeader = SlideHeader.extend({

    className: 'space_h slide_h user-slide_h',

    events: {
        "click .slide-toggler": "onClickToggler"
    },

    onClickToggler: function(e) {
        AppState.set('place', 'hero');
    }

});

module.exports = SpaceHeader;
