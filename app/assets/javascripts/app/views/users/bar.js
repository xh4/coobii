var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var UserAnchor = require('./anchor.js'),
    FollowButton = require('./follow-button.js'),
    userBarTemplate = require('../../templates/users/bar.hbs');

var UserBar = Backbone.View.extend({

    className: 'user-bar',

    initialize: function(options) {
        this.userAnchor = new UserAnchor({
            model: this.model
        });
        this.followButton = new FollowButton({
            model: this.model
        });
    },

    render: function() {
        this.$el.html( userBarTemplate( this.model.toJSON() ) );

        this.$('.user-bar_h').html( this.userAnchor.render().el );
        this.$('.user-bar_side').html( this.followButton.render().el );

        return this;
    }

});

module.exports = UserBar;
