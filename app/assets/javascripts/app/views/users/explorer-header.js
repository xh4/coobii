var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SlideHeader = require("./slide-header.js");

var ExplorerHeader = SlideHeader.extend({

    className: 'explorer_h slide_h user-slide_h',

    render: function() {
        this.$el.html("<span class='user-slide_h_title'>浏览</span>");
        return this;
    }

});

module.exports = ExplorerHeader;
