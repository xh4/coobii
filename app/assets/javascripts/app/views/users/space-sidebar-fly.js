var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarFly = require('./sidebar-fly.js');

var SpaceSidebarFly = Backbone.View.extend({

    className: 'space-sidebar-fly sidebar-fly'

});

module.exports = SpaceSidebarFly;
