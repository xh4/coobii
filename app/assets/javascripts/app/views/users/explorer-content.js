var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SlideContent = require('./content.js'),
    ContentBox = require('./content-box.js'),
    User = require('../../models/user.js'),
    UserBar = require('./bar.js'),
    Coobii = require('../../coobii.js'),
    Me = require('../../me.js');

var ExplorerContent = SlideContent.extend({

    className: 'explorer-content user-slide-content',

    initialize: function() {
        this.state = Coobii.createState('explorer-content');
        this.listenTo(this.state, 'change', this.onChangeState);
    },

    onChangeState: function(state, value, options) {
        var name = state.get('name'),
            method,
            capitalise = function(s) {
                return s.charAt(0).toUpperCase() +
                    s.slice(1);
            };
        if (['index',
             'friends',
             'followers',
             'likes',
             'marks'].indexOf(name) !== -1) {
            method = 'view' + capitalise(name);
        } else {
            var m = name.match(/(marked|liked)-(messages|works)/);
            method = 'view' + capitalise(m[1]) + capitalise(m[2]);
        }
        var slideContent = this;
        slideContent[method](state, value, options);
    },

    viewIndex: function(state) {
        this.viewContents([{
            name: 'ContentsView',
            options: {
                type: 'timeline',
                user: Me,
                limit: 10,
                contentType: ["message", "post"]
            }
        }]);
    },

    viewFollowers: function(state) {
        this.viewContents([{
            name: 'Users',
            options: {
                title: "我的关注者",
                url: "/api/followers"
            }
        }]);
    },

    viewFriends: function(state) {
        this.viewContents([{
            name: 'Users',
            options: {
                title: "我关注的",
                url: "/api/friends"
            }
        }]);
    },

    viewLikes: function(state) {
        this.viewContents([{
            name: 'ContentsView',
            options: {
                type: 'likes',
                user: Me,
                limit: 10,
                contentType: ["message", "post"]
            }
        }]);
    },

    viewMarks: function(state) {
        this.viewContents([{
            name: 'ContentsView',
            options: {
                type: 'marks',
                user: Me,
                limit: 10,
                contentType: ["message", "post"]
            }
        }]);
    },

    loadUsers: function(options, fn) {
        var content = this;
        var contentBox = new ContentBox({
            title: options.title
        });
        contentBox.render().$el.hide();
        contentBox.$('.content-box_b').html("<ol></ol>");

        var renderUsers = function(users) {
            _.each(users, function(u) {
                var user = new User(u),
                    userBar = new UserBar({
                        model: user
                    });
                contentBox.$('ol').append( userBar.render().el );
            });
        };

        $.get(options.url).done(function(users) {
            renderUsers(users);
            content.$el.append( contentBox.el );
            contentBox.$el.fadeIn(function() {
                if (fn) fn();
            });
        });

    }

});

module.exports = ExplorerContent;
