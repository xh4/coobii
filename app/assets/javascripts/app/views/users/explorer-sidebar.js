var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Sidebar = require('./sidebar.js'),
    Explore = require('./sidebar-modules/explore.js'),
    MessageBox = require('../../mio/box-message.js'),
    Suggestions = require('./sidebar-modules/suggestions.js');

var ExplorerSidebar = Sidebar.extend({

    className: 'explorer-sidebar sidebar',

    initialize: function(options) {

        this.renderPlaceholder();

    },

    renderPlaceholder: function() {
        this.$el.html("<div class='ph-explore'></div> \
                      <div class='ph-message-box'></div> \
                      <div class='ph-suggestions'></div> \
                      <div class='ph-trend'></div> \
                      <div class='ph-radar'></div> \
                      <div class='sidebar-overlay'></div> \
                      ");
    },

    load: function() {

        var explore = this.loadModule(Explore,
                                      null,
                                      this.$('.ph-explore'));

        var messageBox = this.loadModule(MessageBox, {
            className: 'sidebar-module mio-box-message',
            owner: 'sidebar',
            maxImageSize: 188
        }, this.$('.ph-message-box'));
        messageBox.render();
        messageBox.trigger('render-ready');
        messageBox.on('post-done', function() {
            this.clear();
        });

        var suggestions = this.loadModule(Suggestions,
                                          null,
                                          this.$('.ph-suggestions'));
    }

});

module.exports = ExplorerSidebar;
