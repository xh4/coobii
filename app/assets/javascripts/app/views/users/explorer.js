var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Header = require('./explorer-header.js'),
    Sidebar = require('./explorer-sidebar.js'),
    Content = require('./explorer-content.js'),
    Slide = require('../slide.js');

var explorerTemplate = require('../../templates/users/explorer.hbs');

var Explorer = Slide.extend({

    className: 'explorer user-slide slide hide',

    name: 'explorer',

    initialize: function() {
        this.header = new Header;

        this.listenSliderEvents();
    },

    render: function() {
        this.$el.html( explorerTemplate() );
        this.$('.slide-card').prepend( this.header.render().el );
        return this;
    },

    loadSidebar: function() {
        this.sidebar = new Sidebar();
        this.$('.explorer_b').append( this.sidebar.el );
        this.sidebar.load();
    },

    loadIndexContent: function() {
        this.content = new Content;
        this.$('.explorer_cnt').html( this.content.el );
        this.content.state.set('name', 'index');
    },

    loaded: false,

    beforeSlideIn: function() {
        this.render();
        this.$el.removeClass('hide');
        this.trigger('ready');
    },

    beforeSlideUp: function() {
        if (!this.loaded) this.render();
        this.trigger('ready');
    },

    beforeSlideOut: function() {
        this.detach();
    },

    beforeSlideDown: function() {
        this.detach();
    },

    afterSlideIn: function() {
        this.resume();
        this.load();
    },

    afterSlideUp: function() {
        this.resume();
        if (!this.loaded) this.load();
    },

    afterSlideOut: function() {
        this.recover();
        this.kill();
    },

    afterSlideDown: function() {
        this.recover();
    },

    load: function() {
        this.loaded = true;
        this.loadSidebar();
        this.loadIndexContent();
    },

    detach: function() {
        this.pullHeader();
        this.header.$el.css({
            top: this.el.scrollTop
        });
    },

    resume: function() {
        this.pushHeader();
    },

    recover: function() {
        this.$el.addClass('hide');
        this.el.scrollTop = 0;
        this.header.$el.css('top', 0);
        this.setTop();
    },

    kill: function() {
        this.loaded = false;
        this.$el.detach();
    }

});

module.exports = Explorer;
