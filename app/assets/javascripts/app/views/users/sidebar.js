var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Sidebar = Backbone.View.extend({

    className: 'sidebar',

    minLineHeight: 30,

    lineTop: 44,

    loadModule: function(Module, options, ph) {
        var module = new Module(options);
        this.listenToOnce(module, 'render-ready', function() {
            $(ph).after(module.el).remove();
        });
        module.render();
        return module;
    }
});

module.exports = Sidebar;
