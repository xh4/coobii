var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js');

var MessageBox = SidebarModule.extend({

    className: 'sidebar-module-message-box \
    message-box \
    user-sidebar-module \
    sidebar-module',

    initialize: function(options) {
        this.user = options.user;
    },

    render: function() {
        var module = this;

        this.user.fetch()
            .done(function() {
                module.$el.html("myblog");
                module.trigger("render-ready");
        });
    }


});

module.exports = MessageBox;
