var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    UserAnchor = require('../anchor.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js');

var notificationsTemplate = require('../../../templates/sidebar-modules/notifications.hbs');

var Notifications = SidebarModule.extend({

    className: 'sidebar-module-notifications user-sidebar-module sidebar-module',

    initialize: function() {

    },

    render: function() {
        this.$el.html( notificationsTemplate() );

        this.trigger("render-ready");
    }

});

module.exports = Notifications;
