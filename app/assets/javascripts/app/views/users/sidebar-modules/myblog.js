var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    UserAnchor = require('../anchor.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js');

var myblogTemplate = require('../../../templates/sidebar-modules/myblog.hbs');

var ContentStore = require('../../../stores/content-store');

var Myblog = SidebarModule.extend({

    className: 'sidebar-module-myblog user-sidebar-module sidebar-module',

    initialize: function(options) {
        this.user = options.user;
    },

    template: myblogTemplate,

    render: function() {
        var module = this;

        this.user.fetch().done($.proxy(function() {

            this.$el.html( this.template( this.user.toJSON() ) );

            var userAnchor = new UserAnchor({
                model: this.user
            });
            this.$('.sidebar-module_h').prepend(userAnchor.render().el);

            this.$('.count').each(function(index, el) {
                if ($(el).text() === '0') {
                    $(el).empty();
                }
            });

            this.listenTo(ContentStore, 'new-composition', function(content) {
                this.changeCount(content.get('type'), "+");
            });
            this.listenTo(ContentStore, "remove", function(content) {
                this.changeCount(content.get('type'), "-");
            });

            this.trigger("render-ready");

        }, module));
    },

    changeCount: function(type, action) {
        var numString = this.$(".sidebar-module-menus li[data-content-type=" + type + "] .count").html(),
            num = parseInt(numString);
        num = isNaN(num) ? 0 : num;
        if (action === "+") {
            num++;
        } else if (action === "-") {
            num--;
        }
        num = num === 0 ? null : num;
        this.$(".sidebar-module-menus li[data-content-type=" + type + "] .count").html(num);
    },

    events: {
        "click .sidebar-module_h": function(e) {
            e.preventDefault();
            this.$('.sidebar-module-menus li').removeClass('active');
            this.changeState({
                name: 'index'
            });
        },
        "click .sidebar-module-menus li": function(e) {
            var menu = $(e.target).closest('li');
            menu.siblings().removeClass('active');
            menu.addClass('active');
            var type = menu.attr('data-content-type');

            this.changeState({
                name: 'typified',
                contentType: type
            });
        }
    },

    changeState: function(s) {
        var state = Coobii.getState('space-content');
        state.set(s);
    }

});

module.exports = Myblog;
