var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js');

var Settings = SidebarModule.extend({

    className: 'sidebar-module-settings user-sidebar-module sidebar-module',

    template: require('../../../templates/sidebar-modules/settings.hbs'),

    render: function() {
        this.$el.html( this.template() );
        this.trigger('render-ready');
    },

    events: {
        "click li": function(e) {
            var menu = $(e.target).closest('li'),
                name = menu.data('page');
            this.changeState(name);
        }
    },

    changeState: function(name) {
        var state = Coobii.getState('settings-content');
        state.set('name', name);
    }

});

module.exports = Settings;
