var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js');

var Radar = SidebarModule.extend({

    className: 'sidebar-module-radar user-sidebar-module sidebar-module',

    initialize: function() {

    },

    template: require('../../../templates/sidebar-modules/suggestions.hbs'),

    render: function() {
        var module = this;

    }

});

module.exports = Radar;
