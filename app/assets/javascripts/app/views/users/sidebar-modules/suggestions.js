var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    UserAnchor = require('../anchor.js'),
    User = require('../../../models/user.js'),
    FollowButton = require('../follow-button.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js');

var Suggestions = SidebarModule.extend({

    className: 'sidebar-module-suggestions user-sidebar-module sidebar-module',

    initialize: function() {

    },

    template: require('../../../templates/sidebar-modules/suggestions.hbs'),

    render: function() {
        var module = this;

        this.$el.html( this.template( Me.toJSON() ) );

        $.ajax({
            type: 'GET',
            url: '/api/suggestions/users'
        }).done(function(users) {
            _.each(users, function(u) {
                var user = new User(u),
                    userAnchor = new UserAnchor({
                        model: user
                    }),
                    followButton = new FollowButton({
                        model: user
                    }),
                    li = $('<li></li>');
                li.append(userAnchor.render().el);
                li.append(followButton.render().el);
                li.appendTo(module.$('ul'));
            });
            if (users.length !== 0) {
                module.trigger('render-ready');
            }
        });
    }

});

module.exports = Suggestions;
