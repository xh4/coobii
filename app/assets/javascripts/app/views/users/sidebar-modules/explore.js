var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = require('./module.js'),
    UserAnchor = require('../anchor.js'),
    Me = require('../../../me.js'),
    Coobii = require('../../../coobii.js'),
    exploreTemplate = require('../../../templates/sidebar-modules/explore.hbs');

var Explore = SidebarModule.extend({

    className: 'sidebar-module-explore user-sidebar-module sidebar-module',

    initialize: function() {

    },

    render: function() {
        var module = this;
        Me.fetch().then(function() {
            module.$el.html( exploreTemplate( Me.toJSON() ) );
            var userAnchor = new UserAnchor({
                model: Me
            });
            module.$('.sidebar-module_h').prepend(userAnchor.render().el);
            module.trigger("render-ready");

            module.listenTo(Me, 'change-relation', module.onChangeRelation);
        });
    },

    events: {
        "click .user-stat-friends": function(e) {
            this.changeState('friends');
        },
        "click .user-stat-followers": function(e) {
            this.changeState('followers');
        },
        "click .sidebar-module-menus li": function(e) {
            var menu = $(e.target).closest('li'),
                name = menu.attr('data-li-name');
            this.changeState(name);
        }
    },

    changeState: function(name, options) {
        var state = Coobii.getState('explorer-content'),
            attr = _.extend({
                name: name
            }, options);
        state.set(attr);
    },

    onChangeRelation: function(type, user) {
        var currentNum = parseInt( this.$('.user-stat-friends strong').html() );
        if (type === 'add')
            currentNum++;
        else if (type === 'delete')
            currentNum--;
        this.$('.user-stat-friends strong').html(currentNum);
    }

});

module.exports = Explore;
