var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarModule = Backbone.View.extend({

    className: 'user-sidebar-module sidebar-module'

});

module.exports = SidebarModule;
