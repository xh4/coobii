var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SlideHeader = Backbone.View.extend({

    className: 'slide_h user-slide_h'

});

module.exports = SlideHeader;
