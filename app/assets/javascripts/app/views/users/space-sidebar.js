var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Sidebar = require('./sidebar.js'),
    Myblog = require('./sidebar-modules/myblog.js'),
    MessageBox = require('../../mio/box-message.js'),
    Notifications = require('./sidebar-modules/notifications.js');

var Me = require('../../me.js');

var SpaceSidebar = Sidebar.extend({

    className: 'space-sidebar sidebar',

    initialize: function(options) {

        this.user = options.user;

        this.renderPlaceholder();
    },

    renderPlaceholder: function() {
        this.$el.html("<div class='ph-myblog'></div> \
                      <div class='ph-message-box'></div> \
                      <div class='ph-notifications'></div>\
                      <div class='sidebar-overlay'></div> \
                      ");
    },

    load: function() {
        var myblog = this.loadModule(Myblog, {
            user: this.user
        }, this.$('.ph-myblog'));

        if (!Me.isGuest()) {
            var messageBox = this.loadModule(MessageBox, {
                className: 'sidebar-module mio-box-message',
                owner: 'sidebar',
                maxImageSize: 188
            }, this.$('.ph-message-box'));
            messageBox.render();
            messageBox.trigger('render-ready');
            messageBox.on('post-done', function() {
                this.clear();
            });
        }
    }


});

module.exports = SpaceSidebar;
