var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Mio = require('../../mio/mio.js'),
    ContentsView = require('../timelines/contents.js');

var Content = Backbone.View.extend({

    className: 'user-slide-content',

    viewContents: function(contents) {
        var content = this;
        this.$el.fadeOut(400, function() {
            $(this).empty().show();
            content.loadContents(contents);
        });
    },

    loadContents: function(contents) {
        var _this = this;
        var content = contents[0],
            name = content.name,
            options = content.options,
            method = 'load' +
                name.charAt(0).toUpperCase() +
                name.slice(1),
            nextContent = contents[1];
        this[method](options, function() {
            if (nextContent) {
                _this.loadContents(contents.slice(1));
            } else {
                // all complete
            }
        });
    },

    loadMio: function(options, fn) {
        var mio = new Mio,
            $el = mio.render().$el.hide();
        this.$el.append( $el );
        $el.fadeIn(function() {
            fn();
        });
    },

    loadContentsView: function(options, fn) {
        var contentsView = new ContentsView(options),
            $el = contentsView.$el.hide();
        this.$el.append( $el );
        contentsView.renderAndLoad(function() {
            $el.fadeIn(function() {
                fn();
            });
        });
    }

});

module.exports = Content;
