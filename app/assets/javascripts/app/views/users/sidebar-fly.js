var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SidebarFly = Backbone.View.extend({

    className: 'sidebar-fly'

});

module.exports = SidebarFly;
