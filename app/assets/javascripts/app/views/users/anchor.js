var $ = require('jquery'),
    Backbone = require('backbone'),
    Handlebars = require('handlebars');
Backbone.$ = $;

var User = require('../../models/user.js'),
    userAnchorTemplate = require('../../templates/users/anchor.hbs');

var CurrentUser = require('../../states/current-user');

var UserAnchor = Backbone.View.extend({

    tagName: 'a',

    className: 'user-a',

    attributes: {
        rel: 'nofollow',
        target: '_blank'
    },

    template: userAnchorTemplate,

    initialize: function(options) {
        if (options.model &&
            CurrentUser.isLoggedIn() &&
            options.model.id === CurrentUser.id)
            this.listenTo(this.model, 'change', this.render);
    },

    render: function(options) {

        this.$el.html(this.template( this.model.toJSON() ));
        this.$el.attr('href', "/" + this.model.get('username'));

        var avatarUrl = this.model.getAvatarUrl();
        if (avatarUrl) {
            var cssAttr = "url(" + avatarUrl + ")";
            this.$('.user-a_avatar').css('background-image', cssAttr);
            var img = new Image;
            img.src = avatarUrl;
            this.$('.user-a_avatar').html(img);
        }

        return this;
    },

    hook: function() {
        var attrs = {
            id: this.$el.data('id'),
            username: this.$el.data('username'),
            username_lower: this.$el.data('username').toLowerCase(),
            display_name: this.$el.data('display-name')
        };
        if (CurrentUser.isLoggedIn() && attrs.id === CurrentUser.id) {
            this.model = CurrentUser;
            this.listenTo(this.model, 'change', this.render);
            this.render();
        } else {
            this.model = new User(attrs);
        }
    }

});

module.exports = UserAnchor;
