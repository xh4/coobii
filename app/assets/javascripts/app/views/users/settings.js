var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Header = require('./slide-header.js'),
    Sidebar = require('./settings-sidebar.js'),
    Content = require('./settings-content.js'),
    Slide = require('../slide.js');

var settingsTemplate = require('../../templates/users/settings.hbs');

var Settings = Slide.extend({

    className: 'settings user-slide slide hide',

    name: 'settings',

    initialize: function() {
        this.header = new Header;

        this.listenSliderEvents();
    },

    render: function() {
        this.$el.html( settingsTemplate() );
        this.$('.slide-card').prepend( this.header.render().el );
        this.$('.user-slide_h').html("<span class='user-slide_h_title'>设置</span>");
        return this;
    },

    load: function() {
        this.loadSidebar();
        this.loadProfileContent();
    },

    loadSidebar: function() {
        this.sidebar = new Sidebar;
        this.$('.settings_b').append( this.sidebar.el );
        this.sidebar.load();
    },

    loadProfileContent: function() {
        this.content = new Content;
        this.$('.settings_cnt').html( this.content.el );
        this.content.state.set('name', 'profile');
    },

    loaded: false,

    beforeSlideIn: function() {
        this.render();
        this.$el.removeClass('hide');
        this.trigger('ready');
    },

    beforeSlideUp: function() {
        if (!this.loaded) this.render();
        this.trigger('ready');
    },

    beforeSlideOut: function() {
        this.detach();
    },

    beforeSlideDown: function() {
        this.detach();
    },

    afterSlideIn: function() {
        this.resume();
        this.load();
    },

    afterSlideUp: function() {
        this.resume();
        if (!this.loaded) this.load();
    },

    afterSlideOut: function() {
        this.recover();
        this.kill();
    },

    afterSlideDown: function() {
        this.recover();
    },

    load: function() {
        this.loaded = true;
        this.loadSidebar();
        this.loadProfileContent();
    },

    detach: function() {
        this.pullHeader();
        this.header.$el.css({
            top: this.el.scrollTop
        });
    },

    resume: function() {
        this.pushHeader();
    },

    recover: function() {
        this.$el.addClass('hide');
        this.el.scrollTop = 0;
        this.header.$el.css('top', 0);
        this.setTop();
    },

    kill: function() {
        this.loaded = false;
        this.$el.detach();
    }

});

module.exports = Settings;
