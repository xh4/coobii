var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Coobii = require('../../coobii'),
    CurrentUser = require('../../states/current-user'),
    User = require('../../models/user'),
    UserAnchor = require('./anchor'),
    Slide = require("../slide"),
    Header = require('./space-header'),
    Sidebar = require('./space-sidebar'),
    Content = require('./space-content'),
    Mio = require('../../mio/mio') ;

var Space = Slide.extend({

    className: 'space user-slide slide',

    name: 'space',

    initialize: function(options) {
        this.header = new Header({
            el: this.$('.space_h')
        });

        if (CurrentUser.isGuest()) {
            this.user = new User({
                id: $('#user-fm').data('user-id'),
                username: $('#user-fm').data('user-username'),
                display_name: $('#user-fm').data('user-display-name')
            });
        } else {
            this.user = CurrentUser;
        }

        this.listenSliderEvents();
    },

    hook: function() {
        this.hookHeader();
        this.hookMio();
        this.hookIndexContent();
    },

    hookHeader: function() {
        var userAnchor = new UserAnchor({
            el: this.header.$('.user-a')
        });
        userAnchor.hook();
    },

    hookMio: function() {
        var mio = new Mio({
            el: this.$('.mio')
        });
    },

    hookIndexContent: function() {
        this.content = new Content({
            user: this.user,
            el: this.$('.space-content')
        });
        this.content.hookIndex();
    },

    loadSidebar: function() {
        this.sidebar = new Sidebar({
            user: this.user
        });
        this.$('.space_b').append(this.sidebar.el);
        this.sidebar.load();
    },

    loaded: false,

    beforeSlideIn: function() {
        this.$el.show();
        this.$el.removeClass('hide');
        this.trigger('ready');
    },

    beforeSlideUp: function() {
        this.$el.show();

        if ($('html').hasClass('no-cssscrollbar')) {

        }

        this.trigger('ready');
    },

    beforeSlideOut: function() {
        this.detach();
    },

    beforeSlideDown: function() {
        this.detach();
    },

    afterSlideIn: function() {
        this.resume();
        if (!this.loaded) this.load();
    },

    afterSlideUp: function() {
        this.resume();
        if (!this.loaded) this.load();
    },

    afterSlideOut: function() {
        this.recover();
        this.kill();
    },

    afterSlideDown: function() {
        this.recover();
    },

    load: function() {
        this.loaded = true;
        this.loadSidebar();
        this.hook();
    },

    detach: function() {
        this.pullHeader();
        this.header.$el.css({
            top: this.el.scrollTop
        });
    },

    resume: function() {
        this.pushHeader();
    },

    recover: function() {
        this.el.scrollTop = 0;
        this.$el.addClass('hide');
        this.header.$el.css('top', 0);
        this.setTop();
    },

    kill: function() {
        this.$el.detach();
    }

});

module.exports = Space;
