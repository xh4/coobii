var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var SettingsBox = require('./settings-box.js'),
    CurrentUser = require('../../states/current-user'),
    AvatarField = require('../fields/field-avatar.js'),
    TextField = require('../fields/field-text.js'),
    TextareaField = require('../fields/field-textarea.js'),
    profileBoxTemplate = require('../../templates/users/settings-box-profile.hbs');

var ProfileBox = SettingsBox.extend({

    className: 'setting-box-profile setting-box',

    render: function() {
        var box = this;

        this.$el.html( profileBoxTemplate() );

        CurrentUser.fetch().done(function() {
            box.renderFields();
        });

        return this;
    },

    renderFields: function() {

        var avatar = CurrentUser.get('avatar');
        avatar.url = CurrentUser.getAvatarUrl();
        avatar.original_url = CurrentUser.getOrigAvatarUrl();

        this.avatarField = new AvatarField({
            name: '头像',
            avatar: avatar
        });
        this.$('.setting-box_cnt').append( this.avatarField.render().el );

        this.displayNameField = new TextField({
            name: '昵称'
        });
        this.$('.setting-box_cnt').append( this.displayNameField.render().el );
        this.displayNameField.$('input').val( CurrentUser.get('display_name') );

        this.headlineField = new TextField({
            name: '签名'
        });
        this.$('.setting-box_cnt').append( this.headlineField.render().el );
        this.headlineField.$('input').val( CurrentUser.get('headline') );

        this.bioField = new TextareaField({
            name: '简介'
        });
        this.$('.setting-box_cnt').append( this.bioField.render().el );
        this.bioField.$('textarea').val( CurrentUser.get('bio') );
    },

    events: {
        "click .act-save": "save"
    },

    save: function(e) {

        var btn = $(e.target),
            box = this;

        this.showSpinner();
        this.showOverlay();

        var data = {
            display_name: this.displayNameField.getValue(),
            headline: this.headlineField.getValue(),
            bio: this.bioField.getValue()
        };

        this.avatarField.imagePicker.request({
            url: "/api/users",
            type: "PUT",
            formData: {
                display_name: this.displayNameField.getValue(),
                headline: this.headlineField.getValue(),
                bio: this.bioField.getValue()
            },
            done: function(res) {
                if (this.avatarField.changed) {
                    CurrentUser.updateAvatarTimestamp();
                }
                CurrentUser.set(res);
                alert('更新成功');
            }.bind(this),
            fail: function() {
                alert('更新失败');
            }.bind(this),
            always: function() {
                this.hideSpinner();
                this.hideOverlay();
            }.bind(this)
        });

    }

});

module.exports = ProfileBox;
