var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Button = Backbone.View.extend({

    className: 'btn'

});

module.exports = Button;
