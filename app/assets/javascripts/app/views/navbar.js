var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var PageNavigator = require('./pages/navigator.js'),
    UserAnchor = require('./users/anchor.js'),
    navbarTemplate = require('../templates/navbar.hbs');

var AppState = require('../states/app-state.js'),
    AppActions = require('../actions/app-actions.js');

var CurrentUser =  require('../states/current-user');

var Navbar = Backbone.View.extend({

    el: '#navbar',

    initialize: function(options) {

        this.pageNavigator = new PageNavigator();

        this.render();

        this.listenTo(AppState, "change:place", this.renderState);
    },

    template: navbarTemplate,

    render: function() {


        if (!CurrentUser.isGuest()) {

            this.$('.navbar_inner').append( this.template() );
            this.$('.default-invisible').append( this.pageNavigator.render().el );

            var userAnchor = new UserAnchor({
                model: CurrentUser
            });
            this.$('.userpin').html( userAnchor.render().el );

            this.$('.default-invisible').fadeIn();
        }

        return this;
    },

    events: {
        "click .logo": "onClickLogo",
        "click .nav-tabs_tab": "onClickTab",
        "click .act-tab-logout": "logout"
    },

    logout: function() {
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: '/sessions'
        }).done(function() {
            window.location.href = '/';
        });
    },

    onClickLogo: function(e) {
        if (this.locked) return;
        if (!CurrentUser.isGuest()) {
            e.preventDefault();
            AppActions.togglePlace('hero');
        }
    },

    onClickTab: function(e) {
        if (this.locked) return;
        var tab = $(e.target).closest('li'),
            naCurrentUser = tab.attr('class').match(/act-tab-(\w+)/)[1];
        if (['space', 'explorer', 'settings'].indexOf(naCurrentUser) !== -1) {
            if (tab.hasClass('active')) {
                AppActions.togglePlace('hero');
            } else {
                AppActions.togglePlace(naCurrentUser);
            }
        }
    },

    renderState: function() {
        this.$('.nav-tabs_tab').removeClass('active');
        var activeTab = AppState.get('place');
        if (activeTab !== 'hoCurrentUser') {
            this.$('.act-tab-' + activeTab).addClass('active');
        }
    },

    lock: function() {
        this.locked = true;
    },

    unlock: function() {
        this.locked = false;
    },

    isLocked: function() {
        return this.locked === true;
    }


});

module.exports = Navbar;
