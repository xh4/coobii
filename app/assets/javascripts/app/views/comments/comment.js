var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var Comment = require('../../models/comment.js'),
    UserAnchor = require('../users/anchor.js'),
    commentTemplate = require('../../templates/comments/comment.hbs');

var CurrentUser = require('../../states/current-user');

var CommentView = Backbone.View.extend({

    className: 'comment',

    model: Comment,

    voted: false,

    noReply: false,

    render: function() {

        this.$el.html( commentTemplate( this.model.toJSON() ) );

        this.$el.attr({
            "data-id": this.model.id
        });

        this.userAnchor = new UserAnchor({
            model: this.model.user
        });
        this.$('.comment_h').prepend( this.userAnchor.render().el );

        var view = this;
        _.each(['up', 'down'], function(attr) {
            if (view.model.get(attr) === 0) {
                view.$(".vote-"+attr+" .count").empty();
            }
        });

        var image = this.model.get('image');
        if (image) {
            var extra;
            if (this.model.displayImage) {
                var img = new Image;
                img.src = image.url;
                extra = img;
            } else {
                extra = $('<a></a>')
                    .attr({
                        'href': image.url,
                        'target': '_blank'
                    })
                    .addClass('act-view-image')
                    .html('<i class="icon-picture"></i>查看图片');
            }
            this.$('>.comment_b .extra').show().html(extra);
        }

        var replies = this.model.replies,
            commentView = this;
        if (replies.length !== 0) {
            _.each(replies, function(reply) {
                var replyView = new CommentView({
                    model: reply
                });
                replyView.noReply = true;
                replyView.render();
                commentView.$el.find('>.replies').append(replyView.el).show();
                commentView.$el.find('>.replies').show();
            });
        }

        if (this.noReply) {
            this.$el.find('.bullet, .act-reply').remove();
        }

        return this;
    },

    events: {
        "click .vote .comment_menu_btn": "vote",
        "click .act-reply": "reply",
        "click .act-view-image": 'onClickViewImage'
    },

    vote: function(e) {
        if (CurrentUser.isGuest()) {
            return alert("登录后才能投票");
        }

        var commentView = this,
            comment = this.model;
        var btn = $(e.target).closest('.comment_menu_btn'),
            action = btn.attr('class').match(/vote-(\w+)/)[1],
            url = this.model.url();
        if (this.voted === action) return;
        var voteReverse = {
            "up": "down",
            "down": "up"
        };
        if (action === 'up') {
            url += '/up';
        } else {
            url += '/down';
        }
        if (this.voted) {
            comment.set(voteReverse[action],
                        comment.get(voteReverse[action]) - 1);
        }
        comment.set(action, comment.get(action) + 1);
        this.render();
        this.voted = action;

        $.ajax({
            url: url,
            type: 'POST'
        }).done(function() {

        }).fail(function() {

        }).always(function(data) {
            comment.set(data);
            commentView.render();
        });
    },

    onClickViewImage: function(e) {
        e.preventDefault();
        this.model.displayImage = true;
        this.render();
    },

    reply: function() {
        if (CurrentUser.isGuest()) {
            return alert("登录后才能回复");
        }

        this.trigger('reply', this.model);
    }

});

module.exports = CommentView;
