var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');
Backbone.$ = $;

var CommentView = require('./comment.js'),
    Comment = require('../../models/comment.js'),
    Comments = require('../../collections/comments.js'),
    commentAreaTemplate = require('../../templates/comments/comment-area.hbs');

var UserAnchor = require('../users/anchor');

require('jquery.fileupload');
var loadImage = require('../../vendor/load-image.js');
require('jquery.autosize');

var CurrentUser = require('../../states/current-user');

var CommentArea = Backbone.View.extend({

    className: 'comment-area',

    model: null, // content

    image: null,

    replyTo: null,

    initialize: function(options) {
        this.collection = new Comments;
    },

    render: function() {
        this.$el.html( commentAreaTemplate() );
        this.$('textarea').on('focus', function() {
            $(this).autosize({'append': ''});
        });
        this.renderAvatar();
        this.renderUploader();
        return this;
    },

    renderAvatar: function() {
        if (CurrentUser.isLoggedIn()) {
            this.$el.addClass('logged-in');
            var userAnchor = new UserAnchor({
                model: CurrentUser
            });
            this.$('.edit-comment').prepend(userAnchor.render().el);
        }
    },

    renderUploader: function() {
        var area = this;
        this.$('.image-selector').fileupload({
            dropZone: this.$(".image-selector"),
            paramName: 'image',
            url: '/api/contents/' + this.model.id + '/comments',
            add: function(e, data) {
                var file = data.files[0];
                if (["image/jpeg",
                     "image/png",
                     "image/gif"].indexOf(file.type) === -1) {
                } else {
                    area.image = file;
                    loadImage(file, function(img) {
                        area.$(".add-comment .extra").show();
                        area.$(".add-comment .extra .image").html(img).append("<i class='fa fa-minus-square act-remove-image'></i>");
                        area.$(".add-image").addClass("has-image");
                    }, {
                        maxWidth: 226,
                        crop: false,
                        canvas: false
                    });
                }
            }
        });
    },

    events: {
        'click .add-comment .content': 'expand',
        'click .create-comment': 'createComment',
        "click .act-remove-image": "removeImage",
        'blur .add-comment textarea': 'collapse',
        "mouseover .btn": "disableBlur",
        "mouseleave .btn": "enableBlur",
        "click .act-cancel-reply": "cancelReply"
    },

    getText: function() {
        return this.$('textarea').val();
    },

    expand: function() {
        if (!this.ensureLoggedIn()) return;
        this.$('.add-comment .place-holder').css('display', 'none');
        this.$('.add-comment .menu').css('display', 'block');
        this.$('.add-comment .content').addClass('active');
        this.$('.add-comment textarea').focus();
    },

    enableBlur: function() {
        this.noBlur = false;
    },

    disableBlur: function() {
        this.noBlur = true;
    },

    collapse: function() {
        if (this.getText() !== '' ||
            this.noBlur ||
            this.image) return;
        this.$('.add-comment .content').removeClass('active');
        this.$('.add-comment .place-holder').css('display', 'block');
        this.$('.add-comment .menu').css('display', 'none');
        this.$('.add-comment textarea').css('height', 'auto');
    },

    // when user click the comment button on contentView's footer
    focus: function() {
        this.expand();
        this.$('.add-comment textarea').focus();
    },

    clear: function() {
        this.$('textarea').val("");
    },

    removeImage: function() {
        this.$('.add-comment .extra .image').empty();
        this.$('.add-comment .extra').hide();
        this.image = null;
        this.$(".add-image").removeClass("has-image");
        this.noBlur = false;
        this.$('.add-comment textarea').focus();
    },

    showSpinner: function() {

    },

    hideSpinner: function() {

    },

    showOverlay: function() {

    },

    hideOverlay: function() {

    },

    ensureLoggedIn: function() {
        if (CurrentUser.isLoggedIn()) {
            return true;
        }
        alert('登录后才能发表评论');
        return false;
    },

    loadComments: function(fn) {
        var url = '/api/contents/' + this.model.id + '/comments',
            area = this;
        $.get(url).done(function(res) {
            _.each(res, function(c) {
                var comment = new Comment(c);
                area.collection.add(comment);
            });
            area.addComments();
            if (fn) fn();
        });
    },

    addComments: function() {
        var comments = this.collection,
            area = this;
        var level1 = this.collection.filter(function(comment) {
            return comment.get('parent_id') === null;
        });
        _.each(level1, function(comment) {
            area.addAsLevel1(comment);
        });
        var level2 = this.collection.filter(function(comment) {
            return comment.get('parent_id') !== null;
        });
        _.each(level2, function(comment) {
            area.addAsLevel2(comment);
        });
    },

    addAsLevel1: function(comment) {
        var commentView = new CommentView({
            model: comment
        });
        this.$('>ul.comments').append( commentView.render().el );
        this.listenTo(commentView, 'reply', this.setReply);
        commentView.listenTo(commentView.model, 'add-reply', function(comment) {
            this.render();
        });
    },

    addAsLevel2: function(comment) {
        var parentId = comment.get('parent_id'),
            parent = this.collection.findWhere({ id: parentId });
        if (!parent) return;
        parent.replies.push(comment);
        parent.trigger('add-reply', comment);
    },

    setReply: function(comment) {
        this.replyTo = comment.id;
        this.$('.reply-to-username').html( comment.user.get('display_name') );
        this.$('.reply-to').show();
        this.focus();
    },

    cancelReply: function() {
        this.replyTo = null;
        this.$('.reply-to-username').empty();
        this.$('.reply-to').hide();
    },

    createComment: function() {
        var area = this,
            data = {
                text: this.getText(),
                content_id: this.model.id
            };
        if (this.replyTo) data.parent_id = this.replyTo;

        var done = function(res) {
            this.clear();
            this.removeImage();
            this.$('textarea').blur();
            this.cancelReply();
            var comment = new Comment(res);
            this.collection.add(comment);
            if (comment.get('parent_id')) {
                this.addAsLevel2(comment);
            } else {
                this.addAsLevel1(comment);
            }
        };

        var fail = function() {

        };

        var always = function() {

        };

        if (this.image) {
            this.$('.image-selector').fileupload('option', {
                formData: data,
                done: function(e, data) {
                    done.call(area, data.result);
                },
                fail: function(e, data) {
                    fail.call(area,
                              data.jqXHR.responseJSON,
                              data.jqXHR.status,
                              data.errorThrown);
                },
                always: function() {
                    always.call(area);
                }
            });
            this.$('.image-selector').fileupload('send', {files: this.image});
        } else {
            $.ajax({
                url: '/api/contents/' + this.id + '/comments',
                type: 'POST',
                data: data
            }).done(function(res) {
                done.call(area, res);
            }).fail(function(jqXHR, textStatus, errThrown) {
                fail.call(area,
                          jqXHR.responseJSON,
                          jqXHR.status,
                          errThrown);
            }).always(function() {
                always.call(area);
            });
        }
    }

});

module.exports = CommentArea;
