var $ = require('jquery'),
    _ = require('underscore');

require('jquery.transit');

var Alert = function() {

    this.getEl = function() {
        if ($('#alert').length === 0) {
            return $('<div id="alert">').appendTo('#container');
        } else {
            return $('#alert');
        }
    };

    this.locked = false,

    this.alert = function(text, type) {
        if (this.locked) return;
        this.locked = true;

        var el = this.getEl(),
            alert = this;
        el.addClass(type).html(text);

        el.transition({
            top: "7px"
        }, function() {
            setTimeout(function() {
                el.transition({
                    top: "-32px"
                }, function() {
                    el.attr('class', '');
                    alert.locked = false;
                });
            }, 3000);
        });
    };

    this.error = function(text) {
        this.alert(text, 'error');
    };

    this.success = function(text) {
        this.alert(text, 'success');
    };

    this.warning = function(text) {
        this.alert(text, 'warning');
    };

};

module.exports = new Alert;
