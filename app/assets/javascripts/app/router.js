var $ = require('jquery'),
    Backbone = require('backbone');
Backbone.$ = $;

var Router = Backbone.Router.extend({

    initialize: function() {
    },

    routes: {
        ":namespace": "viewNamespace",
        ":namespace/": "viewNamespace",
        ":namespace/topics/:topicId": "viewTopic",
        ":namespace/:contentType/:contentId" : "viewContent"
    }

});

module.exports = Router;
