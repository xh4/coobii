var _ = require('underscore'),
    Backbone = require('backbone');

var State = Backbone.Model.extend();
var Store = Backbone.Collection.extend();

var Coobii = function() {

    window.Coobii = this;

    window.states = {};

    window.stores = {};

};

_.extend(Coobii, {

    createState: function(name, attributes) {
        window.states[name] = new State(attributes);
        return window.states[name];
    },

    getState: function(name) {
        return window.states[name];
    },

    createStore: function(name, attributes) {
        window.stores[name] = new Store(attributes);
        return window.stores[name];
    },

    getStore: function(name) {
        return window.stores[name];
    }

});



module.exports = Coobii;
