var Backbone = require('backbone');

var Contents = require('./contents.js'),
    Post = require('../models/post.js');

var Posts = Contents.extend({

    model: Post,

    initialize: function(options) {
    }

});

module.exports = Posts;
