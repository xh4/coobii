var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone');

var Comment = require('../models/comment.js');

var Comments = Backbone.Collection.extend({

    model: Comment

});

module.exports = Comments;
