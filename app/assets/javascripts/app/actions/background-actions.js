var AppDispatcher = require('../dispatcher/app-dispatcher');

var BackgroundConstants = require('../constants/background-constants');

var BackgroundActions = {

    changeBackground: function(attrs) {
        AppDispatcher.handleViewAction({
            actionType: BackgroundConstants.CHANGE_BACKGROUND,
            attrs: attrs
        });
    }

};

module.exports = BackgroundActions;
