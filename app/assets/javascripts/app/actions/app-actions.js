var AppDispatcher = require('../dispatcher/app-dispatcher');

var AppConstants = require('../constants/app-constants');

var AppActions = {

    togglePlaceLock: false,

    togglePlace: function(name, options) {
        if (this.togglePlaceLock) return;
        AppDispatcher.handleViewAction({
            actionType: AppConstants.TOGGLE_PLACE,
            place: name,
            options: options
        });
    }

};

module.exports = AppActions;
