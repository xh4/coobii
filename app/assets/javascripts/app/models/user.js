var Backbone = require('backbone');

var User = Backbone.Model.extend({

    urlRoot: "/api/users",

    avatarTimestamp: null,

    backgroundTimestamp: null,

    getAvatarUrl: function() {
        if (!this.get('avatar')) {
            return null;
        } else {
            var url = this.get('avatar').url;
            if (this.avatarTimestamp) {
                url = url + "?_=" + this.avatarTimestamp;
            }
            return url;
        }
    },

    getOrigAvatarUrl: function() {
        var url = this.get('avatar').original_url;
        if (this.avatarTimestamp) {
            url = url + "?_=" + this.avatarTimestamp;
        }
        return url;
    },

    getBackgroundUrl: function() {
        var url = this.get('background').url;
        if (this.backgroundTimestamp) {
            url = url + "?_=" + this.backgroundTimestamp;
        }
        return url;
    },

    getTitle: function() {
        return this.get('display_name') + " (" + this.get('username') + ")";
    },

    updateAvatarTimestamp: function() {
        this.avatarTimestamp = new Date().getTime();
    },

    updateBackgroundTimestamp: function() {
        this.backgroundTimestamp = new Date().getTime();
    }

});

module.exports = User;
