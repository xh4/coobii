var Backbone = require('backbone');

var Me = require('../me.js'),
    User = require('./user.js');

var Content = Backbone.Model.extend({

    idAttribute: "cid",

    urlRoot: "/api/contents",

    initialize: function(options) {
        var u = this.get('user');
        if (u &&
            !Me.isGuest() &&
            u.id === Me.get('id')) {
            Me.set(u);
            this.user = Me;
        } else {
            this.user = new User(u);
        }
    }

});

module.exports = Content;
