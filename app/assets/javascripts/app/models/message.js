var Content = require('./content.js');

var Message = Content.extend({

    initialize: function(options) {
        Content.prototype.initialize.call(this, options);
    },

    getTitleText: function() {
        var text = this.get('text');
        if (text.length <=30) {
            return text;
        } else {
            return text.slice(0, 30) + " ...";
        }
    }

});

module.exports = Message;
