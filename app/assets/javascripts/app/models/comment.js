var Backbone = require('backbone');

var User = require('./user.js');

var Comment = Backbone.Model.extend({

    urlRoot: "/api/comments",

    initialize: function() {
        this.user = new User(this.get('user'));
        this.replies = [];
    },

    displayImage: false

});

module.exports = Comment;
