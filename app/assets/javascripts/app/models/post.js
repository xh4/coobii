var Backbone = require('backbone');

var Content = require('./content.js'),
    User = require('./user.js'),
    Me = require('../me.js');

var Post = Content.extend({

    initialize: function(options) {
        Content.prototype.initialize.call(this, options);
    }

});

module.exports = Post;
