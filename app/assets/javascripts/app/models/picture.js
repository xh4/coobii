var Content = require('./content.js');

var Picture = Content.extend({

    initialize: function(options) {
        Content.prototype.initialize.call(this, options);
    }

});

module.exports = Picture;
