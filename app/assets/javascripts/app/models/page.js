var _ = require('underscore'),
    Backbone = require('backbone');


var Page = Backbone.Model.extend({

    urlRoot: "/api/pages",

    getDisplayNS: function() {
        var s = _.find(this.get('aliases'), function(a) {
            return a.type === 'short';
        });
        return s? s.name : this.get('name');
    }

});

module.exports = Page;
