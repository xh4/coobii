var $ = require('jquery'),
    Backbone = require('backbone');

var User = require('./user.js'),
    Content = require('./content.js'),
    Me = require('../me.js');

var Topic = Backbone.Model.extend({

    urlRoot: "/api/topics",

    initialize: function(options) {
        Content.prototype.initialize.call(this, options);
    },

    increaseViewsCount: function() {
        $.ajax({
            type: 'POST',
            url: '/api/topics/' + this.id + '/views'
        })
        .done(function() {
            var viewsCount = this.get('views_count');
            this.set('views_count', viewsCount++);
        }.bind(this));
    }

});

module.exports = Topic;
