module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compass: {
            sass: {
                options: {
                    sassDir: "stylesheets/sass",
                    cssDir: "stylesheets/compiled"
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'stylesheets/main.css': ['stylesheets/compiled/**/*.css', 'stylesheets/vendor/**/*.css']
                }
            }
        },
        clean: {
            css: ["stylesheets/compiled"]
        },
        watch: {
            css: {
                files: ["stylesheets/sass/**/*.scss", "stylesheets/vendor/**/*.css"],
                tasks: ["compass", "cssmin", "clean"]
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['compass', 'cssmin', 'clean', 'watch']);
};
