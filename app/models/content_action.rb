class ContentAction < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  belongs_to :user
  belongs_to :content

end
