class Namespace < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  validates :name_lower, presence: true, uniqueness: { case_sensitive: false }

end
