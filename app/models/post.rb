class Post < ActiveRecord::Base

  belongs_to :topic
  belongs_to :user

  has_many :post_images

  has_one :content, :foreign_key => :detail_id

  validates :user_id, presence: true
  validates :topic_id, presence: true
  validate :validate_blocks, unless: :draft

  after_save :clean_unused_images, unless: :draft
  after_destroy :delete_images
  after_destroy :decrease_topic_posts_count, unless: :draft

  def validate_blocks
    if !self.blocks.is_a? Array
      return self.errors.add(:blocks, "Invalid blocks")
    end

    self.blocks = self.blocks.inject([]) do |blocks, block|
      if !block.is_a?(Hash) ||
	  !block["type"] ||
	  !valid_block_types.include?(block["type"])
      else
	block = self.send("sanitize_#{block["type"]}_block", block)
	if block
	  blocks << block
	end
      end
    end

    if self.blocks.blank?
      self.errors.add(:blocks, "Empty blocks")
    end
  end

  private

  def valid_block_types
    %w(text figure)
  end

  def sanitize_text_block(block)
    block = block.select { |k, v| %w(type content).include? k }
    content = block["content"]
    if !content || content.blank?
      return nil
    end
    block["content"] = content.strip
    block
  end

  def sanitize_figure_block(block)
    block = block.select { |k, v| %w(type id).include? k }
    image = self.post_images.find_by_id(block["id"])
    unless image
      return nil
    end
    block
  end

  def clean_unused_images
    ids = self.blocks.inject([]) do |ids, block|
      if block["type"] == "figure"
	ids << block["id"]
      end
      ids
    end
    self.post_images.where.not(id: ids).each do |image|
      image.destroy
    end
  end

  def delete_images
    self.post_images.each do |i|
      i.destroy
    end
  end

  private

  def decrease_topic_posts_count
    self.topic.decrement(:posts_count).save
  end

end
