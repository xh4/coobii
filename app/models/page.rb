class Page < ActiveRecord::Base

  include BackgroundHelper

  has_many :topics

  mount_uploader :background, BackgroundUploader

  def aliases
    Namespace.where(model_id: self.id,
                    type: ["page_alias:short", "page_alias:zh_cn"])
      .map do |a|
      { type: a.type.split(':')[1],
        name: a.name,
        name_lower: a.name_lower}
      end
  end

  def display_ns
    s = self.aliases.select { |a| a[:type]=="short" } .first
    s[:name] || self.name
  end

end
