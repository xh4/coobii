class TopicEvent < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  belongs_to :topic
  belongs_to :user


end
