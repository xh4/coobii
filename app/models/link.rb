class Link < ActiveRecord::Base

  validates :title, presence: true, length: {:maximum => 256}
  validates :url, presence: true

  before_create :trim_text

  def trim_text
    self.title = self.title.strip
    self.caption = self.caption.strip
  end

  def image
    return nil unless self.image_id
    Image.find_by_id(self.image_id)
  end

end
