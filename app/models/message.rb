class Message < ActiveRecord::Base

  include Twitter::Autolink
  include ERB::Util # h

  mount_uploader :image, MessageImageUploader

  belongs_to :user
  has_one :content, :foreign_key => :detail_id

  validates :text, presence: true
  validates :user_id, presence: true
  validate :check_length

  def check_length
    urls = Twitter::Extractor.extract_urls self.text
    length = urls.length * 22
    length += self.text.length - urls.join.length
    if length > 140
      errors.add :text, I18n.t('activerecord.errors.models.message.attributes.text.too_long')
    end
    length
  end

  def fancy_time
    I18n.l content.created_at, format: :message_long
  end

  def cooked
    escaped = h self.text
    linked = auto_link escaped
    doc = Nokogiri::HTML.fragment linked
    doc.xpath('a').each do |link|
      link["target"] = '_blank'
      if link["href"] and link["href"] == link.text
        text = link.text
        text = text.first text.length - 1 if text.last == '/'
        path = text.split('/').last
        path = "#{path[0..13]}..." if path.length > 14
        index = text.rindex '/'
        text = "#{text[0..index-1]}/#{path}"
        # remove protocol prefix
        text = text.gsub /http(s)?:\/\//, ''
        link.children = text
      end
    end
    linked = doc.to_s
    linked = linked.gsub(/>(\w+)</, '>@\1<')
    linked = linked.gsub(/@</, '<')
  end

  def title_text
    if self.text.length <= 30
      self.text
    else
      self.text[0..29] + " ..."
    end
  end

  private

  def delete_image
    self.image.destroy
  end

end
