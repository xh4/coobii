class Article < ActiveRecord::Base
  belongs_to :user

  has_one :post, :foreign_key => :detail_id
end
