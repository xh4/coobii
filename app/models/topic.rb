class Topic < ActiveRecord::Base

  belongs_to :page
  belongs_to :user

  has_many :posts

  has_many :topic_events

  validates :title, presence: true, unless: :pending?
  validates :user_id, presence: true
  validates :page_id, presence: true
  validates :state, inclusion: { in: %w(pending open close) }

  before_destroy :delete_posts

  auto_strip_attributes :title

  def primer
    self.posts.find_by_primer(true)
  end

  def thumbnails
    self.published_posts.inject([]) do |images, post|
      post.blocks.each do |block|
	if block["type"] == "figure"
	  image = PostImage.find_by_id(block["id"])
	  if image
	    attrs = image.image_attributes
	    if attrs["versions"] && attrs["versions"]["thumb"]
	      images << image
	    end
	  end
	end
      end
      images
    end
  end

  def post_contents
    Content.joins("LEFT JOIN posts ON contents.detail_id = posts.id")
      .where('posts.topic_id' => self.id)
  end

  def published_posts
    self.posts.where(draft: false)
  end

  def pending?
    self.state == 'pending'
  end

  def open
    self.state = 'open'
    self.opened_at = Time.now
    self.save
  end

  def close
    self.state = 'close'
    self.save!
  end

  private

  def delete_posts
    self.post_contents.each do |c|
      c.destroy
    end
  end

end
