class Notification < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  belongs_to :user

  validates :type, :presence => true
  validates :detail, :presence => true
  validates :user_id, :presence => true
  validates :read, :presence => true
end
