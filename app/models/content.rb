class Content < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  belongs_to :user
  belongs_to :category
  has_many :comments

  belongs_to :message, :dependent => :destroy, :foreign_key => :detail_id
  belongs_to :picture, :dependent => :destroy, :foreign_key => :detail_id
  belongs_to :article, :dependent => :destroy, :foreign_key => :detail_id
  belongs_to :link,    :dependent => :destroy, :foreign_key => :detail_id
  belongs_to :post, :dependent => :destroy, :foreign_key => :detail_id

  has_many :content_actions

  validate :validate_detail

  after_destroy :delete_detail

  after_create :put_on_user_contents

  after_destroy :delete_from_user_contents
  after_destroy :delete_from_timeline_contents

  def detail
    self.send(self.type)
  end

  def liked_by?(user)
    ContentAction.exists?(type: "like", user_id: user.id, content_id: self.id)
  end

  def marked_by?(user)
    ContentAction.exists?(type: "mark", user_id: user.id, content_id: self.id)
  end

  def shared_by?(user)
    ContentAction.exists?(type: "share", user_id: user.id, content_id: self.id)
  end

  def put_on_user_contents
    UserContent.create(user_id: self.user_id, content_id: self.id, content_user_id: self.user_id)
  end

  def delete_detail
    self.detail.destroy if self.detail
  end

  def delete_from_user_contents
    UserContent.destroy_all("content_id = #{ self.id }")
  end

  def delete_from_timeline_contents
    TimelineContent.destroy_all("content_id = #{ self.id }")
  end

  private

  def validate_detail
    if !self.detail
      self.errors.add(:detail, "No detail")
    end
  end

end
