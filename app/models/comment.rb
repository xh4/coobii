class Comment < ActiveRecord::Base

  belongs_to :content
  belongs_to :user
  has_many :comment_votes

  mount_uploader :image, CommentImageUploader

  validates :text, presence: true
  validates :user_id, presence: true
  validates :content_id, presence: true

  validate :check_parent_existence
  validate :check_comment_level

  def parent
    return nil unless self.parent_id
    Comment.find_by_id(self.parent_id)
  end

  private

  def check_parent_existence
    if self.parent_id && !self.parent
      errors.add(:parent_id, "Parent not exists");
    end
  end

  def check_comment_level
    if self.parent && self.parent.parent
      errors.add(:parent_id, "Comment level too deep");
    end
  end

end
