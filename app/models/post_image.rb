class PostImage < ActiveRecord::Base

  belongs_to :post
  belongs_to :user

  mount_uploader :image, PostImageUploader

  validates :post_id, :presence => true
  validates :user_id, :presence => true
  validates :image, :presence => true
  validates :image_attributes, :presence => true

end
