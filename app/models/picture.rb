class Picture < ActiveRecord::Base
  belongs_to :user
  has_one :content, :foreign_key => :detail_id

  def image
    Image.find_by_id image_id
  end

end
