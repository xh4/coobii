# coding: utf-8
require 'valid_email'

class User < ActiveRecord::Base
  authenticates_with_sorcery!

  include UsersHelper
  include AvatarHelper
  include BackgroundHelper

  mount_uploader :avatar, AvatarUploader
  mount_uploader :background, BackgroundUploader

  has_many :user_contents
  has_many :timeline_contents

  has_many :contents
  has_many :categories

  has_many :messages
  has_many :posts
  has_many :pictures
  has_many :articles
  has_many :links

  has_many :images

  has_many :likes
  has_many :marks
  has_many :shares

  has_many :comments
  has_many :comment_votes

  has_many :notifications

  # || user_id | follower_id ||
  has_many :relations, foreign_key: 'user_id', dependent: :destroy
  has_many :reverse_relations, foreign_key: 'follower_id', dependent: :destroy,
           class_name: 'Relation'
  has_many :followers, through: :relations, source: :follower
  has_many :friends, through: :reverse_relations, source: :user

  validates :username, presence: true ,
  length: {maximum: 15, minimum: 3},
  format: {with: /\A[a-zA-Z0-9_]{1,15}\z/},
  reduce: true
  validates :display_name, presence: true, length: {maximum: 20}, reduce: true
  validates :email, presence: true, uniqueness: {case_sensitive: false}, email: true, reduce: true
  validates :password, presence: true, length: {minimum: 6}, reduce: true, on: :create

  validates :headline, length: {maximum: 100}
  validates :bio, length: {maximum: 160}
  validates :location, length: {maximum: 30}
  validates :website, length: {maximum: 100}

  validates_presence_of :avatar

  before_validation :strip_whitespace
  before_save :strip_whitespace

  before_validation :set_username_lower
  before_create :set_username_lower

  validate :check_namespace_existence, on: :create

  after_create :register_namespace
  after_destroy :destroy_namespace

  after_destroy :destroy_contents

  def followed_by?(user)
    relations.exists?(:follower_id => user.id)
  end

  def following?(user)
    reverse_relations.exists?(:user_id => user.id)
  end

  def counts
    {
      :likes => self.likes_count,
      :marks => self.marks_count,
      :shares => self.shares_count,

      :followers => self.followers_count,
      :friends => self.friends_count,

      :contents => self.contents_count,

      :messages => self.contents.where(:type => :message).count,
      :articles => self.contents.where(:type => :article).count,
      :pictures => self.contents.where(:type => :picture).count,
      :links => self.contents.where(:type => :link).count,
      :quotes => self.contents.where(:type => :quote).count
    }
  end

  private

  def strip_whitespace
    attrs = [:username, :display_name, :headline, :bio, :location, :website]
    attrs.each do |attr|
      self[attr] = self[attr].strip unless self[attr].nil?
    end
  end

  def set_username_lower
    self.username_lower = self.username.downcase unless self.username.nil?
  end

  def check_namespace_existence
    if Namespace.find_by_name_lower(self.username_lower)
      self.errors.add(:username, self.errors.generate_message(:username, :taken))
    end
  end

  def register_namespace
    Namespace.where(:name => self.username,
                    :name_lower => self.username.downcase,
                    :type => 'user',
                    :model_id => self.id,
                    ).first_or_create
  end

  def destroy_namespace
    Namespace.find_by_model_id(self.id).destroy
  end

  def destroy_contents
    true
  end

end
