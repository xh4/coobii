class CommentVote < ActiveRecord::Base
  self.inheritance_column = :_type_disabled # disable STI

  belongs_to :user
  belongs_to :comment

  after_create :increase_vote_count
  after_destroy :decrease_vote_count

  private

  def increase_vote_count
    self.comment.send(self.type+"=", self.comment.send(self.type) + 1)
    self.comment.save
  end

  def decrease_vote_count
    self.comment.send(self.type+"=", self.comment.send(self.type) - 1)
    self.comment.save
  end

end
