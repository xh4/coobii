class UnfollowAndDeleteContentsWorker
  @queue = :unfollow_and_delete_contents_queue

  def self.perform(user_id, friend_id)
    # user_id, content_id, content_user_id, sender_id

    user = User.find_by_id user_id
    friend = User.find_by_id friend_id

    user.timeline_contents
      .where(content_user_id: friend.id)
      .each do |c|

      c.destroy
    end

  end
end
