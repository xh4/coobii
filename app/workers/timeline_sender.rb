# -*- coding: utf-8 -*-
class TimelineSender
  @queue = :timelines_queue

  def self.perform(content_id, sender_id)
    # user_id, content_id, content_user_id, sender_id
    # 将content推送到sender的所有follower的timeline

    sender = User.find_by_id sender_id
    content = Content.find_by_id content_id
    # put on sender's timeline
    TimelineContent.create(user_id: sender.id,
                        content_id: content.id,
                        content_user_id: content.user_id,
                        sender_id: sender.id
                        )

    # put on followers' timeline
    sender.followers.each do |f|
      TimelineContent.create(user_id: f.id,
                          content_id: content.id,
                          content_user_id: content.user_id,
                          sender_id: sender.id
                          )
    end
  end
end
