class FollowAndPullContentsWorker
  @queue = :follow_and_pull_contents_queue

  def self.perform(user_id, friend_id)
    # user_id, content_id, content_user_id, sender_id

    user = User.find_by_id user_id
    friend = User.find_by_id friend_id

    friend.user_contents
      .order("created_at DESC")
      .limit(30)
      .each do |c|
      user.timeline_contents.where(
                                user_id: user.id,
                                content_id: c.id,
                                content_user_id: c.user_id,
                                sender_id: friend.id
                                ).first_or_create
    end
  end
end
