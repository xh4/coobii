object @user => nil

attributes :id,
:username, :username_lower, :display_name,
:headline, :bio, :location, :website,
:sponsor, :moderator,
:language,
:counts,
:last_login

node(:avatar, :if => lambda {|user| user.avatar_attributes}) do |user|
  user.avatar_attributes.merge({"url" => user.avatar.normal.url,
                                 "original_url" => user.avatar.url})
end

node(:background, :if => lambda {|user| user.background_attributes}) do |user|
  user.background_attributes.merge({"url" => user.background.url})
end

node :is_friend do |user|
  if !user_signed_in? || current_user.id == user.id || current_user.following?(user) == false
    false
  else
    true
  end
end

node :is_following_me do |user|
  if !user_signed_in? || current_user.id == user.id || current_user.followed_by?(user) == false
    false
  else
    true
  end
end
