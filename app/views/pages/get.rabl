object @page => nil

attributes :id, :name, :name_lower, :display_name, :color

node(:background, :if => lambda {|page| page.background_attributes}) do |page|
  page.background_attributes.merge({"url" => page.background.url})
end

node :aliases do |p|
  p.aliases
end

node :display_namespace do |p|
  p.display_ns
end
