object @post_image => nil

attributes :id, :post_id, :user_id

node :url do |i|
  i.image.url
end

node do |i|
  attrs = i.image_attributes
  if attrs["versions"]
    attrs["versions"].each_pair do |k, v|
      attrs["versions"][k]["url"] = i.image.send(k).url
    end
  end
  attrs
end
