object @post => nil

attributes :id, :draft, :primer

unless locals[:hide_topic]
  node :topic do |post|
    partial('topics/get', :object => post.topic)
  end
end

node :user do |post|
  partial('users/get', :object => post.user)
end

node :blocks do |post|
  if post.draft
    nil
  else
    post.blocks.inject([]) do |blocks, block|
      if block["type"] == 'figure'
	i = post.post_images.find_by_id(block["id"])
	image = i.image
	attrs = i.image_attributes

	block["url"] = image.url
	block["width"] = attrs["width"]
	block["height"] = attrs["height"]
      end
      blocks << block
    end
  end
end
