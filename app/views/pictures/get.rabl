object @picture

attributes :title, :description, :exif, :licence,
            :album_id

# image
node(:image, :if => lambda {|picture| picture.image_id}) do |picture|
  partial('contents/image', :object => picture.image)
end