object @message # extends content

attributes :text, :cooked

# image
node(:image, :if => lambda {|message| message.image.url}) do |message|
  message.image_attributes.merge({:url => message.image.url})
end

# location
node(:location, :if => lambda {|message| message.location}) do |message|
  'location'
end

node :fancy_time do |message|
  message.fancy_time
end

node :time_ago do |message|
  TimeHelper.time_ago(message.content.created_at)
end
