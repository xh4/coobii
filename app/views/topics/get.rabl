object @topic => nil

attributes :id, :title, :state,
:views_count, :posts_count,
:updated_at, :created_at


node :page do |topic|
  partial('pages/get', :object => topic.page)
end

node :likes_count, :if => lambda { |t| t.primer } do |topic|
  topic.primer.content.likes_count
end

node :primer, :if => lambda { |t| t.primer } do |topic|
  partial('contents/get', :object => topic.primer.content,
          :locals => {
            detail_locals: { hide_topic: true }
          })
end

node :user do |topic|
  partial('users/get', :object => topic.user)
end

node :thumbnails do |topic|
  topic.thumbnails.inject([]) do |thumbs, image|
    thumbs << partial('posts/get_image', :object => image)
  end
end

node :events do |topic|
  topic.topic_events.inject([]) do |events, e|
    events << {
      id: e.id,
      type: e.type,
      user: partial('users/get', object: e.user)
    }
  end
end
