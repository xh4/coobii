object false

node :topics do
  @topics.map { |t| partial('topics/get', :object => t) }
end

node :pagination do
  paginate @topics
end
