object false

node :contents do
  @contents.map { |c| partial('contents/get', :object => c) }
end

node :pagination do |c|
  paginate(@contents)
end
