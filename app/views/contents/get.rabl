object @content => nil

attributes :type,
  :likes_count, :marks_count, :shares_count, :comments_count,
  :state, :comment_state,
  :created_at, :updated_at

node(:id) { |c| c.detail.id }

node(:cid) { |c| c.id }

node false do |content|
  partial("#{content.type}s/get",
          :object => content.detail,
          :locals => locals[:detail_locals])
end

node :user do |content|
  partial('users/get', :object => content.user)
end

node :liked do |content|
  if user_signed_in? && content.liked_by?(current_user) then true else false end
end

node :marked do |content|
  if user_signed_in? && content.marked_by?(current_user) then true else false end
end

node :shared do |content|
  if user_signed_in? && content.shared_by?(current_user) then true else false end
end

node :fancy_date do |content|
  content.created_at.strftime("%B %e, %Y")
end
