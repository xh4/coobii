object @comment => nil

attributes :id, :content_id, :text,
:up, :down, :parent_id, :created_at

# image
node(:image, :if => lambda {|comment| comment.image.url}) do |comment|
  comment.image_attributes.merge({:url => comment.image.url})
end

# user
node :user do |comment|
  partial('users/get', :object => comment.user)
end

node :time_ago do |comment|
  TimeHelper.time_ago(comment.created_at)
end
