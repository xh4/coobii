Coobii::Application.routes.draw do

  root 'welcome#index'

  scope 'api' do
    get    'welcome/creators'       => 'welcome#list_creators'
  end

  # users
  scope 'api' do
    get    'users' => 'users#whoami'
    put    'users' => 'users#update'
    put    'users/password' => 'users#reset_password'
    put    'users/background' => 'users#update_bg'
  end

  # users
  scope 'api' do
    get    'users/:id'              => 'users#get'
  end

  # blocks
  scope 'api' do

  end

  # suggestions
  scope 'api' do
    get    'suggestions/radar'      => 'suggestions#get_radar'
    get    'suggestions/users'      => 'suggestions#list_users'
    get    'suggestions/posts'      => 'suggestions#list_posts'
  end

  # contents
  scope 'api' do
    get    'contents'       => 'contents#list_user'
    get    'contents/:content_id'   => 'contents#get'
    delete 'contents/:content_id'   => 'contents#delete'

    [:like, :mark, :share].each do |action|
      action_resource = action.to_s.pluralize
      post   "contents/:content_id/#{action_resource}", to: "content_actions##{action}"
      delete "contents/:content_id/#{action_resource}", to: "content_actions#un#{action}"
    end

    post   'messages'    => 'messages#create'
    post   'pictures'    => 'pictures#create'
  end

  # timeline
  scope 'api' do
    get    'timeline'    => 'contents#list_timeline'
  end

  # categories
  scope 'api' do
    get    'categories/:id'        => 'categories#get'
    get    'categories'            => 'categories#list'
    post   'categories'            => 'categories#create'
    delete 'categories/:id'        => 'categories#delete'
  end

  # marks
  scope 'api' do
    get    'marks'          => 'contents#list_marks'
  end

  # likes
  scope 'api' do
    get    'likes'          => 'contents#list_likes'
  end

  # shares
  scope 'api' do
    get    'shares'          => 'shares#list'
  end

  # comments
  scope 'api' do
    get    'contents/:id/comments'   => 'comments#list'
    post   'contents/:id/comments'   => 'comments#create'
    delete 'comments/:id'         => 'comments#delete'
    post   'comments/:id/up'      => 'comments#up'
    post   'comments/:id/down'    => 'comments#down'
  end

  # relations
  scope 'api' do
    get    'followers'        => 'relations#list_followers'
    get    'friends'          => 'relations#list_friends'
    post   'friends'          => 'relations#create'
    delete 'friends'          => 'relations#delete'
  end

  # topics
  scope 'api' do
    get    'topics/:topic_id' => 'topics#get'
    get    'topics' => 'topics#list'
    put    'topics/:topic_id' => 'topics#update'
    post   'topics' => 'topics#create_or_get_pending'
    post   'topics/:topic_id/close' => 'topics#close'
    post   'topics/:id/views' => 'topics#increase_views_count'
  end

  # posts
  scope 'api' do
    get    'posts' => 'posts#list'
    post   'posts' => 'posts#create_or_get_draft'
    put    'posts/:id' => 'posts#update'
    post   'posts/:id/publish' => 'posts#publish'
    get    'posts/:id/images' => 'posts#list_images'
    post   'posts/:id/images' => 'posts#new_image'
  end

  # pages
  scope 'api' do
    get    'pages/:id' => 'pages#get'
    put    'pages/:id/background' => 'pages#change_bg'
  end

  # status
  scope 'api' do
    get    'status/counts'    => 'status#counts'
  end

  get    'login' => 'users/sessions#new'

  post   'sessions' => 'users/sessions#create'
  delete 'sessions' => 'users/sessions#destroy'

  get    'register' => 'users/registrations#new'
  post   'register' => 'users/registrations#show'

  post   'registrations' => 'users/registrations#create'

  # front end test
  get    'test'     => 'tests#show'

  get    '403'      => 'status#forbid'

  get    ':namespace' => 'namespaces#show'
  get    ':namespace/topics/:topic_id' => 'topics#show'
  get    ':namespace/:content_type/:detail_id' => 'contents#show'

end
