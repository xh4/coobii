# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140830085116) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ahoy_events", id: false, force: true do |t|
    t.uuid     "id",         null: false
    t.uuid     "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.json     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["time"], name: "index_ahoy_events_on_time", using: :btree
  add_index "ahoy_events", ["user_id"], name: "index_ahoy_events_on_user_id", using: :btree
  add_index "ahoy_events", ["visit_id"], name: "index_ahoy_events_on_visit_id", using: :btree

  create_table "comment_votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "comment_id"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comment_votes", ["comment_id", "user_id"], name: "index_comment_votes_on_comment_id_and_user_id", unique: true, using: :btree

  create_table "comments", force: true do |t|
    t.integer  "content_id",                   null: false
    t.text     "text",                         null: false
    t.integer  "up",               default: 0, null: false
    t.integer  "down",             default: 0, null: false
    t.integer  "user_id",                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.inet     "ip"
    t.integer  "parent_id"
    t.string   "image"
    t.json     "image_attributes"
  end

  create_table "content_actions", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "content_id", null: false
    t.string   "type",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contents", force: true do |t|
    t.string   "type",                             null: false
    t.integer  "user_id",                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "likes_count",    default: 0,       null: false
    t.integer  "marks_count",    default: 0,       null: false
    t.integer  "shares_count",   default: 0,       null: false
    t.integer  "detail_id",                        null: false
    t.inet     "ip"
    t.string   "comment_state",  default: "close", null: false
    t.integer  "comments_count", default: 0,       null: false
    t.string   "state",                            null: false
  end

  create_table "invite_codes", force: true do |t|
    t.string   "code",       null: false
    t.integer  "belongs_to"
    t.integer  "used_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.text    "text",             null: false
    t.json    "location"
    t.integer "user_id",          null: false
    t.string  "image"
    t.json    "image_attributes"
  end

  create_table "namespaces", force: true do |t|
    t.string   "name_lower", null: false
    t.string   "type",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "model_id",   null: false
    t.string   "name",       null: false
  end

  create_table "notifications", force: true do |t|
    t.string   "type",                       null: false
    t.json     "detail",                     null: false
    t.integer  "user_id",                    null: false
    t.boolean  "read",       default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "name",                  null: false
    t.string   "name_lower",            null: false
    t.string   "display_name"
    t.integer  "creator_id"
    t.string   "color"
    t.string   "avatar"
    t.json     "avatar_attributes"
    t.string   "background"
    t.json     "background_attributes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_images", force: true do |t|
    t.integer  "post_id",          null: false
    t.integer  "user_id",          null: false
    t.string   "image",            null: false
    t.json     "image_attributes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.integer  "topic_id",                   null: false
    t.integer  "user_id",                    null: false
    t.boolean  "draft",      default: true,  null: false
    t.json     "blocks"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "primer",     default: false, null: false
  end

  create_table "relations", force: true do |t|
    t.integer  "user_id",     null: false
    t.integer  "follower_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relations", ["follower_id"], name: "index_relations_on_follower_id", using: :btree
  add_index "relations", ["user_id", "follower_id"], name: "index_relations_on_user_id_and_follower_id", unique: true, using: :btree
  add_index "relations", ["user_id"], name: "index_relations_on_user_id", using: :btree

  create_table "timeline_contents", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "content_id",      null: false
    t.integer  "sender_id",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "content_user_id", null: false
  end

  add_index "timeline_contents", ["user_id", "content_id", "sender_id"], name: "index_timeline_contents_on_user_id_and_content_id_and_sender_id", unique: true, using: :btree

  create_table "topic_events", force: true do |t|
    t.integer  "topic_id",   null: false
    t.integer  "user_id",    null: false
    t.string   "type",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topics", force: true do |t|
    t.integer  "page_id",                 null: false
    t.string   "title"
    t.string   "state",                   null: false
    t.integer  "views_count", default: 0, null: false
    t.integer  "posts_count", default: 0, null: false
    t.integer  "user_id",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "opened_at"
  end

  create_table "user_contents", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "content_id",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "content_user_id", null: false
  end

  add_index "user_contents", ["user_id", "content_id"], name: "index_user_contents_on_user_id_and_content_id", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                                      default: "",      null: false
    t.string   "username",                        limit: 15,                   null: false
    t.string   "username_lower",                                               null: false
    t.string   "display_name",                    limit: 20,                   null: false
    t.string   "headline"
    t.string   "bio"
    t.string   "location"
    t.string   "website"
    t.string   "language",                                   default: "zh-CN", null: false
    t.boolean  "moderator",                                  default: false,   null: false
    t.boolean  "sponsor",                                    default: false,   null: false
    t.integer  "likes_count",                                default: 0,       null: false
    t.integer  "friends_count",                              default: 0,       null: false
    t.integer  "followers_count",                            default: 0,       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
    t.json     "avatar_attributes"
    t.string   "background"
    t.json     "background_attributes"
    t.integer  "marks_count",                                default: 0,       null: false
    t.integer  "shares_count",                               default: 0,       null: false
    t.integer  "contents_count",                             default: 0,       null: false
    t.string   "crypted_password",                                             null: false
    t.string   "salt",                                                         null: false
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_me_token"], name: "index_users_on_remember_me_token", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", using: :btree
  add_index "users", ["username_lower"], name: "index_users_on_username_lower", unique: true, using: :btree

  create_table "visits", id: false, force: true do |t|
    t.uuid     "id",               null: false
    t.uuid     "visitor_id"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree

end
