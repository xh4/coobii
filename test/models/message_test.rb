require 'test_helper'

class MessageTest < ActiveSupport::TestCase

  test "text should not be null" do
    m = Message.new
    m.valid?
    assert m.errors.has_key?(:text)
    m.text = ""
    m.valid?
    assert m.errors.has_key?(:text)
    m.valid?
    m.text = "    "
    assert m.errors.has_key?(:text)
  end


  test "should trim text before save" do
    m = Message.create(text: "   message text    ", user_id: 1)
    m.save
    assert_equal m.text, "message text"
  end

end
