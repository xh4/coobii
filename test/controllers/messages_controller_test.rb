require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  test "create" do
    post :create, { :text => "a message" }, { :current_user_id => 1 }
    assert_response :success
  end

  test "create with image" do
    image_path = Rails.root.join 'test/fixtures/files/image.jpg'
    file = Rack::Test::UploadedFile.new image_path, 'image/jpeg'
    post :create, { :image => file, :text => "test message with image" }, { :current_user_id => 1 }
    assert_response :success
  end

end
