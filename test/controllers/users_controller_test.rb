require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'upload avatar' do
    test_avatar = Rails.root.join 'test/fixtures/files/avatar.jpg'
    file = Rack::Test::UploadedFile.new test_avatar, 'image/jpeg'
    put :update, :id => 1, :avatar => file
    assert_response :success
  end
end
