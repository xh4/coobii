require '../app/models/invite_code'

100.times do
  code = [*('A'..'Z')].sample(7).join
  if InviteCode.find_by_code(code).nil?
    InviteCode.create(code: code)
    puts "create a code: #{ code }"
  end
end
