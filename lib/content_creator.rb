class ContentCreator

  def initialize(model)
    @model = model
  end

  def create_content
    @content = Content.new do |c|
      c.type = @model.class.name.downcase
      c.detail_id = @model.id
      c.user_id = @model.user_id
      c.comment_state = 'allow:all'
    end

    if @content.type == 'post'
      @content.state = 'pending'
    else
      @content.state = 'normal'
    end

    @content.save
    @content
  end

end
