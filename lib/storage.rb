require 'fog'

module Storage

  CON = Fog::Storage.new(
                         provider:               "AWS",
                         aws_access_key_id:      "8W4I41YWM3G3ZPD3OU4E",
                         aws_secret_access_key:  "1Ey_iQvkY8J-qL6kp--_AEyzzhhcCqD-OmecAQ==",
                         host:                   "storage.coobii.com",
                         scheme:                 "http",
                         connection_options: {
                                              proxy: "http://storage.coobii.com",
                                              port: 80
                                             }
                        )
  class Bucket
    class << self
      def url(name)
        "http://#{name}.storage.coobii.com"
      end
    end
    
    def initialize(name)
      @dir = CON.directories.find{|d| d.key == name}
    end

    def files
      @dir.files
    end
  end
end
