require './oss'

paths = Dir.glob("/Users/kevin/Desktop/patterns/*.png")

paths.each do |p|
  filename = p.split('/').last
  OSS::Object.store "patterns/#{filename}", File.open(p), 'coobii-backgrounds'
  Pattern.create(filename: filename)
  puts "stored a pattern: #{filename}"
end