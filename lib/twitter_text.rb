module Twitter
  # A module for including Tweet auto-linking in a class. The primary use of this is for helpers/views so they can auto-link
  # usernames, lists, hashtags and URLs.
  module Autolink extend self
  # Default URL base for auto-linked usernames
    DEFAULT_USERNAME_URL_BASE = "/".freeze
  # Default URL base for auto-linked lists
    DEFAULT_LIST_URL_BASE = "/".freeze
  # Default URL base for auto-linked hashtags
    DEFAULT_HASHTAG_URL_BASE = "/".freeze
  # Default URL base for auto-linked cashtags
    DEFAULT_CASHTAG_URL_BASE = "/".freeze
  end

end