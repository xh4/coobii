module ShowNamespace

  def show_user
    @user = User.find_by_username_lower(params[:namespace].downcase)

    user_resources

    render 'users/show', :layout => 'application'
  end

  def show_page
    @page = Page.find_by_id(@namespace.model_id)

    @user = current_user

    page_resources

    if user_signed_in?

       user_resources

      render 'pages/show'
    else
      render 'pages/show_guest'
    end
  end

  private

  def user_resources

    @contents = Content
      .joins("INNER JOIN user_contents ON contents.id = user_contents.content_id")
      .where('contents.state' => 'normal')
      .where('user_contents.user_id' => @user.id)
      .order('user_contents.created_at DESC')
      .page(1)
      .per(10)

    @message_contents = Content
      .joins("LEFT JOIN user_contents ON contents.id = user_contents.content_id")
      .where('user_contents.user_id' => @user.id)
      .where('contents.type' => 'message')
      .order("user_contents.created_at DESC")
      .limit(10)

    @picture_contents = Content
      .joins("LEFT JOIN user_contents ON contents.id = user_contents.content_id")
      .order("user_contents.created_at DESC")
      .where('user_contents.user_id' => @user.id)
      .where(type: :picture)
      .limit(30)

  end

  def page_resources

    @topics = @page.topics
      .where.not(state: "pending")
      .order("created_at DESC")
      .page(1)
      .per(10)

    @page_messages = Message.last(4)

    @page_images = []
  end

end
